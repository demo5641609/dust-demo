
OUT_DIR := build
PROGRAM := dust

ASM_OUT := $(PROGRAM).asm

include config/Build.mk
include config/Version.mk
include config/System.mk

$(info Output dir: $(OUT_DIR))

$(info Game version: $(GAME_VERSION_MAJOR).$(GAME_VERSION_MINOR).$(GAME_VERSION_PATCH)-$(GAME_VERSION_BUILD) [$(BUILD_TYPE)])

$(info Compiler:   $(shell which $(CPP)))

CPP_FILES := $(wildcard source/*.cpp)
OBJ_FILES  = 

$(info Build type: $(build) [$(OS_TAG)])

GLFW_DIR := vendor/$(OS_TAG)/glfw
FMT_DIR  := vendor/$(OS_TAG)/fmt

VENDOR_INCLUDE := -I$(GLFW_DIR)/include -I$(FMT_DIR)/include
VENDOR_INCLUDE += -Ivendor/glad/include
VENDOR_INCLUDE += -Ivendor/glm/include
VENDOR_INCLUDE += -Ivendor/stb/include
VENDOR_INCLUDE += -Ivendor/tinygltf/include
VENDOR_LFLAGS  := -L$(GLFW_DIR)/lib-mingw-w64 -L$(FMT_DIR)/lib -lglfw3

CPPFLAGS += -fno-exceptions -fno-rtti
CPPFLAGS += -Isource/include $(VENDOR_INCLUDE)

# For tinygltf and its dependencies...
CPPFLAGS += -DJSON_NOEXCEPTION -DTINYGLTF_NOEXCEPTION

LPPFLAGS :=

LPPFLAGS += $(VENDOR_LFLAGS)

OUTPUT  = $(OUT_DIR)/$(PROGRAM)

OBJ_DIR = $(OUT_DIR)/obj
DEP_DIR = $(OBJ_DIR)

vpath %.cpp source
vpath %.cc  vendor/tinygltf/src
vpath %.c   vendor/glad/src
vpath %.o   $(OBJ_DIR)
vpath %.d   $(DEP_DIR)

DEPFLAGS = -MT $@ -MMD -MP -MF $(DEP_DIR)/$*.d

OBJ_FILES := $(patsubst source/%,%,$(patsubst %.cpp,%.o,$(CPP_FILES)))
OBJ_FILES += glad.o
OBJ_FILES += tiny_gltf.o

DEP_FILES := $(patsubst %.o,%.d,$(OBJ_FILES))

$(shell mkdir -p $(OUT_DIR))
$(shell mkdir -p $(OBJ_DIR))
$(shell mkdir -p $(DEP_DIR))

.PHONY: all
all: $(OUTPUT)
	@echo [Make] Done

$(OUTPUT): deps $(OBJ_FILES)
	$(info [C++] Building $(OUTPUT))
	@$(CPP) $(CPPFLAGS) $(CPP_DEFINES) $(addprefix $(OBJ_DIR)/,$(OBJ_FILES)) $(LPPFLAGS) -o $(OUTPUT)
	@echo "[Make] Copying glfw3.dll to $(OUT_DIR)"...
	$(shell cp $(GLFW_DIR)/lib-mingw-w64/glfw3.dll $(OUT_DIR))

%.o: %.cpp %.d | $(DEP_DIR)
	$(info [C++] Building $< -> $(OBJ_DIR)/$@)
	@$(CPP) $(DEPFLAGS) $(CPPFLAGS) $(CPP_DEFINES) $< -c -o $(OBJ_DIR)/$@

%.o: %.cc %.d | $(DEP_DIR)
	$(info [C++] Building $< -> $(OBJ_DIR)/$@)
	@$(CPP) $(DEPFLAGS) $(CPPFLAGS) $(CPP_DEFINES) $< -c -o $(OBJ_DIR)/$@

%.o: %.c %.d | $(DEP_DIR)
	$(info [C++] Building $< -> $(OBJ_DIR)/$@)
	@$(CPP) $(DEPFLAGS) $(CPPFLAGS) $(CPP_DEFINES) $< -c -o $(OBJ_DIR)/$@

.PHONY: deps
deps: $(DEP_FILES)
	$(info Done generating dependency files)

%.d: %.cpp
	$(info [Dep] Generating dependency for $< -> $(DEP_DIR)/$@)
	@$(CPP) $(CPPFLAGS) $(CPP_DEFINES) $^ -MM -MT $(addprefix $(OBJ_DIR)/, $(@:.d=.o)) > $(OBJ_DIR)/$@

%.d: %.cc
	$(info [Dep] Generating dependency for $< -> $(DEP_DIR)/$@)
	@$(CPP) $(CPPFLAGS) $(CPP_DEFINES) $^ -MM -MT $(addprefix $(OBJ_DIR)/, $(@:.d=.o)) > $(OBJ_DIR)/$@

%.d: %.c
	$(info [Dep] Generating dependency for $< -> $(DEP_DIR)/$@)
	@$(CPP) $(CPPFLAGS) $(CPP_DEFINES) $^ -MM -MT $(addprefix $(OBJ_DIR)/, $(@:.d=.o)) > $(OBJ_DIR)/$@

$(DEP_DIR):
	$(shell mkdir -p $(DEP_DIR))

.PHONY: asm
.PHONY: run
.PHONY: debug
.PHONY: clean

include $(wildcard $(DEP_DIR)/*.d)

asm:
	$(info [Asm] Generating assembly file $(ASM_OUT)...)
	@objdump -d $(OUTPUT) > $(ASM_OUT)
	$(info [Asm] Done)

run:
	./$(OUTPUT)

debug:
	$(info [Debug] Starting GDB server at localhost:9091)
	@gdbserver localhost:9091 build/win64/game.exe

clean:
	rm -rf $(OUT_DIR)
	rm -f main.o
	rm -f $(ASM_OUT)
