#include <dust/common.hpp>
#include <dust/utility/error.hpp>
#include <dust/graphics/gl.hpp>
#include <dust/graphics/window.hpp>

#include <iostream>

namespace DUST_NAMESPACE
{

  thread_local dust::Window *thread_window_s = nullptr;

  DUST_API_LOCAL void framebuffer_size_callback(
    GLFWwindow *_window,
    int width,
    int height);

  Window::Window()
    : m_window(nullptr),
      m_previous_width(0),
      m_previous_height(0),
      m_is_fullscreen(false)
  {

  }

  dust_error_t Window::open(
    std::string_view title,
    dust_size_t width,
    dust_size_t height)
  {
    if (!glfw_is_initialized())
    {
      graphics_start();
    }

    if (thread_window_s != nullptr)
    {
      return DUST_ERROR_WINDOW_OPEN;
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

    // Enable multisampling/antialiasing
    // glfwWindowHint(GLFW_SAMPLES, 4);

    m_window = glfwCreateWindow(
      (int) width, (int) height, title.data(), NULL, NULL);

    if (m_window == NULL)
    {
      return DUST_ERROR_GLFW_WINDOW_CREATE;
    }

    glfwMakeContextCurrent(m_window);

    if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress))
    {
      return DUST_ERROR_GLAD_LOAD_GL;
    }

    glfwSetFramebufferSizeCallback(m_window, framebuffer_size_callback);

    thread_window_s = this;

    m_previous_width = width;
    m_previous_height = height;

    set_fullscreen(false);

    // glEnable(GL_MULTISAMPLE);

    return DUST_OK;
  }
  
  void Window::close()
  {
    if (m_window != nullptr)
    {
      glfwWindowShouldClose(m_window);

      thread_window_s = nullptr;
    }
  }

  bool Window::is_open() const
  {
    return m_window != nullptr;
  }
  
  void Window::set_title(
    std::string_view title)
  {
    glfwSetWindowTitle(m_window, title.data());
  }
  
  void Window::enable_vsync(
    bool enable)
  {
    if (glfw_is_initialized())
    {
      glfwSwapInterval((int) enable);
    }
  }
  
  void Window::set_fullscreen(
    bool fullscreen,
    bool windowed)
  {
    GLFWmonitor *monitor;
    GLFWvidmode const *mode;

    int monitors = 0;

    monitor = *glfwGetMonitors(&monitors);
    mode = glfwGetVideoMode(monitor);

    if (fullscreen)
    {
      std::cout << "[Window] Enabling fullscreen: "
                << mode->width << "x" << mode->height
                << std::endl;

      /**
       * Disable window decorations for fullscreen mode so that
       * windowed fullscreen will work properly...
       */
      glfwSetWindowAttrib(m_window, GLFW_DECORATED, GLFW_FALSE);

      glfwSetWindowMonitor(m_window, windowed ? NULL : monitor,
        0, 0, mode->width, mode->height, mode->refreshRate);

      m_is_fullscreen = true;
    }
    else
    {
      std::cout << "[Window] Disabling fullscreen: "
                << m_previous_width << "x" << m_previous_height
                << std::endl;

      glfwSetWindowAttrib(m_window, GLFW_DECORATED, GLFW_TRUE);

      glfwSetWindowMonitor(m_window, nullptr, 10, 75,
        m_previous_width, m_previous_height, 0);

      m_is_fullscreen = false;

      // resize(m_previous_width, m_previous_height);
    }
  }

  bool Window::is_fullscreen() const
  {
    return m_is_fullscreen;
  }

  Window *Window::get_local_thread_window()
  {
    return thread_window_s;
  }

  dust_size_t Window::get_width() const
  {
    int width = 0;

    glfwGetWindowSize(m_window, &width, NULL);

    return (dust_size_t) width;
  }

  dust_size_t Window::get_height() const
  {
    int height = 0;

    glfwGetWindowSize(m_window, NULL, &height);

    return (dust_size_t) height;
  }

  GLFWwindow const *Window::get_window() const
  {
    return m_window;
  }

  GLFWwindow *Window::get_window()
  {
    return m_window;
  }

  void Window::loop_done() const
  {
    glfwSwapBuffers(m_window);
    glfwPollEvents();
  }

  void Window::resize(
    dust_size_t width,
    dust_size_t height)
  {
    glfwSetWindowSize(m_window, (int) width, (int) height);
    glViewport(0, 0, width, height);
  }

  void framebuffer_size_callback(
    GLFWwindow *_window,
    int width,
    int height)
  {
    Window *window = Window::get_local_thread_window();

    DUST_UNUSED _window;

    window->resize((dust_size_t) width, (dust_size_t) height);
  }

}
