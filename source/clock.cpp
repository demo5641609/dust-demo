#include <dust/common.hpp>
#include <dust/utility/clock.hpp>

#include <chrono>

namespace DUST_NAMESPACE
{

  Clock::Clock()
  {
    start();
  }

  Clock::TimePoint Clock::now()
  {
    return std::chrono::steady_clock::now();
  }

  void Clock::start()
  {
    m_clock = Clock::now();
  }

  Clock::TimePoint Clock::get_timepoint() const
  {
    return m_clock;
  }

  float Clock::get_elapsed_time() const
  {
    return Clock::elapsed(m_clock, Clock::now());
  }

  float Clock::get_delta_time() const
  {
    return get_elapsed_time() / 1000.0f;
  }

  float Clock::elapsed(
    TimePoint from,
    TimePoint to)
  {
    return std::chrono::duration_cast<std::chrono::milliseconds>(
      to - from).count();
  }

  float Clock::elapsed(
    Clock const& from,
    Clock const& to)
  {
    return Clock::elapsed(
      from.get_timepoint(), to.get_timepoint());
  }

}
