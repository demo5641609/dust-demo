#include <dust/common.hpp>
#include <dust/memory/sector.hpp>
#include <dust/utility/file.hpp>
#include <dust/graphics/gl.hpp>
#include <dust/graphics/shader.hpp>
#include <dust/graphics/model_shader.hpp>

#include <iostream>
#include <string_view>

namespace DUST_NAMESPACE
{

  ModelShader::ModelShader()
  {
    invalidate();
  }
  
  ModelShader::~ModelShader()
  {
    done();
  }

  dust_error_t ModelShader::initialize(
    Sector& string_sector,
    Sector& data_sector,
    GLsizei extra_vertex_floats,
    std::string_view vertex_shader_path,
    std::string_view fragment_shader_path)
  {
    GLuint shader_program;

    dust_error_t error = Shader::load(
      string_sector, data_sector,
      vertex_shader_path, fragment_shader_path);

    m_vertex_attribute_floats = DUST_MODEL_SHADER_VERTEX_FLOATS + extra_vertex_floats;

    if (error)
    {
      return error;
    }

    shader_program = Shader::get_program();

    m_a_pos_location = glGetAttribLocation(
      shader_program, "aPos");

    if (m_a_pos_location == -1)
    {
      std::cout << "[SpriteShader] Warning: \"aPos\" attribute not found by the shader. Is it unused in shader code?" << std::endl;
    }

    m_a_color_location = glGetAttribLocation(
      shader_program, "aColor");

    if (m_a_color_location == -1)
    {
      std::cout << "[SpriteShader] Warning: \"aColor\" attribute not found by the shader. Is it unused in shader code?" << std::endl;
    }

    /**
     * Cache the matrix attribute locations for
     * later usage...
     */
    m_MVP_id = glGetUniformLocation(
      shader_program, "uMVP");

    if (m_MVP_id == -1)
    {
      std::cout << "[ModelShader] Warning: \"uMVP\" uniform not found by the shader. Is it unused in shader code?" << std::endl;
    }

    if (gl_get_error() != DUST_OK)
    {
      done();

      return DUST_ERROR_OPENGL_SHADER;
    }

    return DUST_OK;
  }

  dust_error_t ModelShader::initialize_VAO() const
  {
    GLsizei const stride = get_vertex_attribute_stride();

    /**
     *  Vertex shader position input attribute...
     */
    glVertexAttribPointer(m_a_pos_location,
      3, GL_FLOAT, GL_FALSE, stride, nullptr);

    glEnableVertexAttribArray(m_a_pos_location);

    /**
     *  Vertex shader color input attribute...
     */
    glVertexAttribPointer(m_a_color_location,
      4, GL_FLOAT, GL_FALSE, stride,
      (void *) (3 * sizeof(GLfloat)));

    glEnableVertexAttribArray(m_a_color_location);

    return gl_get_error();
  }

  GLint ModelShader::get_a_pos_location() const
  {
    return m_a_pos_location;
  }

  GLint ModelShader::get_a_color_location() const
  {
    return m_a_color_location;
  }

  GLuint ModelShader::get_MVP_uniform_id() const
  {
    return m_MVP_id;
  }

  void ModelShader::use()
  {
    Shader::use();

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    /**
     * Make sure transparent quads don't
     * override pixels for other quads...
     */
    glEnable(GL_ALPHA_TEST);
    glAlphaFunc(GL_GREATER, 0.0f);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  }

  void ModelShader::done()
  {
    invalidate();

    Shader::done();
  }

  dust_ptr_t ModelShader::get_vertex_pointer_offset() const
  {
    return (GLuint) (sizeof(GLfloat) * DUST_MODEL_SHADER_VERTEX_FLOATS);
  }

  GLsizei ModelShader::get_vertex_attribute_stride() const
  {
    return (GLsizei) (sizeof(GLfloat) * m_vertex_attribute_floats);
  }

  void ModelShader::invalidate()
  {
    Shader::invalidate();

    m_a_pos_location   = -1;
    m_a_color_location = -1;

    m_MVP_id = -1;
  }

}
