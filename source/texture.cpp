#include <dust/common.hpp>
#include <dust/graphics/gl.hpp>
#include <dust/graphics/texture.hpp>

#include <stb/stb_image.h>

#include <iostream>

namespace DUST_NAMESPACE
{

  std::string const Texture::ColorspaceNames[Texture::COLORSPACE_MAX] = {
    "linear",
    "non-linear"
  };

  Texture::Texture()
    : m_width(0),
      m_height(0),
      m_texture(0)
  {
    if (m_texture != 0)
    {
      glDeleteTextures(1, &m_texture);

      m_texture = 0;
    }
  }
  
  Texture::~Texture()
  {

  }

  dust_error_t Texture::initialize(
    std::string_view image_path,
    Colorspace colorspace,
    GLenum pixel_format,
    Colorspace colorspace_internal,
    GLenum pixel_format_internal)
  {
    dust_byte_t *stb_image_data = nullptr;

    int width_i;
    int height_i;
    int components_i;

    GLenum new_pixel_format;
    GLenum new_pixel_format_internal;

    m_texture = 0;

    stb_image_data = (dust_byte_t *) stbi_load(
      image_path.data(), &width_i, &height_i, &components_i, 0);

    if (stb_image_data != nullptr)
    {
      glGenTextures(1, &m_texture);

      if (gl_get_error() == DUST_OK)
      {
        if (pixel_format != GL_INVALID_ENUM)
        {
          new_pixel_format = pixel_format;
        }
        else
        {
          new_pixel_format = Texture::get_pixel_format(
            (unsigned int) components_i, colorspace);
        }

        if (pixel_format_internal != GL_INVALID_ENUM)
        {
          new_pixel_format_internal = pixel_format_internal;
        }
        else
        {
          new_pixel_format_internal = Texture::get_pixel_format(
            (unsigned int) components_i, colorspace_internal);
        }

        if (new_pixel_format != GL_INVALID_ENUM)
        {
          m_width = (dust_size_t) width_i;
          m_height = (dust_size_t) height_i;
          m_components = (unsigned int) new_pixel_format_internal;

          glBindTexture(GL_TEXTURE_2D, m_texture);

          glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
          glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

          glTexImage2D(GL_TEXTURE_2D, 0, new_pixel_format_internal,
            (GLsizei) width_i, (GLsizei) height_i, 0,
            new_pixel_format, GL_UNSIGNED_BYTE, stb_image_data);

          free(stb_image_data);
          stb_image_data = nullptr;

          glGenerateMipmap(GL_TEXTURE_2D);

          glBindTexture(GL_TEXTURE_2D, 0);

          free(stb_image_data);
          stb_image_data = nullptr;

          if (gl_get_error() == DUST_OK)
          {
            return DUST_OK;
          }
        }
        else
        {
          std::cout << "[Texture] Failed to upload image pixels\n"
                    << " -> Image: " << image_path << std::endl;
        }
      }
      else
      {
        std::cout << "[Texture] Failed to generate an OpenGL texture object"
                  << std::endl;
      }
    }
    else
    {
      std::cout << "[Texture] STB failed to load \""
                << image_path << "\"" << std::endl;
    }

    free(stb_image_data);
    stb_image_data = nullptr;

    glDeleteTextures(1, &m_texture);

    return DUST_ERROR_TEXTURE_INIT;
  }

  dust_size_t Texture::get_width() const
  {
    return m_width;
  }

  dust_size_t Texture::get_height() const
  {
    return m_height;
  }

  unsigned int Texture::get_color_components() const
  {
    return m_components;
  }

  GLuint Texture::get_texture() const
  {
    return m_texture;
  }

  GLenum Texture::get_pixel_format(
    unsigned int color_components,
    Colorspace colorspace)
  {
    GLenum const linear[] = {
      GL_R,
      GL_RG,
      GL_RGB,
      GL_RGBA
    };
    
    GLenum const non_linear[] = {
      GL_SRGB8,
      GL_SRGB8_ALPHA8
    };

    if ((color_components < 3))
    {
      if ((color_components == 0) || (colorspace == NON_LINEAR))
      {
        return GL_INVALID_ENUM;
      }

      return linear[color_components - 1];
    }
    else
    {
      if (color_components > 4)
      {
        return GL_INVALID_ENUM;
      }
      else
      {
        switch(colorspace)
        {
          case LINEAR:
            return linear[color_components - 1];
            break;

          case NON_LINEAR:
            return non_linear[color_components - 1];
            break;

          default:
            break;
        }
      }
    }

    std::cout << "[Texture] Failed to determine the pixel format:\n"
              << " -> Color components: " << color_components << "\n"
              << " -> Colorspace: " << ColorspaceNames[colorspace]
              << std::endl;

    return GL_INVALID_ENUM;
  }

}
