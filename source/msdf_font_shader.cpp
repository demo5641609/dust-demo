#include <dust/common.hpp>
#include <dust/memory/sector.hpp>
#include <dust/utility/file.hpp>

#include <dust/graphics/gl.hpp>
#include <dust/graphics/shader.hpp>
#include <dust/graphics/model_shader.hpp>
#include <dust/graphics/msdf_font_shader.hpp>

#include <iostream>
#include <string_view>

namespace DUST_NAMESPACE
{

  MSDFFontShader::MSDFFontShader()
    : ModelShader()
  {
    invalidate();
  }

  dust_error_t MSDFFontShader::initialize(
    Sector& string_sector,
    Sector& data_sector,
    GLsizei extra_vertex_floats,
    std::string_view vertex_shader_path,
    std::string_view fragment_shader_path)
  {
    GLuint shader_program;

    dust_error_t error = ModelShader::initialize(
      string_sector, data_sector,
      DUST_MSDF_FONT_SHADER_EXTRA_VERTEX_FLOATS + extra_vertex_floats,
      vertex_shader_path, fragment_shader_path);

    if (error)
    {
      std::cout << "[MSDFFontShader] Initialization failed because the model shader failed to initialize" << std::endl;

      return error;
    }

    shader_program = Shader::get_program();

    m_screen_pixel_range_uniform_id = glGetUniformLocation(
      shader_program, "uScreenPixelRange");

    if (m_screen_pixel_range_uniform_id == -1)
    {
      std::cout << "[MSDFFontShader] Warning: \"uScreenPixelRange\" uniform not found by the shader. Is it unused in shader code?" << std::endl;
    }

    m_a_UV_location = glGetAttribLocation(shader_program, "aUV");

    if (m_a_UV_location == -1)
    {
      std::cout << "[MSDFFontShader] Warning: \"aUV\" attribute not found by the shader. Is it unused in shader code?" << std::endl;
    }

    return DUST_OK;
  }

  dust_error_t MSDFFontShader::initialize_VAO() const
  {
    GLsizei const stride = get_vertex_attribute_stride();
    dust_ptr_t const pointer = ModelShader::get_vertex_pointer_offset();

    ModelShader::initialize_VAO();

    /**
     * This shader adds 2 floats for the UV coordinates
     * on top of the model shader attributes so we pass
     * 2 to the ModelShader 'get_stride' function...
     */

    /**
     *  Vertex shader texture UVs attribute...
     */
    glVertexAttribPointer(m_a_UV_location, 2, GL_FLOAT, GL_FALSE,
      stride, (void const *) pointer);

    glEnableVertexAttribArray(m_a_UV_location);

    return gl_get_error();
  }

  GLuint MSDFFontShader::get_screen_pixel_range_uniform_id() const
  {
    return m_screen_pixel_range_uniform_id;
  }

  void MSDFFontShader::done()
  {
    ModelShader::done();

    invalidate();
  }

  dust_ptr_t MSDFFontShader::get_vertex_pointer_offset() const
  {
    /**
     * This shader adds 2 floats for the UV coordinate
     * on top of the model shader attributes so we add
     * 2 to the pointer returned by the model shader...
     */

    return (GLuint) (ModelShader::get_vertex_pointer_offset()
      + (DUST_MSDF_FONT_SHADER_EXTRA_VERTEX_FLOATS * sizeof(GLfloat)));
  }

  void MSDFFontShader::invalidate()
  {
    ModelShader::invalidate();

    m_a_UV_location = -1;
    m_screen_pixel_range_uniform_id = -1;
  }

}
