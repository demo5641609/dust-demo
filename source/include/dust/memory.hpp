#ifndef DUST_MEMORY_HPP
#define DUST_MEMORY_HPP

#include <dust/common.hpp>

// #include <dust/memory/hashmap.hpp>

/**
 * Numeric type to compare and perform arithmetic over pointers.
 */
typedef uintptr_t dust_ptr_t;

/**
 * Calculates the amount of bytes that corresponds to 'n' kilobytes.
 * @return n * 1024
 */
#define DUST_KILOBYTES(n) (1024LLU * n)

/**
 * Calculates the amount of bytes that corresponds to 'n' megabytes.
 * @return n * 1024^2
 */
#define DUST_MEGABYTES(n) (1024LLU * 1024LLU * n)

/**
 * Calculates the amount of bytes that corresponds to 'n' gigabytes.
 * @return n * 1024^3
 */
#define DUST_GIGABYTES(n) (1024LLU * 1024LLU * 1024LLU * n)

/**
 * Assumed memory page size.
 * This should probably be replaced with
 * system-specific functions later.
 */
#define DUST_PAGE_SIZE (4 * 1024)

/**
 * Calculates the next aligned pointer with alignment 'a' after 'p'.
 */
#define DUST_ALIGN_POINTER(p, a) (((dust_ptr_t) p) + ((a - 1) & -(a)))

/**
 * Offsets the pointer 'p' by 'o' bytes.
 */
#define DUST_PTR_OFFSET(p, o) ((decltype(p)) (((dust_ptr_t) p) + (o)))

/**
 * Subtracts the right pointer from the left pointer.
 * @return pl - pr
 */
#define DUST_PTR_DIFF(pl, pr) (((dust_ptr_t) pl) - ((dust_ptr_t) pr))

#ifndef DUST_MINIMUM_REGION_SIZE
  #define DUST_MINIMUM_REGION_SIZE DUST_KILOBYTES(64)
#endif

namespace DUST_NAMESPACE
{

  /**
   * Since the compiler tries to perform some object-oriented nonsense
   * when std::memmove isn't directly given void pointers, I've made
   * this alias. It just passes the arguments to std::memmove.
   * (it's actually not nonsense. It invokes the assignment operator
   * for the underlying type. If you actually need that, use
   * std::memmove instead!)
   */
  void memory_move(void *dst, void const *src, std::size_t const size);

  /**
   * Since the compiler tries to perform some object-oriented nonsense
   * when std::memcpy isn't directly given void pointers, I've made
   * this alias. It just passes the arguments to std::memcpy.
   * (it's actually not nonsense. It invokes the assignment operator
   * for the underlying type. If you actually need that, use
   * std::memcpy instead!)
   */
  void memory_copy(void *dst, void const *src, std::size_t const size);

  /**
   * Sets 'size' bytes to 0 starting at 'dst'.
   * This is a shorthand for a type-less memset to 0s.
   */
  void memory_clear(void *dst, std::size_t const size);

  /**
   * Sets the object's bytes 0 with respect to its size.
   */
  template<class T>
  DUST_API_LOCAL void memory_clear(T& object)
  {
    dust::memory_clear(&object, sizeof(T));
  }

  /**
   * A simple function that copies the binary memory of an object
   * to the given destination. Does not invoke object operators.
   */
  template<class T>
  DUST_API_LOCAL void copy_object(void *dst, T const& src)
  {
    dust::memory_copy(dst, &src, sizeof(T));
  }

}

#endif // DUST_MEMORY_HPP
