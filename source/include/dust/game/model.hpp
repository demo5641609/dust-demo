#ifndef DUST_GAME_MODEL_HPP
#define DUST_GAME_MODEL_HPP

#include <dust/common.hpp>
#include <dust/graphics/gl.hpp>

#include <glm/glm.hpp>
#include <glm/matrix.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

/**
 * Expands into a glm::vec3 with Z set to 1.0f.
 */
#define DUST_MODEL_2D_ROTATION_AXIS \
  (glm::vec3{ 0.0f, 0.0f, 1.0f })

namespace DUST_NAMESPACE
{

  /**
   * Object encapsulating position, rotation and
   * scale properties. Provides simple API to read
   * and set these properties with. Provides model
   * matrix calculation API.
   */
  class Model
  {
    public:

      /**
       * Initializes a model with all properties
       * set to 0 except scale which is set to
       * (1, 1, 1).
       */
      Model();

      /**
       * @return The rotation quaternion of the
       * object.
       */
      glm::quat const& get_rotation() const;

      /**
       * @return The position of the object.
       */
      glm::vec3 get_position() const;

      /**
       * @return The scale of the object.
       */
      glm::vec3 get_scale() const;
      /**
       * Sets the position of the object.
       * @param position The position to which to
       * move the object.
       * @return Reference to 'this'.
       */

      /**
       * @return The origin point of the object.
       */
      glm::vec3 get_origin() const;

      Model& set_position(
        glm::vec3 const& position);

      /**
       * Sets the rotation of the object.
       * @param rotation The rotation to which to
       * rotate the object. Component values in
       * radians.
       * @return Reference to 'this'.
       */
      Model& set_rotation(
        glm::vec3 const& rotation);

      /**
       * Sets the rotation of the object.
       * @param rotation The rotation to which to
       * rotate the object.
       * @return Reference to 'this'.
       */
      Model& set_rotation(
        glm::quat const& rotation);

      /**
       * Sets the rotation of the object on the Z
       * axis only.
       * @param rotation The Z-axis rotation (in
       * radians) to which to rotate the object.
       * @return Reference to 'this'.
       */
      Model& set_rotation_2D(
        float radians);

      /**
       * Sets the scale of the object.
       * @param scale The scale to which to scale
       * the object.
       * @return Reference to 'this'.
       */
      Model& set_scale(
        glm::vec3 const& scale);

      /**
       * Adds the given offset to the position of
       * the object.
       * @param offset The offset which to apply
       * to the position of the object.
       * @return Reference to 'this'.
       */
      Model& translate(
        glm::vec3 const& offset);

      /**
       * Adds the given rotation to the rotation
       * of the object.
       * @param offset The rotation which to apply
       * to the current rotation of the object.
       * Component values in radians.
       * @return Reference to 'this'.
       */
      Model& rotate(
        glm::vec3 const& rotation);

      /**
       * Adds the given rotation to the rotation
       * of the object.
       * @param offset The rotation which to apply
       * to the current rotation of the object.
       * @return Reference to 'this'.
       */
      Model& rotate(
        glm::quat const& rotation);

      /**
       * Adds the given rotation to the rotation
       * of the object.
       * @param offset The rotation which to apply
       * to the current rotation of the object.
       * @return Reference to 'this'.
       */
      Model& rotate(
        float radians,
        glm::vec3 const& axis);

      /**
       * Adds the given rotation to the rotation
       * of the object on the Z axis.
       * @param offset The rotation which to apply
       * to the current Z rotation of the object.
       * @return Reference to 'this'.
       */
      Model& rotate_2D(
        float radians);

      /**
       * Multiplicatively adds the given scale to
       * the scale of the object.
       * @param offset The scale which to apply
       * to the current scale of the object.
       * @return Reference to 'this'.
       */
      Model& scale(
        glm::vec3 const& scale);

      /**
       * Sets the point around which the model is
       * scaled and rotated. The origin acts as the
       * local-space anchor for scaling and rotation.
       * @param origin The position around which to
       * apply transformations.
       * @return Reference to 'this'.
       */
      Model& set_origin(
        glm::vec3 const& origin);

      /**
       * Sets the rotation of the object such that
       * it points directly at the t target position.
       * @param target_position The point to which to
       * look at.
       */
      void look_at(
        glm::vec3 const& target_position);

      /**
       * @return The model matrix of the object.
       */
      glm::mat4 get_model_matrix() const;

      /**
       * Stores the object's model matrix in the
       * given 'model_matrix'.
       * @param model_matrix The matrix into which
       * to store the calculated model matrix of
       * this object.
       */
      void export_model_matrix(
        glm::mat4& model_matrix) const;

    private:

      /**
       * The rotation quaternion of the model.
       */
      glm::quat m_rotation;

      /**
       * Translation of the model.
       */
      glm::vec3 m_position;

      /**
       * Scale of the model.
       * 1.0 is neutral or "default" scale.
       */
      glm::vec3 m_scale;

      /**
       * The point around which scaling and
       * rotation is applied.
       */
      glm::vec3 m_origin;

  };

}

#endif // DUST_GAME_MODEL_HPP
