#ifndef DUST_GAME_CAMERA_H
#define DUST_GAME_CAMERA_H

#include <dust/common.hpp>
#include <dust/graphics/gl.hpp>

#include <dust/graphics/model_shader.hpp>
#include <dust/game/model.hpp>

#include <glm/matrix.hpp>

/**
 * Default perspective view FOV value.
 */
#define DUST_CAMERA_PERSP_FOV_DEFAULT  (45.0f)

/**
 * Default perspective view 'near' clipping
 * plane distance.
 */
#define DUST_CAMERA_PERSP_NEAR_DEFAULT (0.1f)

/**
 * Default perspective view 'far' clipping
 * plane distance.
 */
#define DUST_CAMERA_PERSP_FAR_DEFAULT  (100.0f)

/**
 * Default orthographic view 'near' clipping
 * plane distance.
 */
#define DUST_CAMERA_ORTHO_NEAR_DEFAULT (-1.0f)

/**
 * Default orthographic view 'far' clipping
 * plane distance.
 */
#define DUST_CAMERA_ORTHO_FAR_DEFAULT  ( 1.0f)

namespace DUST_NAMESPACE
{

  /**
   * Object which provides the projection and view
   * matrices. The transformations applied on the
   * object determine the view matrix.
   */
  class Camera : public Model
  {
    public:

      enum Type
      {
        PERSPECTIVE,
        ORTHOGRAPHIC
      };

      /**
       * Initializes an invalid camera instance.
       */
      Camera();

      /**
       * @param type Perspective or orthographic.
       * @param width The width of the camera's view
       * in pixels.
       * @param height The height of the camera's view
       * @return DUST_OK on success, or
       * DUST_ERROR_BAD_ENUM_VALUE on failure.
       */
      dust_error_t initialize(
        Camera::Type type,
        dust_size_t width,
        dust_size_t height);

      /**
       * @return The projection matrix of the camera.
       */
      glm::mat4 const& get_projection_matrix() const;

      /**
       * @return The view matrix of the camera.
       */
      glm::mat4 get_view_matrix() const;

      /**
       * Sets the camera's projection matrix to a
       * perspective view matrix
       * @param width The width of the camera's view
       * in pixels.
       * @param height The height of the camera's view
       * in pixels.
       * @param fov The field of view in degrees.
       * @param near The near clipping plane distance.
       * @param far The far clipping plane distance.
       */
      void set_perspective_projection_matrix(
        dust_size_t width,
        dust_size_t height,
        GLfloat fov  = DUST_CAMERA_PERSP_FOV_DEFAULT,
        GLfloat near = DUST_CAMERA_PERSP_NEAR_DEFAULT,
        GLfloat far  = DUST_CAMERA_PERSP_FAR_DEFAULT);

      /**
       * Sets the camera's projection matrix to an
       * orthographic view matrix
       * @param width The width of the camera's view
       * in pixels.
       * @param height The height of the camera's view
       * in pixels.
       * @param near The near clipping plane distance.
       * @param far The far clipping plane distance.
       */
      void set_orthographic_projection_matrix(
        dust_size_t width,
        dust_size_t height,
        GLfloat near = DUST_CAMERA_ORTHO_NEAR_DEFAULT,
        GLfloat far  = DUST_CAMERA_ORTHO_FAR_DEFAULT);

      /**
       * Calculates the projection-view-model matrix
       * based on the given model matrix. Uses the
       * internal model matrix as the view matrix.
       * @param model_matrix The model matrix to
       * apply.
       * @return The MVP matrix to use as a shader
       * uniform.
       */
      glm::mat4 get_MVP_matrix(
        glm::mat4 const& model_matrix) const;

    private:

      /**
       * Matrix for perspective view.
       */
      glm::mat4 m_projection_matrix;

      /**
       * We don't store the model or view matrices
       * because we use the camera's model matrix
       * as the view matrix for the scenes...
       */

  };

}

#endif // DUST_GAME_CAMERA_H
