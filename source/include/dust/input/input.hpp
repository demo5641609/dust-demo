#ifndef DUST_INPUT_INPUT_HPP
#define DUST_INPUT_INPUT_HPP

#include <dust/common.hpp>
#include <dust/memory/array.hpp>
#include <dust/memory/allocator.hpp>

#include <dust/graphics/window.hpp> // GLFW

#include <unordered_map>

/**
 * Key state for when a key is being actively held down.
 */
#define DUST_KEY_PRESSED       0x01

/**
 * Key state for when a key was previously released and
 * it was just updated to "pressed".
 */
#define DUST_KEY_JUST_PRESSED  0x02

/**
 * Key state for when a key is not being pressed.
 */
#define DUST_KEY_RELEASED      0x04

/**
 * Key state for when a key was previously pressed and
 * it was just updated to "released".
 */
#define DUST_KEY_JUST_RELEASED 0x08

/**
 * Type used by GLFW to represent keycode and scancodes.
 */
typedef int dust_keycode_t;

namespace DUST_NAMESPACE
{

  /**
   * Updates the 'key' state for the local Input instance.
   * Assumes the local Input instance exists and is valid.
   */
  extern void input_glfw_key_callback(
    GLFWwindow* window,
    int key,
    int scancode,
    int action,
    int mods);

  /**
   * Object for keeping track of keyboard keys and the mouse.
   * Provides simple API for updating everything and reading
   * specific key states and mouse events and position.
   */
  class Input
  {
    protected:

      /**
       * Small state object that represents a single keyboard
       * key. Provides a simple API to set and check the state
       * of a keyboard key.
       * 
       * Used by the Input class to keep track of an array of
       * keys.
       */
      class KeyState
      {
        public:

          /**
           * Sets the key into the "released" state.
           */
          void initialize();

          /**
           * Updates the state of the key.
           * @param action
           */
          void update(int action);

          /**
           * @return True if the key is currently pressed,
           * false otherwise.
           */
          bool is_pressed();

          /**
           * This event occurs when the key goes from "released"
           * state to "pressed" state.
           * @return True if the key has been just pressed,
           * false otherwise.
           */
          bool is_just_pressed();

          /**
           * @return True if the key is currently released,
           * false otherwise.
           */
          bool is_released();

          /**
           * This event occurs when the key goes from "pressed"
           * state to "released" state.
           * @return True if the key has been just released,
           * false otherwise.
           */
          bool is_just_released();

        private:

          /**
           * Bit-flags representing the state of
           * the key from the last input update.
           */
          uint8_t m_state;

      };

    public:

      /**
       * Initializes the object as an invalid instance.
       */
      Input();

      /**
       * Initializes a thread-local instance that can track
       * keyboard and mouse input.
       * @param allocator The allocator through which to
       * request memory.
       * @return DUST_OK on success, or
       * DUST_ERROR_INPUT_INIT_FAIL on failure.
       */
      dust_error_t initialize(
        Allocator& allocator);

      /**
       * @param key The key which to check.
       * @return True if the key is currently pressed,
       * false otherwise.
       */
      bool is_key_pressed(
        dust_keycode_t key);

      /**
       * This event occurs when the key goes from "released"
       * state to "pressed" state.
       * @param key The key which to check.
       * @return True if the key has been just pressed,
       * false otherwise.
       */
      bool is_key_just_pressed(
        dust_keycode_t key);

      /**
       * @param key The key which to check.
       * @return True if the key is currently released,
       * false otherwise.
       */
      bool is_key_released(
        dust_keycode_t key);

      /**
       * This event occurs when the key goes from "pressed"
       * state to "released" state.
       * @param key The key which to check.
       * @return True if the key has been just released,
       * false otherwise.
       */
      bool is_key_just_released(
        dust_keycode_t key);

      /**
       * Updates all keyboard key states. Should be done
       * every frame before user input is being read.
       */
      void update();

      /**
       * @return Cursor coordinates relative to the top-left
       * corner of the window.
       */
      glm::vec2 get_cursor_position() const;

      /**
       * @return Cursor coordinates relative to the
       * bottom-left corner of the window.
       */
      glm::vec2 get_cursor_position_opengl() const;

    protected:

      /**
       * Allow the GLFW key callback to invoke updates for
       * specific keys...
       */
      friend void input_glfw_key_callback(
        GLFWwindow* window,
        int key,
        int scancode,
        int action,
        int mods);

      /**
       * Updates the key's state based on the given action.
       * @param key The key which to update.
       * @param action GLFW_PRESS, GLFW_RELEASE or
       * GLFW_REPEAT.
       */
      void update_key(
        dust_size_t key,
        int action);

    private:

      /**
       * Array of all trackable keys.
       */
      Array<KeyState> m_keys;

      /**
       * A map of keys that don't fit into the predefined
       * GLFW range but were accessed.
       * 
       * Such keys are not updated until they have been
       * accessed at least once.
       */
      std::unordered_map<dust_keycode_t, KeyState> m_extra_keys;

      glm::vec2 m_cursor_position;

      /**
       * Allocator with from which to request memory.
       */
      Allocator *m_allocator;

  };

}

#endif // DUST_INPUT_INPUT_HPP
