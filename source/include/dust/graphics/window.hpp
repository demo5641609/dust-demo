#ifndef DUST_GRAPHICS_WINDOW_H
#define DUST_GRAPHICS_WINDOW_H

#include <dust/common.hpp>
#include <dust/graphics/gl.hpp>

#include <glm/matrix.hpp>

#include <string_view>

namespace DUST_NAMESPACE
{

  class Window
  {
    public:

      /**
       * Initializes an invalid window instance.
       */
      Window();

      /**
       * Opens a window of the requested size.
       * Targets OpenGL version 3.3 compatibility.
       * Initializes an OpenGL context.
       * @param title The string-title of the window.
       * @param width The width of the window in pixels.
       * @param width The height of the window in pixels.
       * @return DUST_OK on success, or
       * DUST_ERROR_WINDOW_OPEN or
       * DUST_ERROR_GLFW_WINDOW_CREATE on failure.
       */
      dust_error_t open(
        std::string_view title,
        dust_size_t width,
        dust_size_t height);

      /**
       * Closes the window and clears the local thread
       * reference if the window was previously open.
       */
      void close();

      /**
       * @return True if the window is currently open,
       * false otherwise.
       */
      bool is_open() const;

      /**
       * Sets the title of the window to the given string.
       * @param title The new title of the window.
       */
      void set_title(
        std::string_view title);

      /**
       * Enables or disables fullscreen mode for the window.
       * @param fullscreen If true, forces the window into
       * 'fullscreen' mode, or disables 'fullscreen' otherwise.
       * @param windowed If true and window is entering
       * 'fullscreen' mode, the window will enter 'windowed
       * fullscreen' mode and not 'native fullscreen' mode.
       */
      void set_fullscreen(
        bool fullscreen = true,
        bool windowed = false);

      /**
       * @return True if the window is currently in
       * fullscreen mode, or false otherwise.
       */
      bool is_fullscreen() const;

      /**
       * Enables or disables vsync.
       * @param enable True to enable vsync, false to
       * disable it.
       */
      void enable_vsync(
        bool enable);

      /**
       * @return Pointer to the thread-local window.
       * Will be nullptr if no window is currently open
       * on the thread.
       */
      static Window *get_local_thread_window();

      /**
       * @return Width of the window in pixels.
       */
      dust_size_t get_width() const;

      /**
       * @return Height of the window in pixels.
       */
      dust_size_t get_height() const;

      /**
       * @return The internal GLFW window of the instance.
       */
      GLFWwindow *get_window();

      /**
       * @return The internal GLFW window of the instance.
       */
      GLFWwindow const *get_window() const;

      /**
       * Swaps the internal GLFW/GL buffers and pools for
       * events with the internal GLFW window.
       */
      void loop_done() const;

      /**
       * 
       * @param width The width of the window in pixels.
       * @param width The height of the window in pixels.
       */
      void resize(
        dust_size_t width,
        dust_size_t height);

    private:

      /**
       * Internal GLFW window instance.
       */
      GLFWwindow *m_window;

      /**
       * Non-fullscreen width of the window.
       */
      dust_size_t m_previous_width;

      /**
       * Non-fullscreen height of the window.
       */
      dust_size_t m_previous_height;

      /**
       * Since 'glfwGetWindowMonitor(m_window)' returns
       * nullptr when the window is in 'windowed fullscreen'
       * mode that is not a good enough indicator, so we
       * manually store the mode in this variable.
       */
      bool m_is_fullscreen;

  };

}

#endif // DUST_GRAPHICS_WINDOW_H
