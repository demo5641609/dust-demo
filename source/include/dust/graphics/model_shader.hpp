#ifndef DUST_GRAPHICS_MODEL_SHADER_HPP
#define DUST_GRAPHICS_MODEL_SHADER_HPP

#include <dust/common.hpp>
#include <dust/memory/sector.hpp>
#include <dust/graphics/gl.hpp>
#include <dust/graphics/shader.hpp>

#include <string_view>

/**
 * Default chunk vertex shader program path.
 */
#define DUST_MODEL_SHADER_SOURCE_VERTEX \
  "assets/shaders/model_vertex.glsl"

/**
 * Default chunk fragment shader program path.
 */
#define DUST_MODEL_SHADER_SOURCE_FRAGMENT \
  "assets/shaders/model_fragment.glsl"

/**
 * The number of floats used by the model shader
 * when defining vertex attributes.
 */
#define DUST_MODEL_SHADER_VERTEX_FLOATS 7U

namespace DUST_NAMESPACE
{

  /**
   * Provides access to the model shader uniform.
   * The uniform is MVP - the model-view-projection
   * matrix.
   * 
   * This shader serves as the most basic model shader
   * which can be inherited from to automatically have
   * access to model transform features.
   */
  class ModelShader : public Shader
  {
    public:

      /**
       * Initializes an invalid shader instance.
       */
      ModelShader();

      /**
       * Calls 'done()'.
       */
      ~ModelShader();

      /**
       * Reads and compiles the vertex and fragment shader.
       * @param string_sector Sector for filenames.
       * @param data_sector Sector for shader data.
       * @param extra_vertex_floats The additional
       * number of float values after the aPos-vec3 and
       * aColor-vec4 attributes. This argument is for
       * shaders that build on top of the model shader
       * and enable extra attributes. The value is added
       * to the 'stride'.
       * @param vertex_shader_path Path to the vertex
       * shader file.
       * @param fragment_shader_path Path to the fragment
       * shader file.
       * @return DUST_OK on success, or Shader::load() errors.
       */
      dust_error_t initialize(
        Sector& string_sector,
        Sector& data_sector,
        GLsizei extra_vertex_floats = 0,
        std::string_view vertex_shader_path = DUST_MODEL_SHADER_SOURCE_VERTEX,
        std::string_view fragment_shader_path = DUST_MODEL_SHADER_SOURCE_FRAGMENT);

      /**
       * Initializes vertex attributes for a VAO.
       * Does not bind or unbind a VAO, the user has to
       * bind and later unbind their own VAO.
       */
      virtual dust_error_t initialize_VAO() const;

      /**
       * @return The ID of the aPos vertex attribute,
       * or -1 if the shader was not initialized.
       */
      GLint get_a_pos_location() const;

      /**
       * @return The ID of the aPos vertex attribute,
       * or -1 if the shader was not initialized.
       */
      GLint get_a_color_location() const;

      /**
       * @return Shader model-view-projection
       * matrix uniform location.
       */
      GLuint get_MVP_uniform_id() const;

      /**
       * Invokes 'Shader::use()'. Enables
       * depth testing and alpha blending.
       */
      void use() override;

      /**
       * Deletes the internal VAO and invalidates the object.
       */
      void done() override;

    protected:

      /**
       * @return The offset in bytes that the inheriting
       * shader needs to use as the "pointer" for the
       * first additional attribute when calling
       * 'glVertexAttribPointer()'.
       */
      virtual dust_ptr_t get_vertex_pointer_offset() const;

      /**
       * @return The offset in bytes that the inheriting
       * shader needs to use as the "stride" for the
       * when calling 'glVertexAttribPointer()'.
       */
      GLsizei get_vertex_attribute_stride() const;

      /**
       * Sets uniform and attribute IDs to -1.
       */
      void invalidate() override;

    private:

      /**
       * Shader position attribute location.
       */
      GLint m_a_pos_location;

      /**
       * Shader color attribute location.
       */
      GLint m_a_color_location;

      /**
       * Model-view-projection matrix shader
       * uniform ID.
       */
      GLint m_MVP_id;

      /**
       * 
       */
      GLuint m_vertex_attribute_floats;

  };

}

#endif // DUST_GRAPHICS_MODEL_SHADER_HPP
