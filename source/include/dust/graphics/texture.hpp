#ifndef DUST_GRAPHICS_TEXTURE_H
#define DUST_GRAPHICS_TEXTURE_H

#include <dust/common.hpp>
#include <dust/memory/allocator.hpp>
#include <dust/graphics/gl.hpp>

#include <string>
#include <string_view>

namespace DUST_NAMESPACE
{

  /**
   * Object which provides API for loading an image
   * and detecting color format details.
   */
  class Texture
  {
    public:

      enum Colorspace
      {
        LINEAR,
        NON_LINEAR,

        COLORSPACE_MAX
      };

      /**
       * List of colorspace name strings ordered
       * in respect to 'Colorspace' enum ordering.
       * ColorspaceNames[Colorspace:: ...] corresponds
       * to the name string in the array for the
       * given colorspace enum value.
       */
      static std::string const ColorspaceNames[COLORSPACE_MAX];

      /**
       * Initializes an invalid texture instance.
       */
      Texture();

      /**
       * Deletes the resources held by the instance.
       */
      ~Texture();

      /**
       * Initializes the texture by loading an image from
       * disk and either trying to guess the color format
       * or trusts the user with given arguments.
       * @param colorspace 'linear' colorspace is used by
       * default when loading pixels from an image.
       * You can change this parameter to load the texture
       * in 'non-linear' colorspace.
       * @param pixel_format Use this parameter to override
       * automatic pixel format guessing. If this is left
       * as GL_INVALID_ENUM the function will try to guess
       * the pixel format based on detected color components.
       * @param colorspace_internal 'linear' colorspace is
       * used by default for storing the texture with OpenGL.
       * You can change this parameter to store the texture
       * in 'non-linear' colorspace.
       * @param pixel_format_internal Use this parameter to
       * override automatic pixel format guessing. If this
       * is left as GL_INVALID_ENUM the function will try to
       * guess the pixel format based on detected color
       * components.
       */
      dust_error_t initialize(
        std::string_view image_path,
        Colorspace colorspace = Colorspace::LINEAR,
        GLenum pixel_format = GL_INVALID_ENUM,
        Colorspace colorspace_internal = Colorspace::LINEAR,
        GLenum pixel_format_internal = GL_INVALID_ENUM);

      /**
       * @return The width of the texture in pixels.
       */
      dust_size_t get_width() const;

      /**
       * @return The height of the texture in pixels.
       */
      dust_size_t get_height() const;

      /**
       * Note: This is not necessarily the number of
       * components the texture is originally stored
       * with on the disk.
       * @return The number of color components the
       * texture is internally stored with by OpenGL.
       * This is determined during texture
       * initialization.
       */
      unsigned int get_color_components() const;

      /**
       * @return The internal OpenGL texture ID.
       */
      GLuint get_texture() const;

      /**
       * Tries to determine the OpenGL color format
       * based on the given hints.
       * @param color_components The number of color
       * components of a texture.
       * @param colorspace Linear or non-linear
       * colorspace.
       * @return One of the GL_[S*]R|G|B|[A/_ALPHA*]
       * enums on success, or GL_INVALID_ENUM on
       * failure.
       */
      static GLenum get_pixel_format(
        unsigned int color_components,
        Colorspace colorspace);

    private:

      /**
       * Width of the texture in pixels.
       */
      dust_size_t m_width;

      /**
       * Height of the texture in pixels.
       */
      dust_size_t m_height;

      /**
       * The number of color components detected by
       * stb_image.
       */
      unsigned int m_components;

      /**
       * OpenGL texture ID.
       */
      GLuint m_texture;

  };

}

#endif // DUST_GRAPHICS_TEXTURE_H
