#ifndef DUST_GRAPHICS_GL_HPP
#define DUST_GRAPHICS_GL_HPP

#include <dust/common.hpp>
#include <dust/utility/error.hpp>

#ifdef DUST_OS_WIN64
  #define GLFW_DLL
#else
  #error OS-specific preprocessor statement not implemented
#endif

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/matrix.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/ext/matrix_clip_space.hpp>

namespace DUST_NAMESPACE
{

  /**
   * Initializes GLFW.
   */
  DUST_API dust_error_t graphics_start();

  /**
   * @return true if GLFW was previously initialized,
   * false otherwise.
   */
  DUST_API bool glfw_is_initialized();

  /**
   * Constructs a perspective view matrix.
   * @param width The width of the perspective view in
   * pixels.
   * @param height The height of the perspective view
   * in pixels.
   * @param fov The field-of-view in degrees.
   * @param near The near clipping plane distance.
   * @param far The far clipping plane distance.
   * @return A perspective projection matrix based
   * on the given screen dimensions.
   */
  DUST_API glm::mat4 get_perspective_projection_matrix(
    dust_size_t width,
    dust_size_t height,
    GLfloat fov,
    GLfloat near,
    GLfloat far);

  /**
   * Constructs an orthographic view matrix.
   * @param width The width of the orthographic view in
   * pixels.
   * @param height The height of the orthographic view
   * @param near The near clipping plane distance.
   * @param far The far clipping plane distance.
   * in pixels.
   * @return An orthographic projection matrix based
   * on the given screen dimensions.
   */
  DUST_API glm::mat4 get_orthographic_projection_matrix(
    dust_size_t width,
    dust_size_t height,
    GLfloat near,
    GLfloat far);

  /**
   * Terminates GLFW.
   */
  DUST_API void graphics_end();

  /**
   * Clears the OpenGL color buffer and depth buffers.
   * Call this before doing any rendering.
   */
  DUST_API void gl_clear();

  /**
   * @return DUST_OK if no OpenGL errors happened, or
   * DUST_ERROR_OPENGL otherwise.
   */
  DUST_API dust_error_t gl_get_error();

  /**
   * Binds the VBO.
   */
  DUST_API void gl_bind_VBO(
    GLuint VBO);

  /**
   * Unbinds the VBO (binds 0 as the VBO).
   */
  DUST_API void gl_unbind_VBO();

  /**
   * Binds the VAO.
   */
  DUST_API void gl_bind_VAO(
    GLuint VAO);

  /**
   * Unbinds the VAO (binds 0 as the VAO).
   */
  DUST_API void gl_unbind_VAO();

}

#endif // DUST_GRAPHICS_GL_HPP
