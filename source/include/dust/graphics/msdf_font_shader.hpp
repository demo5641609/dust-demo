#ifndef DUST_GRAPHICS_MSDF_FONT_SHADER_HPP
#define DUST_GRAPHICS_MSDF_FONT_SHADER_HPP

#include <dust/common.hpp>
#include <dust/memory/sector.hpp>

#include <dust/graphics/gl.hpp>
#include <dust/graphics/shader.hpp>
#include <dust/graphics/model_shader.hpp>

/**
 * Default font vertex shader program path.
 */
#define DUST_MSDF_FONT_SHADER_SOURCE_VERTEX \
  "assets/shaders/msdf_font_vertex.glsl"

/**
 * Default font fragment shader program path.
 */
#define DUST_MSDF_FONT_SHADER_SOURCE_FRAGMENT \
  "assets/shaders/msdf_font_fragment.glsl"

/**
 * The number of floats added on top of model shader
 * vertex attributes when defining vertex attributes.
 */
#define DUST_MSDF_FONT_SHADER_EXTRA_VERTEX_FLOATS 2U

namespace DUST_NAMESPACE
{

  /**
   * Provides access to font shader attributes.
   */
  class MSDFFontShader : public ModelShader
  {
    public:

      /**
       * Initializes an invalid shader instance.
       */
      MSDFFontShader();

      /**
       * Reads and compiles the vertex and fragment shader.
       * @param string_sector Sector for filenames.
       * @param data_sector Sector for shader data.
       * @param vertex_shader_path Path to the vertex shader file.
       * @param fragment_shader_path Path to the fragment shader file.
       * @param extra_vertex_floats The additional
       * number of float values after the model and
       * font shader vertex attributes. This argument
       * is for shaders that build on top of the font
       * shader and enable extra attributes. The value
       * is added to the 'stride'.
       * @return DUST_OK on success, or ModelShader::initialize()
       * errors.
       */
      dust_error_t initialize(
        Sector& string_sector,
        Sector& data_sector,
        GLsizei extra_vertex_floats = 0,
        std::string_view vertex_shader_path = DUST_MSDF_FONT_SHADER_SOURCE_VERTEX,
        std::string_view fragment_shader_path = DUST_MSDF_FONT_SHADER_SOURCE_FRAGMENT);

      /**
       * Initializes vertex attributes for a VAO.
       * Does not bind or unbind a VAO, the user has to
       * bind and later unbind their own VAO.
       */
      dust_error_t initialize_VAO() const override;

      /**
       * @return Shader screen pixel range
       * uniform location.
       */
      GLuint get_screen_pixel_range_uniform_id() const;

      /**
       * Invalidates the object.
       */
      void done() override;

    protected:

      /**
       * @return The offset in bytes that the inheriting
       * shader needs to use as the "pointer" for the
       * first additional attribute when calling
       * 'glVertexAttribPointer()'.
       */
      dust_ptr_t get_vertex_pointer_offset() const override;

      /**
       * Sets attribute and uniform IDs to -1.
       */
      void invalidate() override;

    private:

      /**
       * Shader UV attribute location.
       */
      GLint m_a_UV_location;

      /**
       * Shader screen pixel range attribute location.
       */
      GLint m_screen_pixel_range_uniform_id;

  };

}

#endif // DUST_GRAPHICS_MSDF_FONT_SHADER_HPP
