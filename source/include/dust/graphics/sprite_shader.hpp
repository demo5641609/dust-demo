#ifndef DUST_GRAPHICS_SPRITE_SHADER_HPP
#define DUST_GRAPHICS_SPRITE_SHADER_HPP

#include <dust/common.hpp>
#include <dust/memory/sector.hpp>

#include <dust/graphics/gl.hpp>
#include <dust/graphics/shader.hpp>
#include <dust/graphics/model_shader.hpp>

/**
 * Default sprite vertex shader program path.
 */
#define DUST_SPRITE_SHADER_SOURCE_VERTEX \
  "assets/shaders/sprite_vertex.glsl"

/**
 * Default sprite fragment shader program path.
 */
#define DUST_SPRITE_SHADER_SOURCE_FRAGMENT \
  "assets/shaders/sprite_fragment.glsl"

namespace DUST_NAMESPACE
{

  /**
   * Provides access to sprite shader attributes.
   */
  class SpriteShader : public ModelShader
  {
    public:

      /**
       * Initializes an invalid shader instance.
       */
      SpriteShader();

      /**
       * Reads and compiles the vertex and fragment shader.
       * @param string_sector Sector for filenames.
       * @param data_sector Sector for shader data.
       * @param vertex_shader_path Path to the vertex shader file.
       * @param fragment_shader_path Path to the fragment shader file.
       * @return DUST_OK on success, or ModelShader::initialize()
       * errors.
       */
      dust_error_t initialize(
        Sector& string_sector,
        Sector& data_sector,
        GLsizei extra_vertex_floats = 0,
        std::string_view vertex_shader_path = DUST_SPRITE_SHADER_SOURCE_VERTEX,
        std::string_view fragment_shader_path = DUST_SPRITE_SHADER_SOURCE_FRAGMENT);

      /**
       * Initializes vertex attributes for a VAO.
       * Does not bind or unbind a VAO, the user has to
       * bind and later unbind their own VAO.
       */
      dust_error_t initialize_VAO() const override;

    protected:

      /**
       * @return The offset in bytes that the inheriting
       * shader needs to use as the "pointer" for the
       * first additional attribute when calling
       * 'glVertexAttribPointer()'.
       */
      dust_size_t get_vertex_pointer_offset() const override;

      /**
       * Sets attribute and uniform IDs to -1.
       */
      void invalidate() override;

    private:

      /**
       * Shader UV attribute location.
       */
      GLint m_a_UV_location;

  };

}

#endif // DUST_GRAPHICS_SPRITE_SHADER_HPP
