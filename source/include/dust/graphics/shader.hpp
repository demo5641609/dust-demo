#ifndef DUST_GRAPHICS_SHADER_HPP
#define DUST_GRAPHICS_SHADER_HPP

#include <dust/common.hpp>
#include <dust/memory/sector.hpp>
#include <dust/utility/file.hpp>
#include <dust/graphics/gl.hpp>

#include <string_view>

namespace DUST_NAMESPACE
{

  /**
   * Object for compiling a vertex and fragment shader.
   */
  class Shader
  {
    public:

      /**
       * Creates an invalid shader instance.
       */
      Shader();

      /**
       * Deletes the resources used by the shader and
       * invalidates the instance.
       */
      ~Shader();

      /**
       * Loads the vertex and fragment shaders and
       * initializes
       * the instance.
       * @param string_sector Sector for filenames.
       * @param data_sector Sector for shader data.
       * @param vertex_shader_path Path to the vertex
       * shader file.
       * @param fragment_shader_path Path to the fragment
       * shader file.
       * @return DUST_OK on success, or DUST_ERROR_OPENGL_SHADER
       * on failure.
       */
      dust_error_t load(
        Sector& string_sector,
        Sector& data_sector,
        std::string_view vertex_source_path,
        std::string_view fragment_source_path);

      /**
       * @return The OpenGL ID of the shader program.
       */
      GLuint get_program() const;

      /**
       * Wrapper for 'glUseProgram(get_program());'
       */
      virtual void use();

      /**
       * Deletes the resources used by the object.
       */
      virtual void done();

    protected:

      /**
       * Sets internal members to invalid values.
       */
      virtual void invalidate();

    private:

      /**
       * Compiles and links the vertex and fragment shaders.
       * @param vertex_source String containing vertex shader code.
       * Must be null-terminated.
       * @param fragment_source String containing vertex shader code.
       * Must be null-terminated.
       * @return DUST_OK on success, or DUST_ERROR_OPENGL_SHADER
       * on failure.
       */
      dust_error_t initialize(
        std::string_view vertex_source,
        std::string_view fragment_source);

      /**
       * Prints the OpenGL error log if an OpenGL error was raised.
       * @return DUST_OK on success, or DUST_ERROR_OPENGL_SHADER
       * on failure.
       */
      dust_error_t get_shader_error(
        GLuint shader) const;

      /**
       * Prints the shader compilation error log if a shader
       * program failed to link.
       * @return DUST_OK on success, or DUST_ERROR_OPENGL_SHADER
       * on failure.
       */
      dust_error_t get_link_error() const;

    private:

      /**
       * OpenGL shader program ID.
       */
      GLuint m_program;

  };

}

#endif // DUST_GRAPHICS_SHADER_HPP
