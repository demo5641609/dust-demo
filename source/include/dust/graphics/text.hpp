#ifndef DUST_GRAPHICS_TEXT_HPP
#define DUST_GRAPHICS_TEXT_HPP

#include <dust/common.hpp>
#include <dust/memory/buffer.hpp>
#include <dust/memory/allocator.hpp>

#include <dust/graphics/gl.hpp>
#include <dust/graphics/shader.hpp>

#include <dust/graphics/model_shader.hpp>
#include <dust/graphics/msdf_font_shader.hpp>

#include <dust/graphics/glyph.hpp>
#include <dust/graphics/msdf_font.hpp>

#include <dust/game/model.hpp>
#include <dust/game/camera.hpp>

#include <glm/matrix.hpp>

#include <string_view>

/**
 * Default maximum number of characters to store in a text
 * instance, not including the null-terminating character.
 */
#define DUST_TEXT_MAX_CHARS_DEFAULT 32

/**
 * Default render size for text instances.
 */
#define DUST_TEXT_SIZE_DEFAULT (32.0f)

/**
 * Default color for text instances - White with 1.0 alpha.
 */
#define DUST_TEXT_COLOR_DEFAULT { 1.0f, 1.0f, 1.0f, 1.0f }

namespace DUST_NAMESPACE
{

  /**
   * Object providing API for rendering text.
   */
  class Text : public Model
  {
    public:

      /**
       * Initializes an invalid text instance.
       */
      Text();

      /**
       * Invokes 'done()'.
       */
      ~Text();

      /**
       * Initializes a text instance for rendering glyphs
       * with the given font.
       * @param string_allocator Allocator for the internal
       * string buffer.
       * @param data_allocator Allocator for the vertex data
       * buffer.
       * @param font The font with which to render glyphs.
       * @param max_characters The maximum number of characters
       * to reserve storage for, not including the
       * null-terminating character.
       * @param size The render size for glyphs. Not necessarily
       * synonymous with the pixel size of rendered glyphs but
       * may be close.
       * @param color The color of the rendered glyphs. RGBA
       * values in range of [0.0f, 1.0f].
       * @return DUST_OK on success, or DUST_ERROR_TEXT_INIT on
       * failure.
       */
      dust_error_t initialize(
        Allocator& string_allocator,
        Allocator& data_allocator,
        MSDFFont& font,
        GLfloat size = DUST_TEXT_SIZE_DEFAULT,
        glm::vec4 color = DUST_TEXT_COLOR_DEFAULT,
        dust_size_t max_characters = DUST_TEXT_MAX_CHARS_DEFAULT);

      /**
       * Sets the internal string based on the C-format.
       * Updates the VBO and length of the instance.
       * @param format A C-style string format.
       * @param ... Parameters for the string format.
       */
      void set_string(
        std::string_view format,
        ...);

      /**
       * Sets the render size for the text.
       * @param size The size of the text when rendering.
       * Not necessarily synonymous with the pixel size
       * of rendered glyphs but may be close.
       */
      void set_size(
        GLfloat size);

      /**
       * Note: RGBA values in range of [0.0f, 1.0f].
       * @return A mutable reference to the internal color
       * member.
       */
      glm::vec4& get_color();

      /**
       * @return The color of the text.
       */
      glm::vec4 const& get_color() const;

      /**
       * @return The render size of the text.
       */
      GLfloat get_size() const;

      /**
       * @return The font with which the text is rendered.
       */
      MSDFFont *get_font() const;

      /**
       * The internal string of the text.
       */
      std::string_view get_string() const;

      /**
       * Warning: This function does not check that the text
       * has been previously initialized. Taking the size of
       * an uninitialized text could overflow into huge
       * numbers.
       * @return The maximum number of characters that the
       * text can store, not including the null-terminating
       * character.
       */
      dust_size_t get_max_characters() const;

      /**
       * @return The length of the internal string. Length
       * does not include the null-terminating character.
       */
      dust_size_t get_length() const;

      /**
       * Applies the shader uniforms and batch-draws glyphs.
       * @param camera Camera object from which to get the
       * projection and view matrices.
       */
      void draw(
        Camera const& camera);

      /**
       * Deletes the VBO and deallocates the string and
       * glyph buffers. Invalidates the instance.
       */
      void done();

    protected:

      /**
       * Sets all member variables to unusable values. Does
       * not touch the referenced font instance.
       */
      void invalidate();

      /**
       * Applies the MVP matrix uniform, the color uniform
       * and the screen pixel range uniform.
       * @param camera Camera object from which to get the
       * projection and view matrices.
       */
      void apply_shader_uniforms(
        Camera const& camera) const;

    private:

      /**
       * Color of the rendered text.
       */
      glm::vec4 m_color;

      /**
       * C-string which to render. Is null-terminated.
       */
      Buffer<char> m_string;

      /**
       * Array containing VBO data that gets submitted to the
       * GPU.
       */
      Buffer<Glyph::Vertex> m_glyphs;

      /**
       * Allocator for the string buffer.
       */
      Allocator *m_string_allocator;

      /**
       * Allocator for the VBO buffer.
       */
      Allocator *m_data_allocator;

      /**
       * The font with which to render the text.
       */
      MSDFFont *m_font;

      /**
       * Number of characters that are currently used and will
       * be rendered. Does not include null-terminator character.
       */
      dust_size_t m_length;

      /**
       * Scaling for the rendered text. Not to be confused with
       * string size or length.
       */
      GLfloat m_size;

      /**
       * Vertex buffer object for uploading glyph vertex data.
       */
      GLuint m_VBO;

      /**
       * Vertex array object for keeping track of vertex
       * attributes.
       */
      GLuint m_VAO;

  };

}

#endif // DUST_GRAPHICS_TEXT_HPP
