#ifndef DUST_GRAPHICS_SPRITE_H
#define DUST_GRAPHICS_SPRITE_H

#include <dust/common.hpp>
#include <dust/game/model.hpp>
#include <dust/game/camera.hpp>
#include <dust/graphics/gl.hpp>
#include <dust/graphics/sprite_shader.hpp>
#include <dust/graphics/texture.hpp>

/**
 * Default color for sprite instances - White with 1.0 alpha.
 * Pure white doesn't affect pixel colors from the sprite
 * texture.
 */
#define DUST_SPRITE_COLOR_DEFAULT { 1.0f, 1.0f, 1.0f, 1.0f }

namespace DUST_NAMESPACE
{

  /**
   * Object providing API for rendering texture
   * regions or whole textures (images).
   */
  class Sprite : public Model
  {
    public:

      /**
       * Simple struct to make generating vertices
       * easier.
       */
      struct Vertex
      {

        /**
         * 3D position of the vertex.
         */
        glm::vec3 position;

        /**
         * Color of the vertex.
         */
        glm::vec4 color;

        /**
         * UV position in the texture.
         */
        glm::vec2 UV;

      };

      using SpriteVertex = Sprite::Vertex;

      /**
       * Initializes an invalid sprite instance.
       */
      Sprite();

      /**
       * Initializes a sprite instance which uses a
       * region of the given texture for rendering.
       * To use the entire texture for rendering
       * leave width and height as 0.
       * @param texture Texture to use for rendering.
       * @param color Color of the sprite.
       * @param width Width of the sprite in pixels.
       * @param height Height of the sprite in pixels.
       * @param UV_x Horizontal offset for the texture
       * region.
       * @param UV_y Vertical offset for the texture
       * region.
       */
      dust_error_t initialize(
        Texture const& texture,
        glm::vec4 color = DUST_SPRITE_COLOR_DEFAULT,
        dust_size_t width = 0,
        dust_size_t height = 0,
        dust_size_t UV_x = 0,
        dust_size_t UV_y = 0);

      /**
       * @return The shared sprite shader instance.
       */
      static SpriteShader& get_shader();

      /**
       * Applies the shader uniforms and draws the sprite.
       * @param camera Camera object from which to get the
       * projection and view matrices.
       */
      void draw(
        Camera const& camera) const;

    private:

      /**
       * Shared sprite shader instance.
       */
      static SpriteShader m_shader;

      /**
       * Color which mixes with the pixels of
       * the texture
       */
      glm::vec4 m_color;

      /**
       * Texture region position.
       * Should be values from [0.0f, 1.0f].
       */
      glm::vec2 m_UV_pos;

      /**
       * Texture region size.
       * Should be values from [0.0f, 1.0f].
       */
      glm::vec2 m_UV_size;

      /**
       * External texture resource.
       */
      Texture const *m_texture;

      /**
       * Vertex buffer object for storing vertex data.
       */
      GLuint m_VBO;

      /**
       * Vertex array object for keeping track of vertex
       * attributes.
       */
      GLuint m_VAO;

  };

}

#endif // DUST_SPRITE_TEXTURE_H
