#ifndef DUST_GRAPHICS_MSDF_FONT_HPP
#define DUST_GRAPHICS_MSDF_FONT_HPP

#include <dust/common.hpp>
#include <dust/memory/array.hpp>
#include <dust/memory/allocator.hpp>

#include <dust/graphics/gl.hpp>
#include <dust/graphics/msdf_font_shader.hpp>
#include <dust/graphics/glyph.hpp>

#include <string_view>

namespace DUST_NAMESPACE
{

  /**
   * Object which provides simple API for loading an
   * MSDF font. This object is used by text instances
   * to render text.
   */
  class MSDFFont
  {
    public:

      /**
       * Initializes an invalid MSDF font instance.
       */
      MSDFFont();

      /**
       * Deletes the resources held by the instance.
       */
      ~MSDFFont();

      /**
       * Initializes a new MSDF font.
       * @param string_allocator Allocator for strings.
       * @param data_allocator Allocator for data arrays.
       * @param atlas_filename Image containing glyph
       * multi-channel distance fields
       * @param csv_filename CSV file containing glyph
       * metrics.
       * @param field_size Glyph size used when generating
       * the MSDF atlas. Usually 32x32 or 48x48.
       * @param pixel_range Pixel range used when
       * generating the MSDF atlas.
       */
      dust_error_t initialize(
        Allocator& string_allocator,
        Allocator& data_allocator,
        std::string_view atlas_filename,
        std::string_view csv_filename,
        GLuint field_size,
        GLuint pixel_range = 2);

      /**
       * Initializes the shared MSDF font shader.
       * @return DUST_OK on success, or
       * DUST_ERROR_OPENGL_SHADER on failure.
       */
      static dust_error_t initialize_shader();

      /**
       * @return The shared MSDF font shader.
       */
      static MSDFFontShader& get_shader();

      /**
       * @param id Character code which identifies
       * a glyph.
       * @return The glyph corresponding to the
       * character 'id'.
       */
      Glyph const& get_glyph(
        dust_size_t id) const;

      /**
       * Calculates the screen pixel range based on
       * the given render size.
       * @param render_size The desired approximate
       * pixel-size of rendered glyphs.
       * @return Pixel range value in respect to the
       * given render size.
       */
      GLfloat get_screen_pixel_range(
        GLfloat render_size) const;

      /**
       * Binds the font's MSDF texture.
       */
      void bind_atlas_texture() const;

    private:

      /**
       * Shared MSDF font shader.
       */
      static MSDFFontShader m_shader;

      /**
       * Array for storing all glyph definitions.
       */
      Array<Glyph> m_glyphs;

      /**
       * Allocator for the glyph array.
       */
      Allocator *m_allocator;

      /**
       * The largest of the plane size values.
       * Plane sizes define the glyph quad shape.
       */
      GLfloat m_plane_size_max;

      /**
       * The referent pixel-size of glyphs.
       * This is determined when generating an
       * MSDF font image.
       */
      GLfloat m_field_size;

      /**
       * An oddity.
       * This is determined when generating an
       * MSDF font image.
       */
      GLfloat m_pixel_range;

      /**
       * Width of the MSDF image in pixels.
       */
      GLsizei m_atlas_width;

      /**
       * Height of the MSDF image in pixels.
       */
      GLsizei m_atlas_height;

      /**
       * OpenGL ID of the atlas texture.
       */
      GLuint m_texture;

  };

}

#endif // DUST_GRAPHICS_MSDF_FONT_HPP
