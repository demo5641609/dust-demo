#ifndef DUST_GRAPHICS_GLYPH_HPP
#define DUST_GRAPHICS_GLYPH_HPP

#include <dust/graphics/gl.hpp>

#define DUST_GLYPH_LEFT   0
#define DUST_GLYPH_BOTTOM 1
#define DUST_GLYPH_RIGHT  2
#define DUST_GLYPH_TOP    3

namespace DUST_NAMESPACE
{

  /**
   * Object for describing a character for rendering.
   */
  class Glyph
  {
    public:

      struct Vertex
      {
        /**
         * 3D position of the vertex.
         */
        glm::vec3 position;

        /**
         * Color of the glyph.
         */
        glm::vec4 color;

        /**
         * UV coordinate in the font atlas.
         */
        glm::vec2 UV;
      };

      /**
       * Initializes an invalid glyph instance.
       * All member variables are set to 0.0f.
       */
      Glyph();

      /**
       * 
       * @param advance Horizontal offset applied
       * to a glyph being rendered after this one.
       * @param plane_size 4 floats representing
       * the plane dimensions of the glyph.
       * @param UVs UV coordinates of the glyph
       * texture in the font's texture atlas.
       * @return DUST_OK.
       */
      dust_error_t initialize(
        GLfloat advance,
        GLfloat const *plane_size,
        GLfloat const *UVs);

      /**
       * @return 4-float array representing the
       * glyph quad sizes.
       *
       * Order is:
       *
       *   0: Left
       *   1: Bottom
       *   2: Right
       *   3: Top
       */
      GLfloat const *get_plane_size() const;

      /**
       * @return 4-float array representing the
       * glyph UV sizes.
       *
       * Order is:
       *
       *   0: Left
       *   1: Bottom
       *   2: Right
       *   3: Top
       */
      GLfloat const *get_UVs() const;

      /**
       * @return Horizontal offset for the next character.
       */
      GLfloat get_advance() const;

    private:

      /**
       * Left, bottom, right and top plane size.
       * Multiplied with text size when drawing.
       * 
       * Order is:
       * 
       *   0: Left
       *   1: Bottom
       *   2: Right
       *   3: Top
       */
      GLfloat m_plane_size[4];

      /**
       * UV coordinates of the glyph MSDF texture
       * in the font's texture atlas.
       * 
       * Order is:
       * 
       *   0: Left
       *   1: Bottom
       *   2: Right
       *   3: Top
       */
      GLfloat m_UVs[4];

      /**
       * Horizontal offset to apply to the next
       * glyph when rendering text.
       */
      GLfloat m_advance;

  };

}

#endif // DUST_GRAPHICS_GLYPH_HPP
