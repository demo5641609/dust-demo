#ifndef DUST_UTILITY_FILE_HPP
#define DUST_UTILITY_FILE_HPP

#include <dust/common.hpp>
#include <dust/memory/buffer.hpp>
#include <dust/memory/allocator.hpp>

#include <fstream>
#include <string_view>

/**
 * Used to indicate that the file is currently open or closed.
 */
#define DUST_FILE_OPEN   0x01

/**
 * File initialization flag that enables reading.
 */
#define DUST_FILE_READ   0x02

/**
 * File initialization flag that enables writing.
 * Does not make the file clear its content upon opening. Use
 * the DUST_FILE_CLEAR flag if you want the file to truncate.
 */
#define DUST_FILE_WRITE  0x04

/**
 * File initialization flag that makes the file truncate all
 * content upon opening.
 * 
 * Has no effect is the file is read-only mode.
 */
#define DUST_FILE_CLEAR  0x8

/**
 * File initialization flag that makes the file set the initial
 * position at the end.
 */
#define DUST_FILE_AT_END 0x10

/**
 * File initialization flag that makes the file open in 'binary'
 * mode.
 */
#define DUST_FILE_BINARY 0x20

namespace DUST_NAMESPACE
{

  /**
   * Object for manipulating a file through simplified API.
   */
  class File
  {
    public:

      /**
       * Constructs an invalid file object.
       */
      File();

      /**
       * Closes the internal file and deallocates the
       * filename with the given allocator.
       */
      ~File();

      /**
       * Initializes the file for usage. Does not open the
       * file or check for its existence.
       * @param allocator Allocator with which to allocate
       * and deallocate the internal filename buffer.
       * @param filename A valid file path string.
       * @param flags A combination of DUST_FILE_READ,
       * DUST_FILE_WRITE, DUST_FILE_CLEAR and DUST_FILE_BINARY.
       * @return DUST_OK on success, or DUST_ERROR_FILE_INIT
       * on failure.
       */
      dust_error_t initialize(
        Allocator& string_allocator,
        Allocator& buffer_allocator,
        std::string_view filename,
        uint32_t flags);

      /**
       * Opens the file with the options set at initialization.
       * @return DUST_OK on success, or DUST_ERROR_FILE_OPEN
       * on failure.
       */
      dust_error_t open();

      /**
       * Closes the internal std::fstream;
       */
      void close();

      /**
       * @return True if the file exists on the filesystem,
       * false otherwise.
       */
      bool exists() const;

      /**
       * @return The byte-size of the file as reported
       * by the filesystem.
       */
      size_t get_size() const;

      /**
       * @return A view of the internal filename.
       */
      std::string_view get_filename() const;

      /**
       * @return The internal std::fstream of the file.
       */
      std::fstream const& get_stream() const;

      /**
       * @return The internal std::fstream of the file.
       */
      std::fstream& get_stream();

      /**
       * @return The allocator that the File uses to
       * allocate its internal filename.
       */
      Allocator *get_string_allocator() const;

      /**
       * @return The allocator that this File uses to
       * allocate 'read' buffers.
       */
      Allocator *get_buffer_allocator() const;

      /**
       * @param flags A combination of
       * DUST_FILE_[ WRITE | READ | CLEAR | BINARY | AT_END ]
       * options.
       * @return The C++ fstream flags based on the
       * DUST_FILE_* bit options.
       */
      static std::ios_base::openmode get_openmode(
        uint8_t flags);

      /**
       * Allocates a data buffer and reads the contents of
       * the file into the buffer. You are responsible for
       * deallocating the buffer with the File's buffer
       * allocator. You can get the allocator by calling
       * 'get_buffer_allocator()'. If the file is not in
       * 'binary' mode a null-terminating character will be
       * added at the end of the data. When the file is in
       * 'binary' mode the size of the buffer exactly
       * matches the number of bytes read. It is possible
       * that the buffer is bigger than the actual content
       * that was read but any extra bytes are set to 0x0.
       * 'open()'s the file if it was not previously open.
       * @return A valid data buffer allocated with the
       * File's 'data allocator' on success, or an invalid
       * buffer instance on failure.
       */
      Buffer<dust_byte_t> read();

      /**
       * Allocates a data buffer and reads the contents of
       * the file into the buffer. If the file is not in
       * 'binary' mode a null-terminating character will be
       * added at the end of the data. When the file is in
       * 'binary' mode the size of the buffer exactly
       * matches the number of bytes read. It is possible
       * that the buffer is bigger than the actual content
       * that was read but any extra bytes are set to 0x0.
       * 'open()'s the file if it was not previously open.
       * @param allocator User-provided allocator fot the
       * data buffer. You are responsible for deallocating
       * the returned buffer if it is valid.
       * @return A valid data buffer allocated with the
       * File's 'data allocator' on success, or an invalid
       * buffer instance on failure.
       */
      Buffer<dust_byte_t> read(
        Allocator& allocator);

      /**
       * Writes the given data to the file.
       * @param data A buffer with a pointer to data and
       * byte-size to output to the file.
       */
      void write(
        Buffer<dust_byte_t> const& data);

      /**
       * Writes the given string to the file.
       * @param str The string which to output to the file.
       */
      void write(
        std::string_view str);

      /**
       * Clears the contents of the file. Keeps the file
       * open if it was opened beforehand.
       * @return DUST_OK on success, or DUST_FILE_OPEN on
       * failure.
       */
      dust_error_t clear();

    private:

      /**
       * Storage for the original filename.
       */
      Buffer<char> m_filename;

      /**
       * Internal C++ file stream
       */
      std::fstream m_file;

      /**
       * Allocator for the filename.
       */
      Allocator *m_string_allocator;

      /**
       * Allocator for 'read' buffers.
       */
      Allocator *m_buffer_allocator;

      /**
       * Options describing behavior upon opening and
       * internal state storage.
       */
      uint8_t m_flags;

  };

}

#endif // DUST_UTILITY_FILE_HPP
