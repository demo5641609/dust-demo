#ifndef DUST_UTILITY_CLOCK_HPP
#define DUST_UTILITY_CLOCK_HPP

#include <dust/common.hpp>

#include <chrono>

namespace DUST_NAMESPACE
{

  /**
   * Object for storing a point in time and measuring
   * the elapsed time between clocks and timepoints.
   */
  class Clock
  {
    public:

      /**
       * Type used by clocks internally to store time.
       */
      using TimePoint = std::chrono::steady_clock::time_point;

      /**
       * Initializes the clock with current time.
       */
      Clock();

      /**
       * @return The current time.
       */
      static TimePoint now();

      /**
       * Sets the clock's timepoint to current time.
       */
      void start();

      /**
       * @return The timepoint at which the clock was
       * started.
       */
      TimePoint get_timepoint() const;

      /**
       * @return The time elapsed since the clock was
       * started in milliseconds.
       */
      float get_elapsed_time() const;

      /**
       * @return The time elapsed since the clock was
       * started in seconds.
       */
      float get_delta_time() const;

      /**
       * Calculates the elapsed time from the time
       * point 'from' to the time point 'to' in
       * milliseconds.
       * @param from The moment from which to measure
       * time.
       * @param to The moment to which to measure time.
       * @return The time passed between the 'from' and
       * 'to' timepoints in milliseconds.
       */
      static float elapsed(
        TimePoint from,
        TimePoint to);

      /**
       * Calculates the elapsed time from when the 'from'
       * clock was started to when the 'to' was started
       * (or set to 'Clock::now()').
       * @param from The moment from which to measure
       * time.
       * @param to The moment to which to measure time.
       * @return The time passed between the 'from' and
       * 'to' clocks in milliseconds.
       */
      static float elapsed(
        Clock const& from,
        Clock const& to);

    private:

      /**
       * A precise point in time.
       */
      TimePoint m_clock;

  };

}

#endif // DUST_UTILITY_CLOCK_HPP
