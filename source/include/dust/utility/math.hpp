#ifndef DUST_UTILITY_MATH_HPP
#define DUST_UTILITY_MATH_HPP

#include <dust/common.hpp>

#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

#include <cmath>

/**
 * A rather precise PI constant.
 */
#define DUST_PI   (3.1415926535897932384626433f)

/**
 * A rather precise PI constant as a double.
 */
#define DUST_PI_D (3.1415926535897932384626433)

/**
 * Converts radians to degrees as floats.
 */
#define DUST_RAD_TO_DEG(r) (((float) r) * (180.0f / DUST_PI))

/**
 * Converts degrees to radians as floats.
 */
#define DUST_DEG_TO_RAD(d) (((float) d) * (DUST_PI / 180.0f))

/**
 * Converts radians to degrees as doubles.
 */
#define DUST_RAD_TO_DEG_D(r) (((double) r) * (180.0 / DUST_PI_D))

/**
 * Converts degrees to radians as doubles.
 */
#define DUST_DEG_TO_RAD_D(d) (((double) d) * (DUST_PI_D / 180.0))

/**
 * The difference tolerance to apply when calculating
 * the rotation axis in quaternion math. Helps avoid
 * problems when the rotation axis is 'too' parallel
 * between two vectors.
 */
#define DUST_QUAT_AXIS_TOLERANCE (0.001f)

namespace DUST_NAMESPACE
{

  /**
   * Calculates the rotation quaternion between the
   * origin and the target. The rotation points from
   * the origin to the target.
   * @param origin_position The position from which
   * to look.
   * @param target_position The position at which to
   * look at.
   * @return A quaternion describing the rotation
   * from origin to target.
   */
  DUST_API glm::quat look_at(
    glm::vec3 const& origin_position,
    glm::vec3 const& target_position);

}

#endif // DUST_UTILITY_MATH_HPP
