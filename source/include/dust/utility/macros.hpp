#ifndef DUST_UTILITY_MACROS_H
#define DUST_UTILITY_MACROS_H

#define DUST_MACRO_NAME(macro)  #macro
#define DUST_MACRO_VALUE(macro) DUST_MACRO_NAME(macro)

#define DUST_API       extern
#define DUST_API_LOCAL static

#define DUST_UNUSED (void)

#endif // DUST_UTILITY_MACROS_H
