#ifndef DUST_COMMON_HPP
#define DUST_COMMON_HPP

#include <cstddef>
#include <cstdint>

#ifndef DUST_NAMESPACE
  #define DUST_NAMESPACE dust
#endif

#ifdef DUST_DEBUG

  #ifndef DUST_VERSION_MAJOR
    #define DUST_VERSION_MAJOR 0
  #endif

  #ifndef DUST_VERSION_MINOR
    #define DUST_VERSION_MINOR 0
  #endif

  #ifndef DUST_VERSION_PATCH
    #define DUST_VERSION_PATCH 0
  #endif

  #ifndef DUST_VERSION_BUILD
    #define DUST_VERSION_BUILD 0
  #endif

  #ifndef DUST_VERSION
    #define DUST_VERSION "0.0.0"
  #endif

  #ifndef DUST_VERSION_FULL
    #define DUST_VERSION_FULL "0.0.0-0"
  #endif

#else // DUST_DEBUG

  #if !defined(DUST_VERSION) || !defined(DUST_VERSION_FULL)
    #error Game version not defined!
  #endif

#endif // DUST_DEBUG

typedef std::byte      dust_byte_t;
typedef std::size_t    dust_size_t;
typedef std::intmax_t dust_ssize_t;

#include <dust/utility/macros.hpp>
#include <dust/utility/error.hpp>

namespace DUST_NAMESPACE
{

}

#endif // DUST_COMMON_HPP
