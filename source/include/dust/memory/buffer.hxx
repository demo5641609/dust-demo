#ifndef DUST_MEMORY_BUFFER_HXX
#define DUST_MEMORY_BUFFER_HXX

#include <dust/common.hpp>
#include <dust/memory.hpp>

#include <utility> // std::move
#include <cstddef> // dust::memory_move
#include <cstring> // memset

namespace DUST_NAMESPACE
{

  template<class T, class S>
  Buffer<T, S>::Buffer()
  {
    invalidate();
  }

  template<class T, class S>
  Buffer<T, S>::Buffer(Buffer<T, S>&& other)
  {
    take(other.yield());
  }

  template<class T, class S>
  Buffer<T, S>::Buffer(S const size, T *data)
  {
    take(Buffer<T, S>::create(size, data));
  }

  template<class T, class S>
  Buffer<T, S> Buffer<T, S>::null()
  {
    Buffer<T, S> b;

    return b;
  }

  template<class T, class S>
  Buffer<T, S> Buffer<T, S>::create(S size, T *data)
  {
    Buffer<T, S> b;

    if ((size > 0) && (data != nullptr))
    {
      b.m_ptr = data;
      b.m_size = size;
    }
    else
    {
      // Initialize as an invalid buffer.
      b.invalidate();
    }
    
    return b;
  }

  template<class T, class S>
  Buffer<T, S>& Buffer<T, S>::swap(Buffer<T, S>&& other)
  {
    Buffer<T, S> temp = other.yield();

    other = *this;
    *this = temp.yield();

    return *this;
  }

  template<class T, class S>
  Buffer<T, S>& Buffer<T, S>::copy_from(Buffer<T, S> const& other)
  {
    if (m_size <= other.get_size())
    {
      dust::memory_move(m_ptr, other.get_ptr(), other.get_size());
    }

    return *this;
  }

  template<class T, class S>
  Buffer<T, S>& Buffer<T, S>::invalidate()
  {
    m_ptr = nullptr;
    m_size = S();

    return *this;
  }

  template<class T, class S>
  Buffer<T, S>&& Buffer<T, S>::yield()
  {
    return std::move(*this);
  }

  template<class T, class S>
  Buffer<T, S>& Buffer<T, S>::take(Buffer<T, S>&& other)
  {
    m_ptr = other.get_ptr();
    m_size = other.get_size();

    other.invalidate();

    return *this;
  }

  template<class T, class S>
  Buffer<T, S>& Buffer<T, S>::clear()
  {
    std::memset(m_ptr, 0, m_size);

    return *this;
  }

  template<class T, class S>
  T *Buffer<T, S>::get_ptr()
  {
    return m_ptr;
  }

  template<class T, class S>
  T const *Buffer<T, S>::get_ptr() const
  {
    return m_ptr;
  }

  template<class T, class S>
  S Buffer<T, S>::get_size() const
  {
    return m_size;
  }

  template<class T, class S>
  S Buffer<T, S>::get_capacity() const
  {
    return m_size / sizeof(T);
  }

  template<class T, class S>
  bool Buffer<T, S>::is_valid() const
  {
    return (m_ptr != nullptr) && (m_size > 0);
  }

  template<class T, class S>
  Buffer<T, S>& Buffer<T, S>::operator=(Buffer<T, S>&& other)
  {
    take(other.yield());

    return *this;
  }

}

#endif // DUST_MEMORY_BUFFER_HXX
