#ifndef DUST_MEMORY_PAGE_HPP
#define DUST_MEMORY_PAGE_HPP

#include <dust/common.hpp>
#include <dust/memory/array.hpp>
#include <dust/memory/allocator.hpp>

#define DUST_PAGE_BLOCKS_S         32
#define DUST_PAGE_BLOCKS_EXPAND_S  16

#define DUST_PAGE_BLOCKS_M        128
#define DUST_PAGE_BLOCKS_EXPAND_M 256

#define DUST_PAGE_BLOCKS_L        512
#define DUST_PAGE_BLOCKS_EXPAND_L 512

namespace DUST_NAMESPACE
{

  /**
   * Smallest memory management object used by sectors.
   */
  class Page : public Allocator
  {
    public:

      using PageBlock = Buffer<dust_byte_t, dust_size_t>;
      using PageBlocks = Array<PageBlock>;

      /**
       * If this isn't explicitly specified the class won't
       * be able to call 'allocate<T>(...)'
       */
      using Allocator::allocate;

      Page();

      /**
       * Initializes the page by taking ownership of the block
       * buffer. Allocates bookkeeping arrays from the 'meta' sector.
       * @param meta_allocator The allocator with which to
       * allocate bookkeeping arrays.
       */
      dust_error_t initialize(
        Allocator &meta_allocator,
        Buffer<dust_byte_t>&& data,
        dust_size_t blocks_size = DUST_PAGE_BLOCKS_M,
        dust_size_t blocks_expand_size = DUST_PAGE_BLOCKS_EXPAND_M);

      /**
       * Allocates a block of memory of 'size' bytes, aligned
       * to 'align' bytes. If 'align' is 0, alignment will be
       * suitable for any object.
       * @param size Byte-size of the desired memory block.
       * @param align Byte-alignment of the desired memory
       * block. Use 'alignof(T)' to get the alignment of any
       * type, where T is the type name.
       * @return An aligned block of memory, or nullptr on
       * failure.
       */
      Buffer<dust_byte_t> allocate(
        dust_size_t size,
        dust_size_t align = 0) override;

      /**
       * Frees the previously allocated piece of memory.
       * Does nothing if the pointer doesn't match an
       * allocation made by this instance.
       * @return DUST_OK if the page freed the pointer, or DUST_SKIP if the pointer doesn't belong to the page.
       */
      dust_error_t free(void const * t_ptr) override;

      /**
       * @return 'true' if there are no allocated blocks, or 'false' otherwise.
       */
      bool is_empty() const;

      /**
       * @return 'true' is there is no more free memory to allocate from, or 'false' otherwise.
       */
      bool is_full() const;

      /**
       * @return 'true' is the internal Buffer<T> is valid, or 'false' otherwise.
       */
      bool is_valid() const;

      /**
       * @return Reference to the internal memory buffer of the page.
       */
      PageBlock const& get_memory();

      /**
       * @return The number of block descriptors that the bookkeeping
       * arrays can store.
       */
      dust_size_t get_array_size() const;

      /**
       * @return The number of block descriptors that the bookkeeping
       * arrays expand by.
       */
      dust_size_t get_expand_size() const;

      /**
       * Removes all allocations made by the page, leaving it in
       * the initial state of having only one large free block.
       * @return This page.
       */
      Page& reset();

      /**
       * Deallocates the page block arrays.
       * Does not deallocate the internal memory.
       * Clears the block descriptor arrays.
       */
      void release();

    protected:

      friend class MemoryServer;
      friend class Sector;

      /**
       * Initializes the page by taking ownership of the block
       * buffer. The page allocates bookkeeping arrays from itself.
       * This function will only store a pointer to the meta sector
       * for later usage. The sector won't be used to allocate memory
       * in this function.
       * @param meta_sector The sector with which to allocate bookkeeping arrays.
       */
      dust_error_t initialize_self(
        Allocator &meta_allocator,
        Buffer<dust_byte_t>&& data,
        dust_size_t blocks_size = DUST_PAGE_BLOCKS_M,
        dust_size_t blocks_expand_size = DUST_PAGE_BLOCKS_EXPAND_M);

    private:

      dust_error_t expand_blocks_array(
        Array<PageBlock>& blocks_array);

    private:

      /**
       * Array of currently unused blocks.
       */
      PageBlocks m_free_blocks;

      /**
       * Array of currently used (allocated)
       * blocks.
       */
      PageBlocks m_allocated_blocks;

      /**
       * Internal memory over which the page
       * performs allocations.
       */
      PageBlock m_memory;

      /**
       * External allocator for allocating
       * block descriptor arrays.
       */
      Allocator *m_meta_allocator;

      /**
       * The size by which to expand the
       * block descriptor arrays.
       */
      dust_size_t m_expand_size;

  };

}

#endif // DUST_MEMORY_PAGE_HPP
