#ifndef DUST_MEMORY_BUFFER_HPP
#define DUST_MEMORY_BUFFER_HPP

#include <dust/common.hpp>

namespace DUST_NAMESPACE
{

  /**
   * Forward declaration of the memory sector page class.
   * Used to declare friendship with the buffer class.
   */
  class Page;

  /**
   * Object for encapsulating the most basic data storage.
   */
  template<class T = dust_byte_t, class S = dust_size_t>
  class Buffer
  {
    public:
      /**
       * Constructs the buffer as 'invalid' (0 size
       * and null data). This is the same as creating
       * a 'null' buffer.
       */
      Buffer();

      /**
       * Constructs the buffer by taking ownership of
       * the 'other' buffer's data. Invalidates the
       * 'other' buffer.
       */
      Buffer(Buffer<T, S>&& other);

      /**
       * Constructs the buffer with given parameters
       * if the data pointer and size are valid values,
       * otherwise the buffer will be invalid.
       * @param size Number of elements of type T that
       * the buffer can store.
       * @param data The pointer to the data buffer.
       */
      Buffer(S size, T *data);

      /**
       * @return A null (invalid) buffer instance.
       */
      static Buffer null();

      /**
       * Creates a buffer instance with the given size
       * and data. If size is 0 or data is nullptr, the
       * created buffer will be invalid.
       * @param size Number of elements of type T that
       * the buffer can store.
       * @param data The pointer to the data buffer.
       * @return A valid buffer if the arguments are valid,
       * an invalid buffer otherwise.
       */
      static Buffer<T, S> create(S size, T *data);

      /**
       * Swaps the ownership of data between this buffer
       * and the 'other' buffer.
       * @return 'this' buffer.
       */
      Buffer<T, S>& swap(Buffer<T, S>&& other);

      /**
       * Copies the data from the 'other' buffer into 'this'
       * buffer. This is equivalent to a 'dust::memory_move()'
       * from 'other' to 'this' buffer. Does nothing if the
       * size of 'this' buffer is smaller than the size of the
       * 'other' buffer.
       * @return 'this' buffer.
       */
      Buffer<T, S>& copy_from(Buffer<T, S> const& other);

      /**
       * Makes the buffer 'invalid' by setting its size to 0
       * and data to nullptr.
       * @return 'this' buffer.
       */
      Buffer<T, S>& invalidate();

      /**
       * Makes this buffer resign ownership of its current data.
       * This is equivalent to calling 'std::move' on 'this' buffer.
       * @return 'this' buffer.
       */
      Buffer<T, S>&& yield();

      /**
       * Makes this buffer take ownership of the 'other' buffer's
       * data. Invalidates the 'other' buffer.
       * @return 'this' buffer.
       */
      Buffer<T, S>& take(Buffer<T, S>&& other);

      /**
       * Sets the memory held by the buffer to 0s.
       * @return 'this' buffer.
       */
      Buffer<T, S>& clear();

      /**
       * @return The internal array of the buffer.
       */
      T *get_ptr();

      /**
       * @return The internal array of the buffer.
       */
      T const *get_ptr() const;

      /**
       * @return The size of the buffer in bytes.
       */
      S get_size() const;

      /**
       * @return The number of items the buffer can store according to its type.
       */
      S get_capacity() const;

      /**
       * @return True if the buffer is not nullptr and size is not 0, false otherwise.
       */
      bool is_valid() const;

      /**
       * Transfers the ownership of data from the 'other'
       * (right) buffer to 'this' (left) buffer.
       * Invalidates the 'other' buffer. This is equivalent
       * to calling 'take(other.yield())'
       * @return 'this' (left) buffer.
       */
      Buffer<T, S>& operator=(Buffer<T, S>&& other);

    private:

      friend class Page;

      /**
       * Array of type T objects.
       */
      T *m_ptr;

      /**
       * The size of the buffer in bytes.
       */
      S m_size;

  };
}

#include <dust/memory/buffer.hxx>

#endif // DUST_MEMORY_BUFFER_HPP
