#ifndef DUST_MEMORY_ARRAY_HXX
#define DUST_MEMORY_ARRAY_HXX

#include <dust/common.hpp>
#include <dust/memory.hpp>
#include <dust/memory/buffer.hpp>
#include <dust/memory/array.hpp>

#include <utility> // std::move
#include <cstring>

#include <functional>

namespace DUST_NAMESPACE
{

  template<class T, class S, class BS>
  Array<T, S, BS>::Array()
    : m_buffer(Buffer<T, BS>::null()), m_size(S())
  {

  }

  template<class T, class S, class BS>
  Array<T, S, BS>::Array(Array<T, S, BS>&& other)
  {
    take(other.yield());
  }

  template<class T, class S, class BS>
  Array<T, S, BS>::Array(Buffer<T, BS>&& buffer)
  {
    m_buffer.take(buffer.yield());
    m_size = S();
  }

  template<class T, class S, class BS>
  Array<T, S, BS> Array<T, S, BS>::null()
  {
    Array<T, S, BS> a;

    return a;
  }
  
  template<class T, class S, class BS>
  Array<T, S, BS> Array<T, S, BS>::create(Buffer<T, BS>&& buffer)
  {
    Array<T, S, BS> a;

    a.m_buffer.take(buffer.yield());
    a.m_size = S();

    return a.yield();
  }

  template<class T, class S, class BS>
  Array<T, S, BS>& Array<T, S, BS>::swap(Array<T, S, BS>&& other)
  {
    Array<T, S, BS> temp = other.yield();

    other.take(yield());
    take(temp.yield());

    return *this;
  }

  template<class T, class S, class BS>
  Array<T, S, BS>& Array<T, S, BS>::copy_from(Array<T, S, BS> const& other)
  {
    m_buffer.copy_from(other.get_buffer());
    m_size = other.get_size();

    return *this;
  }
  
  template<class T, class S, class BS>
  Array<T, S, BS>& Array<T, S, BS>::invalidate()
  {
    m_buffer.invalidate();
    m_size = 0;

    return *this;
  }
  
  template<class T, class S, class BS>
  Array<T, S, BS>&& Array<T, S, BS>::yield()
  {
    return std::move(*this);
  }
  
  template<class T, class S, class BS>
  Array<T, S, BS>& Array<T, S, BS>::take(Array<T, S, BS>&& other)
  {
    m_buffer.take(other.get_buffer().yield());
    m_size = other.get_size();

    other.invalidate();

    return *this;
  }

  template<class T, class S, class BS>
  Array<T, S, BS>& Array<T, S, BS>::take(Buffer<T, BS>&& buffer)
  {
    m_buffer.take(buffer.yield());
    m_size = S();

    buffer.invalidate();

    return *this;
  }

  template<class T, class S, class BS>
  Array<T, S, BS>& Array<T, S, BS>::empty()
  {
    m_size = 0;

    return *this;
  }

  template<class T, class S, class BS>
  Array<T, S, BS>& Array<T, S, BS>::clear()
  {
    empty();
    m_buffer.clear();

    return *this;
  }

  template<class T, class S, class BS>
  Buffer<T, BS>& Array<T, S, BS>::get_buffer()
  {
    return m_buffer;
  }

  template<class T, class S, class BS>
  Buffer<T, BS> const& Array<T, S, BS>::get_buffer() const
  {
    return m_buffer;
  }

  template<class T, class S, class BS>
  S Array<T, S, BS>::get_size() const
  {
    return m_size;
  }

  template<class T, class S, class BS>
  S Array<T, S, BS>::get_capacity() const
  {
    return m_buffer.get_size() / sizeof(T);
  }

  template<class T, class S, class BS>
  bool Array<T, S, BS>::is_empty() const
  {
    return m_size == 0;
  }

  template<class T, class S, class BS>
  bool Array<T, S, BS>::is_full() const
  {
    return m_size == get_capacity();
  }

  template<class T, class S, class BS>
  bool Array<T, S, BS>::is_valid() const
  {
    return m_buffer.is_valid() && (m_size < m_buffer.get_capacity());
  }

  template<class T, class S, class BS>
  T const *Array<T, S, BS>::begin() const
  {
    return &(*this)[0];
  }
  
  template<class T, class S, class BS>
  T *Array<T, S, BS>::begin()
  {
    return &(*this)[0];
  }

  template<class T, class S, class BS>
  T const *Array<T, S, BS>::end() const
  {
    return &(*this)[m_size];
  }

  template<class T, class S, class BS>
  T *Array<T, S, BS>::end()
  {
    return &(*this)[m_size];
  }
  
  template<class T, class S, class BS>
  T const& Array<T, S, BS>::front() const
  {
    return *begin();
  }

  template<class T, class S, class BS>
  T& Array<T, S, BS>::front()
  {
    return *begin();
  }

  template<class T, class S, class BS>
  T const& Array<T, S, BS>::back() const
  {
    return m_buffer.get_buffer()[m_size - 1];
  }

  template<class T, class S, class BS>
  T& Array<T, S, BS>::back()
  {
    return m_buffer.get_buffer()[m_size - 1];
  }

  template<class T, class S, class BS>
  Array<T, S, BS>& Array<T, S, BS>::push_back(T const& item)
  {
    return insert(item, m_size);
  }

  template<class T, class S, class BS>
  Array<T, S, BS>& Array<T, S, BS>::insert(T const& item, S const i)
  {
    T * items = m_buffer.get_ptr();

    /**
     * By increasing the size by 1 beforehand we ensure the 'size = 0'
     * case works without the need for validating anything since
     * items_to_move = size - i - 1.
     */
    m_size += 1;

    dust::memory_move(&items[i + 1], &items[i], sizeof(T) * (m_size - i));
    dust::memory_copy(&items[i], &item, sizeof(T));

    return *this;
  }

  template<class T, class S, class BS>
  Array<T, S, BS>& Array<T, S, BS>::erase()
  {
    return erase(m_size);
  }

  template<class T, class S, class BS>
  Array<T, S, BS>& Array<T, S, BS>::erase(S const i)
  {
    T *items = m_buffer.get_ptr();

    m_size -= 1;

    dust::memory_move(&(items[i]), &(items[i + 1]), sizeof(T) * (m_size + 1 - i));

    return *this;
  }

  template<class T, class S, class BS>
  Array<T, S, BS>& Array<T, S, BS>::map(
    std::function<void (T&)> f)
  {
    T *const items = m_buffer.get_ptr();

    for(T& item: items)
    {
      f(item);
    }

    return *this;
  }

  template<class T, class S, class BS>
  T& Array<T, S, BS>::operator[](S const pos)
  {
    return m_buffer.get_ptr()[pos];
  }

  template<class T, class S, class BS>
  T const & Array<T, S, BS>::operator[](S const pos) const
  {
    return m_buffer.get_ptr()[pos];
  }

  template<class T, class S, class BS>
  Array<T, S, BS>& Array<T, S, BS>::operator=(Array<T, S, BS>&& other)
  {
    take(other.yield());

    return *this;
  }

}

#endif // DUST_MEMORY_ARRAY_HXX
