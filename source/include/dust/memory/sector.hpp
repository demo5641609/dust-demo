#ifndef DUST_MEMORY_SECTOR_HPP
#define DUST_MEMORY_SECTOR_HPP

#include <dust/common.hpp>
#include <dust/memory/array.hpp>
#include <dust/memory/allocator.hpp>
#include <dust/memory/sector.hpp>
#include <dust/memory/page.hpp>

#define DUST_SECTOR_MINIMUM_SIZE DUST_MINIMUM_REGION_SIZE

#if DUST_SECTOR_MINIMUM_SIZE <= 16
  #define DUST_SECTOR_SIZE_K16 DUST_KILOBYTES(16)
#endif

#if DUST_SECTOR_MINIMUM_SIZE <= 32
  #define DUST_SECTOR_SIZE_K32 DUST_KILOBYTES(32)
#endif

#define DUST_SECTOR_SIZE_K64 DUST_KILOBYTES(64)

#define DUST_SECTOR_SIZE_M2 DUST_MEGABYTES(2)
#define DUST_SECTOR_SIZE_M4 DUST_MEGABYTES(4)
#define DUST_SECTOR_SIZE_M6 DUST_MEGABYTES(6)
#define DUST_SECTOR_SIZE_M8 DUST_MEGABYTES(8)

#define DUST_SECTOR_SIZE_M16  DUST_MEGABYTES(16)
#define DUST_SECTOR_SIZE_M32  DUST_MEGABYTES(32)
#define DUST_SECTOR_SIZE_M64  DUST_MEGABYTES(64)
#define DUST_SECTOR_SIZE_M128 DUST_MEGABYTES(128)
#define DUST_SECTOR_SIZE_M256 DUST_MEGABYTES(256)
#define DUST_SECTOR_SIZE_M512 DUST_MEGABYTES(512)

#define DUST_SECTOR_SIZE_G1 DUST_GIGABYTES(1)
#define DUST_SECTOR_SIZE_G2 DUST_GIGABYTES(2)
#define DUST_SECTOR_SIZE_G4 DUST_GIGABYTES(4)
#define DUST_SECTOR_SIZE_G8 DUST_GIGABYTES(8)

#define DUST_SECTOR_PAGE_CHAIN_SIZE 8

namespace DUST_NAMESPACE
{

  /**
   * Forward-declaration to avoid an include loop.
   * 
   * We can let the <memory_server.hpp> include <sector.hpp>
   * because we only need a pointer to a memory server here,
   * not the full implementation.
   */
  class MemoryServer;

  /**
   * Memory management object used by the Sector class.
   * Allocates memory by managing a chain of pages. Consider
   * this a wrapper object for page. This object tries to
   * expand on demand.
   */
  class Sector : public Allocator
  {
    public:

      /**
       * If this isn't explicitly specified the class won't
       * be able to call 'allocate<T>(...)'
       */
      using Allocator::allocate;

      enum Expand
      {
        /**
         * Sector doesn't try to allocate new pages.
         */
        NEVER,

        /**
         * Sector tries to expand by allocating pages
         * with the same size of the first page.
         */
        BY_NEW_PAGE,

        /**
         * Sector tries to expand by allocating
         * pages of the given expand size.
         */
        BY_NEW_SIZED_PAGE
      };

      /**
       * Constructs an invalid sector instance.
       */
      Sector();

      /**
       * Initializes a sector for general purpose usage. This
       * sector and its pages will rely on the 'meta_sector'
       * to allocate its bookkeeping arrays. Metadata sectors
       * are initialized in a specific way and this function
       * does not cover that case. If you don't know what a
       * metadata sector is then you're not making one anyways.
       * The smallest allowed sector size and expansion size is
       * DUST_SECTOR_MINIMUM_SIZE.
       * @param memory_server The memory server from which to
       * request large region pages.
       * @param meta_allocator The allocator with which to
       * allocate bookkeeping arrays.
       * @param size Desired byte-size of the initial memory
       * region for the sector to allocate.
       * @param expand_size Byte-size for memory region expansions.
       * @param expand_policy Describes the way the sector should
       * behave when it fails an allocation.
       * @param page_blocks Number of block descriptors the
       * sector's pages will have.
       * @param page_blocks_expand Number of new blocks the
       * sector's pages will expand by.
       * @return DUST_OK on success, or DUST_ERROR_SECTOR_INIT_FAIL,
       */
      dust_error_t initialize(
        MemoryServer& memory_server,
        Allocator& meta_allocator,
        dust_size_t size = DUST_SECTOR_MINIMUM_SIZE,
        dust_size_t expand_size = DUST_SECTOR_MINIMUM_SIZE,
        Sector::Expand expand_policy = Expand::BY_NEW_SIZED_PAGE,
        dust_size_t page_blocks = DUST_PAGE_BLOCKS_M,
        dust_size_t page_blocks_expand = DUST_PAGE_BLOCKS_EXPAND_M);

      /**
       * Changes the expand policy of the sector. If no arguments
       * are given the expand policy is set to 'never' expand.
       * Size argument is ignored if the policy is 'by new page'.
       * If 'expand_size' is 0, the policy is set to 'never' expand.
       * @param expand_policy The desired behaviour for when the
       * sector fails an allocation.
       * @param expand_size Byte-size by which to expand the sector.
       */
      void set_expand_policy(
        Sector::Expand expand_policy,
        dust_size_t expand_size = 0);

      /**
       * Allocates a block of memory of 'size' bytes, aligned
       * to 'align' bytes. If 'align' is 0, alignment will be
       * suitable for any object.
       * @param size Byte-size of the desired memory block.
       * @param align Byte-alignment of the desired memory
       * block. Use 'alignof(T)' to get the alignment of any
       * type.
       * @return On success, a buffer object containing a valid
       * pointer and the number of items allocated. On failure,
       * an invalid instance of the Buffer<T> type.
       */
      Buffer<dust_byte_t> allocate(
        dust_size_t size,
        dust_size_t align = 0);

      /**
       * Frees the previously allocated piece of memory.
       * Does nothing if the pointer doesn't match an
       * allocation made by this sector and one of its
       * pages.
       * @param ptr A pointer that was previously allocated
       * by the sector.
       * @return DUST_OK if the sector freed the pointer,
       * or DUST_SKIP if the pointer doesn't belong to the
       * sector.
       */
      dust_error_t free(
        void const * ptr);

    protected:

      friend class MemoryServer;

      /**
       * Initializes the sector as a "meta sector".
       * Sectors of this type are used by other sectors
       * and pages to allocate bookkeeping arrays.
       * The smallest allowed sector size is
       * DUST_SECTOR_MINIMUM_SIZE.
       * @param memory_server The server to request
       * a memory region from.
       * @param size Desired byte-size of the initial
       * memory region for the sector.
       * @return 
       */
      dust_error_t initialize(
        MemoryServer& memory_server,
        dust_size_t size = DUST_MINIMUM_REGION_SIZE);

    private:

      /**
       * Requests a new region from the memory server.
       * Expands the page chain if necessary.
       * @return DUST_OK on success, DUST_SKIP if expansions
       * are not allowed, or
       * DUST_ERROR, DUST_ERROR_MEMORY_SERVER_REGION_FAIL,
       * DUST_ERROR_PAGE_INIT_FAIL
       * on failure.
       */
      dust_error_t request_new_region();

      /**
       * Tries to expand the page chain array to the given
       * size.
       * @param new_size New number of pages that the page
       * chain should be able to hold.
       * @return DUST_OK on success, or
       * DUST_ERROR_SECTOR_PAGE_CHAIN_EXPAND
       * on failure.
       */
      dust_error_t expand_page_chain(
        dust_size_t new_size);

    private:

      Array<Page> m_page_chain;

      /**
       * Allocator for allocating the bookkeeping array.
       */
      Allocator *m_meta_allocator;

      /**
       * Memory server which gives memory regions.
       */
      MemoryServer *m_memory_server;

      /**
       * Byte-size by which to expand the sector when it
       * fails to allocate a memory request.
       */
      dust_size_t m_expand_size;

      /**
       * Tells the sector how to handle the expansion event.
       */
      Sector::Expand m_expand_policy;

  };

}

#endif // DUST_MEMORY_SECTOR_HPP
