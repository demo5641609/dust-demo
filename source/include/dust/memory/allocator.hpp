#ifndef DUST_MEMORY_ALLOCATOR_HPP
#define DUST_MEMORY_ALLOCATOR_HPP

#include <dust/common.hpp>
#include <dust/memory/buffer.hpp>

namespace DUST_NAMESPACE
{

  /**
   * Abstract object type used to provide memory allocation API.
   */
  class Allocator
  {
    public:

      /**
       * Allocates an instance of type T or an array of instances
       * of type T. The returned pointer is suitably aligned for
       * the type.
       * @param n Number of instances to allocate for. If this
       * number is larger than 1, the returned pointer is an array
       * of 'n' instances of T.
       * @return On success, a buffer object containing a valid
       * pointer and the number of items allocated. On failure,
       * an invalid instance of the Buffer<T> type.
       */
      template<class T>
      Buffer<T> allocate(
        dust_size_t n = 1);

      /**
       * Allocates a block of memory of 'size' bytes, aligned
       * to 'align' bytes. If 'align' is 0, alignment will be
       * suitable for any object.
       * @param size Byte-size of the desired memory block.
       * @param align Byte-alignment of the desired memory
       * block. Use 'alignof(T)' to get the alignment of any
       * type.
       * @return On success, a buffer object containing a valid
       * pointer and the number of items allocated. On failure,
       * an invalid instance of the Buffer<T> type.
       */
      virtual Buffer<dust_byte_t> allocate(
        dust_size_t size,
        dust_size_t align = 0) = 0;

      /**
       * Frees the previously allocated piece of memory.
       * Does nothing if the pointer doesn't match an
       * allocation made by this sector and one of its
       * pages.
       * @param ptr A pointer that was previously allocated
       * by the sector.
       * @return DUST_OK if the sector freed the pointer,
       * or DUST_SKIP if the pointer doesn't belong to the
       * sector.
       */
      virtual dust_error_t free(
        void const * ptr) = 0;

    private:

  };

  template<class T>
  Buffer<T> Allocator::allocate(
    dust_size_t n)
  {
    Buffer<T> result_T;

    Buffer<dust_byte_t> result =
      allocate(n * sizeof(T), alignof(T));

    /**
     * Reinterpret the pointer to the desired type T...
     */
    result_T = Buffer<T>::create(
      result.get_size(), (T *) result.get_ptr());

    return result_T;
  }

}

#endif // DUST_MEMORY_ALLOCATOR_HPP
