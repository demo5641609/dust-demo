#ifndef DUST_MEMORY_ARRAY_HPP
#define DUST_MEMORY_ARRAY_HPP

#include <dust/common.hpp>
#include <dust/memory.hpp>
#include <dust/memory/buffer.hpp>

#include <functional>

namespace DUST_NAMESPACE
{

  /**
   * Forward-declaration to avoid an include loop.
   */
  class Page;

  /**
   * Forward-declaration to avoid an include loop.
   */
  class Sector;

  template<class T, class S = dust_size_t, class BS = S>
  class Array
  {
    public:

      /**
       * Constructs an 'invalid' (null) array (0 size and
       * null data). This is the same as creating a 'null'
       * array.
       */
      Array();

      /**
       * Constructs the array by taking ownership of the
       * 'other' array's data.
       * Invalidates the 'other' array.
       */
      Array(Array<T, S, BS>&& other);

      /**
       * Constructs an array that takes ownership of the
       * given buffer.
       */
      Array(Buffer<T, BS>&& buffer);

      /**
       * @return A null (invalid) Array instance.
       */
      static Array<T, S, BS> null();

      /**
       * Creates an array instance with the given data.
       * If size .
       * @return A valid buffer if the arguments are valid,
       * an invalid buffer otherwise.
       */
      static Array<T, S, BS> create(Buffer<T, BS>&& buffer);

      /**
       * Swaps the ownership of data between this array and
       * the 'other' array.
       * @return 'this' array.
       */
      Array<T, S, BS>& swap(Array<T, S, BS>&& other);

      /**
       * Copies the data from the 'other' array into'this'
       * array. This is equivalent to a 'dust::memory_move()'
       * from 'other' to 'this' array. Does nothing if the
       * capacity of 'this' array is smaller than the size of
       * the 'other' array. Sets the size of 'this' array to
       * the size of the 'other' array.
       * @param other The array from which to copy data and
       * copy the size (number of inserted elements) of.
       * @return 'this' array.
       */
      Array<T, S, BS>& copy_from(Array <T, S, BS> const& other);

      /**
       * Sets the size of the array to 0.
       * Does not actually clear memory.
       * @return 'this' array.
       */
      Array<T, S, BS>& empty();

      /**
       * Makes the array 'invalid' by setting its size to 0
       * and invalidating the buffer.
       * @return 'this' array.
       */
      Array<T, S, BS>& invalidate();

      /**
       * Makes this array resign ownership of its current data.
       * This is equivalent to calling 'std::move' on 'this'
       * array.
       * @return 'this' array.
       */
      Array<T, S, BS>&& yield();

      /**
       * Makes this array take ownership of the 'other' array's
       * data. Invalidates the 'other' array. Sets the size of
       * 'this' array to the size of the 'other' array.
       * @return 'this' array.
       */
      Array<T, S, BS>& take(Array<T, S, BS>&& other);

      /**
       * Makes this array take ownership of the buffer's data.
       * Invalidates the 'buffer'. Sets the size to 0.
       * @param buffer The buffer from which to take data.
       * @return 'this' array.
       */
      Array<T, S, BS>& take(Buffer<T, BS>&& buffer);

      /**
       * Sets all the memory held by the array to 0s and sets
       * the size to 0.
       * @return 'this' array.
       */
      Array<T, S, BS>& clear();

      /**
       * @return The internal buffer of the array.
       */
      Buffer<T, BS>& get_buffer();

      /**
       * @return The internal buffer of the array.
       */
      Buffer<T, BS> const& get_buffer() const;

      /**
       * @return The number of items stored in the array.
       */
      S get_size() const;

      /**
       * @return The number of items that can be stored in
       * the array.
       */
      S get_capacity() const;

      /**
       * @return True if there are no items stored in the
       * array, false otherwise.
       */
      bool is_empty() const;

      /**
       * @return True if the size of the array is equal to
       * the capacity of the array, false otherwise.
       */
      bool is_full() const;

      /**
       * @return True if the array currently holds data and
       * size is within bounds, false otherwise.
       */
      bool is_valid() const;

      /**
       * @return Pointer to the first element in the array.
       */
      T const *begin() const;

      /**
       * @return Pointer to the first element in the array.
       */
      T *begin();

      /**
       * @return Pointer after the last element in the array.
       */
      T const *end() const;

      /**
       * @return Pointer after the last element in the array.
       */
      T *end();

      /**
       * @return Reference to the first element in the array.
       */
      T const& front() const;

      /**
       * @return Reference to the first element in the array.
       */
      T& front();

      /**
       * @return Reference to the last element in the array.
       */
      T const& back() const;

      /**
       * @return Reference to the last element in the array.
       */
      T& back();

      /**
       * Inserts the item at position 'i' inside the array.
       * Does not check if the insertion would be valid.
       * @return Reference to the array.
       */
      Array<T, S, BS>& insert(T const& item, S const i);

      /**
       * Inserts the item at the end of the array.
       * Does not check if the array is full.
       * @return Reference to the array.
       */
      Array<T, S, BS>& push_back(T const& item);

      /**
       * Removes the item at position 'i' by moving all items
       * after it to the left.
       * @return Reference to the array.
       */
      Array<T, S, BS>& erase(S const i);

      /**
       * Removes the last item from the array.
       * @return Reference to the array.
       */
      Array<T, S, BS>& erase();

      Array<T, S, BS>& map(
        std::function<void (T&)> f);

      /**
       * @return The item at index 'pos'.
       */
      T& operator[](S const pos);

      /**
       * @return The item at index 'pos'.
       */
      T const & operator[](S const pos) const;

      /**
       * Transfers the ownership of data from the 'other' (right)
       * array to 'this' (left) array. Invalidates the 'other'
       * array. This is equivalent to calling 'take(other.yield())'.
       * @return 'this' (left) array.
       */
      Array<T, S, BS>& operator=(Array<T, S, BS>&& other);

    private:

      /**
       * Data held by the array.
       */
      Buffer<T, BS> m_buffer;

      /**
       * The number of items currently stored in the array.
       */
      S m_size;

  };

  template<class T, class S, class BS>
  Array<T, S, BS>&& null()
  {
    Array<T, S, BS> a;
    return a;
  }
}

#include <dust/memory/array.hxx>

#endif // DUST_MEMORY_ARRAY_HPP
