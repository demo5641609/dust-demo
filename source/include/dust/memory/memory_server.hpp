#ifndef DUST_MEMORY_SERVER_HPP
#define DUST_MEMORY_SERVER_HPP

#include <dust/common.hpp>
#include <dust/memory.hpp>
#include <dust/memory/buffer.hpp>

namespace DUST_NAMESPACE
{

  /**
   * Forward-declaration to avoid an include loop.
   */
  class Sector;

  class MemoryServer
  {
    public:

      MemoryServer();

      ~MemoryServer();

      /**
       * Initializes the server's total memory and sets
       * up the meta sector.
       * @param size The byte-size of the virtual mapping.
       * Note that this won't actually take up the whole
       * 'size' in RAM. Pages will be taken when sectors
       * are created.
       * @return DUST_OK on success, or DUST_ERROR_VIRTUAL_MEM_MAP_FAIL
       * on failure.
       */
      dust_error_t initialize(
        dust_size_t mapped_size = DUST_GIGABYTES(2));

      /**
       * Releases all memory held by the server and
       * resets the internal state of the server.
       * The server can be safely initialized after
       * this. Does nothing if the server is not
       * already holding memory.
       */
      void release_memory();

      /**
       * @return The meta sector of the memory server.
       */
      Sector const& get_meta_sector() const;

      /**
       * @return The meta sector of the memory server.
       */
      Sector& get_meta_sector();

    protected:

      /**
       * Allow sectors to request new regions for
       * their expanding.
       */
      friend class Sector;

      /**
       * Reserves a new region of memory from the
       * server's memory pool.
       * @param size Desired size of the new memory
       * region in bytes.
       * @return Valid Buffer object on success, or
       * an invalid Buffer object on failure.
       */
      Buffer<dust_byte_t> get_new_region(
        dust_size_t size = DUST_MINIMUM_REGION_SIZE);

    protected:

      friend class Sector;

      /**
       * If a sector fails to use a newly created
       * region it can be returned.
       * @param size The byte-size of the allocated
       * region.
       */
      void free_last_region(
        dust_size_t size);

    private:

      /**
       * The sector used by other sectors to allocate
       * bookkeeping objects and arrays.
       */
      Sector m_meta_sector;

      /**
       * The total memory held by the server.
       */
      Buffer<dust_byte_t> m_memory;

      /**
       * Number of bytes that were served for regions.
       */
      dust_size_t m_offset;

  };

}

#endif // DUST_MEMORY_SERVER_HPP
