#include <dust/common.hpp>
#include <dust/memory/buffer.hpp>
#include <dust/utility/file.hpp>

#include <fstream>
#include <filesystem>

namespace DUST_NAMESPACE
{

  File::File()
    : m_filename(),
      m_string_allocator(nullptr),
      m_buffer_allocator(nullptr),
      m_flags(0)
  {

  }

  File::~File()
  {
    close();

    if (m_filename.is_valid())
    {
      /**
       * If the filename was set, deallocate it...
       */
      m_string_allocator->free(m_filename.get_ptr());
    }

    /**
     * Reset object state so that someone can't
     * accidentally get bad pointers...
     */

    m_filename = Buffer<char>::null();
    m_string_allocator = nullptr;
    m_buffer_allocator = nullptr;
    m_flags = 0;
  }

  dust_error_t File::initialize(
    Allocator& string_allocator,
    Allocator& buffer_allocator,
    std::string_view filename,
    uint32_t flags)
  {
    dust_size_t const filename_size = filename.length() + 1;

    if (filename_size > 0)
    {
      m_string_allocator = &string_allocator;
      m_buffer_allocator = &buffer_allocator;
      m_flags = flags;

      /**
       * Allocate a buffer for the filename...
       */
      m_filename.take(
        m_string_allocator->allocate<char>(filename_size));

      if (m_filename.is_valid())
      {
        /**
         * Copy the given filename into our own buffer...
         */
        dust::memory_copy(
          m_filename.get_ptr(), filename.data(), filename_size);
        
        /**
         * Make sure the filename ends with
         * the terminating character...
         */
        m_filename.get_ptr()[filename_size] = '\0';

        /**
         * Don't forget to set the file to not-open state!
         * If we don't do this we can give the bit flag in
         * the 'flags' argument.
         */
        m_flags &= ~DUST_FILE_OPEN;

        m_file.clear();

        return DUST_OK;
      }
    }

    m_string_allocator = nullptr;
    m_buffer_allocator = nullptr;
    m_flags = 0;

    return DUST_ERROR_FILE_INIT;
  }

  dust_error_t File::open()
  {
    if (m_filename.is_valid())
    {
      m_file.open(
        std::string(m_filename.get_ptr()),
        get_openmode(m_flags));
      
      if (m_file.is_open())
      {
        /**
         * Success, set the 'open' flag to 1...
         */
        m_flags |= DUST_FILE_OPEN;

        return DUST_OK;
      }
    }

    return DUST_ERROR_FILE_OPEN;
  }

  void File::close()
  {
    /**
     * Clear the 'open' flag...
     */
    m_flags &= ~DUST_FILE_OPEN;

    m_file.close();
  }

  bool File::exists() const
  {
    return std::filesystem::exists(m_filename.get_ptr());
  }

  size_t File::get_size() const
  {
    if (m_filename.is_valid())
    {
      return std::filesystem::file_size(m_filename.get_ptr());
    }
    else
    {
      return 0;
    }
  }

  std::string_view File::get_filename() const
  {
    return std::string_view{m_filename.get_ptr()};
  }

  std::fstream const& File::get_stream() const
  {
    return m_file;
  }

  std::fstream& File::get_stream()
  {
    return m_file;
  }

  Allocator *File::get_string_allocator() const
  {
    return m_string_allocator;
  }

  Allocator *File::get_buffer_allocator() const
  {
    return m_buffer_allocator;
  }

  std::ios_base::openmode File::get_openmode(
    uint8_t flags)
  {
    std::ios_base::openmode mode{};

    if (flags & DUST_FILE_WRITE)
    {
      /**
       * Enable appending by default. It can be
       * turned off with DUST_FILE_CLEAR...
       */
      mode |= std::ios_base::out;
      mode |= std::ios_base::app;
    }

    if (flags & DUST_FILE_CLEAR)
    {
      /**
       * Enable truncation and disable appending
       * because we are clearing the whole file...
       */
      mode |=  std::ios_base::trunc;
      mode &= ~std::ios_base::app;
    }

    if (flags & DUST_FILE_READ)
    {
      mode |= std::ios_base::in;
    }

    if ((flags & DUST_FILE_READ) && !(flags & DUST_FILE_WRITE))
    {
      /**
       * If file is in read-only mode, disable truncation...
       */
      mode &= ~std::ios_base::trunc;
    }

    if (flags & DUST_FILE_AT_END)
    {
      mode |= std::ios_base::ate;
    }

    return mode;
  }

  Buffer<dust_byte_t> File::read()
  {
    return this->read(*m_buffer_allocator);
  }

  Buffer<dust_byte_t> File::read(
    Allocator& allocator)
  {
    /**
     * Buffer in which to store the bytes we will read
     * from the file...
     */
    Buffer<dust_byte_t> data;

    /**
     * If the file is not in 'binary' mode we will add
     * a '\0' at the end...
     */
    dust_size_t const end = (m_flags & DUST_FILE_BINARY) ? 0 : 1;

    /**
     * File size is reported without the terminating character
     * or EOF, so we add one character if in 'character' mode...
     */
    dust_size_t const file_size = get_size() + end;

    dust_size_t bytes_read = 0;

    if (file_size > 0)
    {
      if ((m_flags & DUST_FILE_OPEN) == 0)
      {
        /**
         * Open the file if it was not previously open...
         */
        if (open() != DUST_OK)
        {
          return Buffer<dust_byte_t>::null();
        }
      }

      /**
       * Prepare a buffer for reading in the whole file...
       */
      data.take(
        allocator.allocate<dust_byte_t>(file_size));

      if (data.is_valid())
      {
        /**
         * Store the contents of the file in the buffer...
         */
        m_file.read((char *) data.get_ptr(), file_size - end);

        bytes_read = m_file.gcount();

        ((char&) data.get_ptr()[file_size - 1]) = 'F';

        dust::memory_clear(
          DUST_PTR_OFFSET(data.get_ptr(), bytes_read),
          data.get_size() - bytes_read);

        /**
         * Check that we either read the whole file or
         * reached the end of the content...
         * 
         * EOF can be reached in 'character' mode if a 0x0
         * is found before the actual end of the file. For
         * this reason we shouldn't compare the actual file
         * size with the number of bytes read. Also, if EOF
         * is reached the state of the internal file is not
         * 'good' but the EOF state flag is set...
         */
        if (m_file.good() || (m_file.eof()))
        {
          m_file.clear();

          /**
           * Insert a '\0' at the end of the read data...
           */
          // if (end)
          // {
          //   ((char&) data.get_ptr()[file_size]) = '\0';
          // }

          return data;
        }
        else
        {
          /**
           * If the reading fails we can't return any data,
           * so deallocate the buffer...
           */
          allocator.free(data.get_ptr());

          /**
           * Clear the error state so the file is still
           * useable...
           */
          m_file.clear();
        }
      }
    }

    return Buffer<dust_byte_t>::null();
  }

  void File::write(Buffer<dust_byte_t> const& data)
  {
    m_file.write((char *) data.get_ptr(), data.get_size());
  }

  void File::write(
    std::string_view str)
  {
    m_file.write(str.data(), str.length());
  }

  dust_error_t File::clear()
  {
    bool was_open = m_flags & DUST_FILE_OPEN;

    /**
     * Close the file in case it was opened beforehand...
     */
    close();

    /**
     * Open the file in truncate mode then just close it.
     * This will clear the content...
     */
    m_file.open(
      m_filename.get_ptr(), std::ios_base::out | std::ios_base::trunc);

    m_file.close();

    /**
     * Re-open the file if it was open before the
     * function was called, or just return...
     */
    return (was_open) ? open() : DUST_OK;
  }

}
