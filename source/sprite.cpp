#include <dust/common.hpp>
#include <dust/game/model.hpp>
#include <dust/graphics/gl.hpp>
#include <dust/graphics/texture.hpp>
#include <dust/graphics/sprite_shader.hpp>
#include <dust/graphics/sprite.hpp>

namespace DUST_NAMESPACE
{

  SpriteShader Sprite::m_shader = SpriteShader();

  Sprite::Sprite()
    : Model(),
      m_color(0.0f, 0.0f, 0.0f, 0.0f),
      m_UV_pos(0.0f, 0.0f),
      m_UV_size(0.0f, 0.0f),
      m_texture(nullptr)
  {

  }

  dust_error_t Sprite::initialize(
    Texture const& texture,
    glm::vec4 color,
    dust_size_t width,
    dust_size_t height,
    dust_size_t UV_x,
    dust_size_t UV_y)
  {
    SpriteVertex vertices[6];

    float real_width  = 1.0f;
    float real_height = 1.0f;

    glGenBuffers(1, &m_VBO);

    if (gl_get_error())
    {
      return DUST_ERROR_SPRITE_INIT;
    }

    gl_bind_VBO(m_VBO);

    glGenVertexArrays(1, &m_VAO);

    if (gl_get_error())
    {
      return DUST_ERROR_SPRITE_INIT;
    }

    gl_bind_VAO(m_VAO);

    if ((width != 0) || (height != 0))
    {
      /**
       * Normalize the pixel width and height
       * to [0.0, 1.0] range...
       */

      real_width  = ((float) width) / texture.get_width();
      real_height = ((float) height) / texture.get_height();
    }

    /**
     * Normalize the UV position x and y
     * to [0.0, 1.0] range...
     */

    m_UV_pos  = {
      (float) UV_x / texture.get_width(),
      (float) UV_y / texture.get_height()
    };

    m_UV_size = {
      real_width,
      real_height
    };

    m_texture = &texture;
    m_color = color;

    Model::set_scale({
      (float) texture.get_width(),
      (float) texture.get_height(),
      0.0f
    });

    vertices[0].position = { 0.0f, 0.0f, 0.0f };
    vertices[0].color    =   m_color;
    vertices[0].UV       = { m_UV_pos.x, m_UV_pos.y };

    vertices[1].position = { 1.0f, 0.0f, 0.0f };
    vertices[1].color    =   m_color;
    vertices[1].UV       = { m_UV_pos.x + m_UV_size.x, m_UV_pos.y };

    vertices[2].position = { 1.0f, 1.0f, 0.0f };
    vertices[2].color    =   m_color;
    vertices[2].UV       = { m_UV_pos.x + m_UV_size.x, m_UV_pos.y + m_UV_size.y };

    vertices[3].position = { 1.0f, 1.0f, 0.0f };
    vertices[3].color    =   m_color;
    vertices[3].UV       = { m_UV_pos.x + m_UV_size.x, m_UV_pos.y + m_UV_size.y };

    vertices[4].position = { 0.0f, 1.0f, 0.0f };
    vertices[4].color    =   m_color;
    vertices[4].UV       = { m_UV_pos.x, m_UV_pos.y + m_UV_size.y };

    vertices[5].position = { 0.0f, 0.0f, 0.0f };
    vertices[5].color    =   m_color;
    vertices[5].UV       = { m_UV_pos.x, m_UV_pos.y };

    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    m_shader.initialize_VAO();

    gl_unbind_VAO();
    gl_unbind_VBO();

    return DUST_OK;
  }

  SpriteShader& Sprite::get_shader()
  {
    return m_shader;
  }

  void Sprite::draw(
    Camera const& camera) const
  {
    glm::mat4 const MVP_matrix =
      camera.get_MVP_matrix(Model::get_model_matrix());

    gl_bind_VBO(m_VBO);
    gl_bind_VAO(m_VAO);

    glUniformMatrix4fv(
      m_shader.get_MVP_uniform_id(),
      1, GL_FALSE, glm::value_ptr(MVP_matrix));

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, m_texture->get_texture());
    glDrawArrays(GL_TRIANGLES, 0, 6);

    gl_unbind_VAO();
    gl_unbind_VBO();
  }

}
