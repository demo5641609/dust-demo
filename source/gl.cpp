#include <dust/common.hpp>
#include <dust/graphics/gl.hpp>

#include <glm/matrix.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/ext/matrix_clip_space.hpp>

#include <iostream>

namespace DUST_NAMESPACE
{

  static int glfw_is_initialized_s = 0;

  dust_error_t graphics_start()
  {
    if (glfwInit() != GLFW_TRUE)
    {
      return DUST_ERROR_GRAPHICS_START;
    }

    glfw_is_initialized_s = 1;

    return DUST_OK;
  }

  bool glfw_is_initialized()
  {
    return glfw_is_initialized_s == true;
  }

  glm::mat4 get_perspective_projection_matrix(
    dust_size_t width,
    dust_size_t height,
    GLfloat fov,
    GLfloat near,
    GLfloat far)
  {
    return glm::perspective(
      glm::radians(fov),
      ((GLfloat) width) / ((GLfloat) height), near, far);
  }

  glm::mat4 get_orthographic_projection_matrix(
    dust_size_t width,
    dust_size_t height,
    GLfloat near,
    GLfloat far)
  {
    return glm::ortho(
      0.0f, (GLfloat) width,
      0.0f, (GLfloat) height,
      near, far);
  }

  void apply_uniform_matrix(
    GLuint shader_uniform_id,
    glm::mat4& matrix)
  {
    glUniformMatrix4fv(
      shader_uniform_id, 1, GL_FALSE, glm::value_ptr(matrix));
  }

  void graphics_end()
  {
    glfwTerminate();

    glfw_is_initialized_s = 0;
  }

  void gl_clear()
  {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  }

  dust_error_t gl_get_error()
  {
    GLenum error = glGetError();

    if (error == GL_NO_ERROR)
    {
      return DUST_OK;
    }

    std::cout << "[OpenGL] Error " << error << std::endl;

    return DUST_ERROR_OPENGL;
  }

  void gl_bind_VBO(
    GLuint VBO)
  {
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
  }

  void gl_unbind_VBO()
  {
    gl_bind_VBO(0);
  }

  void gl_bind_VAO(
    GLuint VAO)
  {
    glBindVertexArray(VAO);
  }

  void gl_unbind_VAO()
  {
    gl_bind_VAO(0);
  }

}
