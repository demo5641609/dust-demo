#include <dust/common.hpp>
#include <dust/graphics/gl.hpp>
#include <dust/graphics/model_shader.hpp>

#include <dust/game/model.hpp>
#include <dust/game/camera.hpp>

#include <glm/matrix.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/ext/matrix_clip_space.hpp>

namespace DUST_NAMESPACE
{
  Camera::Camera()
    : Model()
  {
    m_projection_matrix = glm::identity<glm::mat4>();
  }

  dust_error_t Camera::initialize(
    Camera::Type type,
    dust_size_t width,
    dust_size_t height)
  {
    switch(type)
    {
      case Camera::Type::PERSPECTIVE:
        set_perspective_projection_matrix(width, height);
        break;

      case Camera::Type::ORTHOGRAPHIC:
        set_orthographic_projection_matrix(width, height);
        break;

      default:
        return DUST_ERROR_BAD_ENUM_VALUE;
        break;
    }

    return DUST_OK;
  }

  glm::mat4 const& Camera::get_projection_matrix() const
  {
    return m_projection_matrix;
  }

  glm::mat4 Camera::get_view_matrix() const
  {
    return Model::get_model_matrix();
  }

  void Camera::set_perspective_projection_matrix(
    dust_size_t width,
    dust_size_t height,
    GLfloat fov,
    GLfloat near,
    GLfloat far)
  {
    m_projection_matrix = dust::get_perspective_projection_matrix(
      width, height, fov, near, far);
  }

  void Camera::set_orthographic_projection_matrix(
    dust_size_t width,
    dust_size_t height,
    GLfloat near,
    GLfloat far)
  {
    m_projection_matrix = dust::get_orthographic_projection_matrix(
      width, height, near, far);
  }

  glm::mat4 Camera::get_MVP_matrix(
    glm::mat4 const& model_matrix) const
  {
    /**
     * Remember that the camera's model matrix
     * is the view matrix for everyone else...
     */

    return m_projection_matrix *
           get_model_matrix()  *
           model_matrix;
  }

}
