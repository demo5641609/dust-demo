#include <dust/common.hpp>
#include <dust/memory/array.hpp>
#include <dust/input/input.hpp>

#include <dust/graphics/window.hpp> // GLFW

#include <iostream>

namespace DUST_NAMESPACE
{

  thread_local Input *local_input = nullptr;

  void Input::KeyState::initialize()
  {
    m_state = DUST_KEY_RELEASED;
  }

  void Input::KeyState::update(
    int action)
  {
    switch(action)
    {
      case GLFW_PRESS:

        /**
         * Enter the "pressed" state...
         */
        m_state |= DUST_KEY_PRESSED;

        if ((m_state & DUST_KEY_RELEASED) == DUST_KEY_RELEASED)
        {
          /**
           * If the key is currently in the "released"
           * state and it was just pressed...
           */
            m_state |= DUST_KEY_JUST_PRESSED;
        }
        else
        {
          /**
           * Key was previously "pressed" so clear the
           * "just" state...
           */
          m_state &= ~DUST_KEY_JUST_PRESSED;
        }

        /**
         * Clear the "released" state...
         */
        m_state &= ~(DUST_KEY_RELEASED | DUST_KEY_JUST_RELEASED);

        break;

      case GLFW_RELEASE:

        /**
         * Enter the "released" state...
         */
        m_state |= DUST_KEY_RELEASED;

        if ((m_state & DUST_KEY_PRESSED) == DUST_KEY_PRESSED)
        {
          /**
           * If the key is currently in the "pressed"
           * state and it was just released...
           */
            m_state |= DUST_KEY_JUST_RELEASED;
        }
        else
        {
          /**
           * Key was previously "released" so clear the
           * "just" state...
           */
          m_state &= ~DUST_KEY_JUST_RELEASED;
        }

        /**
         * Clear the "pressed" state...
         */
        m_state &= ~(DUST_KEY_PRESSED | DUST_KEY_JUST_PRESSED);

        break;

      default:
        break;
    }
  }
  
  bool Input::KeyState::is_pressed()
  {
    return (m_state & DUST_KEY_PRESSED) == DUST_KEY_PRESSED;
  }

  bool Input::KeyState::is_just_pressed()
  {
    return (m_state & DUST_KEY_JUST_PRESSED) == DUST_KEY_JUST_PRESSED;
  }

  bool Input::KeyState::is_released()
  {
    return (m_state & DUST_KEY_RELEASED) == DUST_KEY_RELEASED;
  }

  bool Input::KeyState::is_just_released()
  {
    return (m_state & DUST_KEY_JUST_RELEASED) == DUST_KEY_JUST_RELEASED;
  }

  Input::Input()
    : m_keys(), m_allocator(nullptr)
  {

  }

  dust_error_t Input::initialize(
    Allocator& allocator)
  {
    Window *local_window;
    GLFWwindow *local_glfw_window;

    size_t key;

    /**
     * Don't allow two Input instances on one thread...
     * This is mostly because the GLFW window is limited
     * to one instance per thread as well.
     */
    if (local_input != nullptr)
    {
      return DUST_ERROR_INPUT_INIT_FAIL;
    }

    local_window = Window::get_local_thread_window();

    /**
     * Require that the local window was initialized...
     */
    if (local_window == nullptr)
    {
      return DUST_ERROR_INPUT_INIT_FAIL;
    }

    local_glfw_window = local_window->get_window();

    /**
     * GLFW must be operational for this function...
     */
    if (local_glfw_window == nullptr)
    {
      return DUST_ERROR_INPUT_INIT_FAIL;
    }

    m_allocator = &allocator;

    /**
     * Pre-allocate enough space for all the keys that will
     * be updated...
     */
    m_keys.take(
      allocator.allocate<KeyState>(GLFW_KEY_LAST));

    if (!m_keys.is_valid())
    {
      return DUST_ERROR_INPUT_INIT_FAIL;
    }

    /**
     * Currently unused because the Input object is intended to
     * manually update all its keys every frame
     */
    // glfwSetKeyCallback(local_glfw_window, input_glfw_key_callback);
    // glfwSetInputMode(local_glfw_window, GLFW_STICKY_KEYS, GLFW_FALSE);

    for(key = 0; key <= GLFW_KEY_LAST; key++)
    {
      m_keys[key].initialize();
    }

    /**
     * Set self as the local Input instance. No other
     * instance can initialize on this thread until
     * this instance is invalidated.
     */
    local_input = this;

    return DUST_OK;
  }

  bool Input::is_key_pressed(
    dust_keycode_t key)
  {
    if (key <= GLFW_KEY_LAST)
    {
      return m_keys[key].is_pressed();
    }
    else
    {
      return m_extra_keys[key].is_pressed();
    }
  }

  bool Input::is_key_just_pressed(
    dust_keycode_t key)
  {
    if (key <= GLFW_KEY_LAST)
    {
      return m_keys[key].is_just_pressed();
    }
    else
    {
      return m_extra_keys[key].is_just_pressed();
    }
  }

  bool Input::is_key_released(
    dust_keycode_t key)
  {
    if (key <= GLFW_KEY_LAST)
    {
      return m_keys[key].is_released();
    }
    else
    {
      return m_extra_keys[key].is_released();
    }
  }

  bool Input::is_key_just_released(
    dust_keycode_t key)
  {
    if (key <= GLFW_KEY_LAST)
    {
      return m_keys[key].is_just_released();
    }
    else
    {
      return m_extra_keys[key].is_just_released();
    }
  }

  void Input::update()
  {
    GLFWwindow *const local_window = Window::get_local_thread_window()->get_window();

    std::unordered_map<dust_keycode_t, KeyState>::iterator it;

    double x_pos;
    double y_pos;

    size_t key;

    /**
     * Update all predefined GLFW keys...
     */
    for(key = 0; key <= GLFW_KEY_LAST; key++)
    {
      m_keys[key].update(glfwGetKey(local_window, key));
    }

    /**
     * Update all additional keys via scancodes...
     * These are only updated if they were manually accessed.
     */
    for(it = m_extra_keys.begin(); it != m_extra_keys.end(); it++)
    {
      it->second.update(glfwGetKey(local_window, it->first));
    }

    glfwGetCursorPos(local_window, &x_pos, &y_pos);

    m_cursor_position = {
      (float) x_pos,
      (float) y_pos,
    };

  }

  glm::vec2 Input::get_cursor_position() const
  {
    return m_cursor_position;
  }

  glm::vec2 Input::get_cursor_position_opengl() const
  {
    return glm::vec2(
      m_cursor_position.x,
      (float) Window::get_local_thread_window()->get_height() - m_cursor_position.y);
  }

  void input_glfw_key_callback(
    GLFWwindow* window,
    int key,
    int scancode,
    int action,
    int mods)
  {
    DUST_UNUSED window;
    DUST_UNUSED mods;

    switch(key)
    {
      case -1:

        std::cout << "[Input] Unknown key with scancode " << scancode << " was pressed: Event ignored" << std::endl;

        break;
      
      default:

        local_input->update_key((dust_size_t) key, action);

        break;
    }
  }

  void Input::update_key(
    dust_size_t key,
    int action)
  {
    if (key <= GLFW_KEY_LAST)
    {
      m_keys[key].update(action);
    }
    else
    {
      m_extra_keys[key].update(action);
    }
  }

}
