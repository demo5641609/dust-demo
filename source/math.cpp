#include <dust/common.hpp>
#include <dust/utility/math.hpp>

#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

namespace DUST_NAMESPACE
{

  glm::quat look_at(
    glm::vec3 const& origin_position,
    glm::vec3 const& target_position)
  {
    /**
     * Adapted from:
     * 
     * https://www.opengl-tutorial.org/intermediate-tutorials/tutorial-17-quaternions/#i-need-an-equivalent-of-glulookat-how-do-i-orient-an-object-towards-a-point-
     */

    glm::vec3 const start = glm::normalize(origin_position);
    glm::vec3 const target = glm::normalize(target_position);

    glm::vec3 rotation_axis;

    float cos_theta = glm::dot(start, target);

    float root;
    float inverse;

    if (cos_theta < (-1 + DUST_QUAT_AXIS_TOLERANCE))
    {
      /**
       * This is a special case where the vectors are
       * in opposite directions. We will try to guess
       * the rotation axis...
       */

      rotation_axis = glm::cross(glm::vec3(0.0f, 0.0f, 1.0f), start);

      if (glm::length2(rotation_axis) < DUST_QUAT_AXIS_TOLERANCE)
      {
        /**
         * Parallel axis...
         * Take some other axis...
         */

        rotation_axis = glm::cross(glm::vec3(1.0f, 0.0f, 0.0f), start);
      }

      rotation_axis = glm::normalize(rotation_axis);

      return glm::angleAxis(glm::radians(180.0f), rotation_axis);
    }

    rotation_axis = glm::cross(start, target);

    root = std::sqrt((1 + cos_theta) * 2);
    inverse = 1 / root;

    return glm::quat(
      root * 0.5f,
      rotation_axis.x * inverse,
      rotation_axis.y * inverse,
      rotation_axis.z * inverse);
  }
}
