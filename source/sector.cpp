#include <dust/common.hpp>
#include <dust/utility/error.hpp>
#include <dust/memory/page.hpp>
#include <dust/memory/sector.hpp>
#include <dust/memory/memory_server.hpp>

#include <cassert>

namespace DUST_NAMESPACE
{

  Sector::Sector()
    : m_page_chain(Array<Page>::null()),
      m_meta_allocator(nullptr),
      m_memory_server(nullptr),
      m_expand_size(0),
      m_expand_policy(Sector::Expand::NEVER)
  {

  }

  dust_error_t Sector::initialize(
    MemoryServer& memory_server,
    dust_size_t size)
  {
    /**
     * This function is specific to metadata sectors.
     * These sectors handle initialization differently
     * because for the first meta sector there is no
     * other sector to allocate their metadata containers
     * for them. In other words, metadata sectors store
     * their metadata in their own memory.
     */

    /**
     * Temporary page instance that gets copied to the
     * page chain array.
     */
    Page region_page;

    Buffer<dust_byte_t> region_buffer;

    assert(size >= sizeof(Page) * DUST_SECTOR_PAGE_CHAIN_SIZE);

    /**
     * Don't allow re-initializations of already
     * initialized sectors...
     */
    if (m_page_chain.is_valid())
    {
      return DUST_ERROR_SECTOR_INIT_FAIL;
    }

    m_memory_server = &memory_server;
    
    set_expand_policy(Sector::Expand::BY_NEW_PAGE, 0);

    /**
     * This sector is being initialized as a meta
     * sector so it has no reference to another allocator...
     */
    m_meta_allocator = nullptr;

    /**
     * Reserve the initial region memory...
     */
    region_buffer = m_memory_server->get_new_region(
      size);

    if (!region_buffer.is_valid())
    {
      return DUST_ERROR_MEMORY_SERVER_REGION_FAIL;
    }

    /**
     * Initialize the page metadata at the beginning of
     * the meta region. The page is then ready for work...
     * 
     * We don't expect there to be many memory regions
     * so we prepare small arrays in the page...
     * 
     * Note: Page block descriptor arrays grow on demand.
     */
    region_page.initialize_self(
      *this, region_buffer.yield(),
      DUST_PAGE_BLOCKS_S, DUST_PAGE_BLOCKS_EXPAND_S);

    /**
     * Initialize the page chain array. If this is done
     * successfully, the sector is ready for work.
     */
    m_page_chain.take(
      region_page.allocate<Page>(DUST_SECTOR_PAGE_CHAIN_SIZE));

    if (!m_page_chain.is_valid())
    {
      /**
       * Since this is the initialization of a new
       * metadata sector, this should never happen.
       * 
       * But if it does, the caller will have to
       * somehow deal with it.
       */

      return DUST_ERROR_SECTOR_INIT_FAIL;
    }

    /**
     * Set the region page as the first page in the
     * sector's chain. This just means that the sector
     * itself accounts for its own region page. Other
     * sectors will hold this metadata through this
     * sector.
     */
    m_page_chain.push_back(region_page);

    return DUST_OK;
  }

  dust_error_t Sector::initialize(
    MemoryServer& memory_server,
    Allocator& meta_allocator,
    dust_size_t size,
    dust_size_t expand_size,
    Sector::Expand expand_policy,
    dust_size_t page_blocks,
    dust_size_t page_blocks_expand)
  {
    Page region_page;
    Buffer<dust_byte_t> region_buffer;

    assert(size >= DUST_SECTOR_MINIMUM_SIZE);
    assert(expand_size >= DUST_SECTOR_MINIMUM_SIZE);

    /**
     * Don't allow re-initializations of already
     * initialized sectors...
     */
    if (m_page_chain.is_valid())
    {
      return DUST_ERROR_SECTOR_INIT_FAIL;
    }

    m_meta_allocator = &meta_allocator;
    m_memory_server = &memory_server;

    set_expand_policy(expand_policy, expand_size);

    /**
     * Try to initialize the page chain array by
     * allocating a buffer of Page objects...
     */
    m_page_chain.take(
      m_meta_allocator->allocate<Page>(DUST_SECTOR_PAGE_CHAIN_SIZE));

    if (!m_page_chain.is_valid())
    {
      return DUST_ERROR_SECTOR_INIT_FAIL;
    }

    /**
     * Get the initial memory region for the sector...
     */
    region_buffer = m_memory_server->get_new_region(size);

    if (!region_buffer.is_valid())
    {
      m_meta_allocator->free(m_page_chain.get_buffer().get_ptr());

      return DUST_ERROR_SECTOR_INIT_FAIL;
    }

    /**
     * Initialize the region page. This will be the
     * first page we put into the page chain for
     * this sector...
     */
    region_page.initialize(*m_meta_allocator,
      region_buffer.yield(),
      page_blocks, page_blocks_expand);

    if (!region_page.is_valid())
    {
      m_meta_allocator->free(m_page_chain.get_buffer().get_ptr());
      m_memory_server->free_last_region(size);

      return DUST_ERROR_SECTOR_INIT_FAIL;
    }

    m_page_chain.push_back(region_page);

    return DUST_OK;
  }
  
  void Sector::set_expand_policy(
    Sector::Expand expand_policy,
    dust_size_t expand_size)
  {
    switch(expand_policy)
    {
      case Sector::Expand::NEVER:
        m_expand_size = 0;
        break;

      case Sector::Expand::BY_NEW_PAGE:
        m_expand_size = expand_size;
        break;

      case Sector::Expand::BY_NEW_SIZED_PAGE:
        m_expand_size = expand_size;
        break;
    }

    m_expand_policy = expand_policy;
  }

  Buffer<dust_byte_t> Sector::allocate(
    dust_size_t const size,
    dust_size_t const align)
  {
    Buffer<dust_byte_t> result;

    dust_size_t i;

    for(i = 0; i < m_page_chain.get_size(); ++i)
    {
      result = m_page_chain[i].allocate(size, align);
      
      if (result.is_valid())
      {
        return result;
      }
    }

    /**
     * 
     */

      /**
       * Try to perform the allocation with the new page.
       * This should effectively never fail, unless the
       * allocation request is way too big for the
       * configuration of the sector.
       * 
       * If the allocation does fail, we should consider
       * it an improper usage of the sector.
       */
#if 0
      ptr = new_page.allocate(size, align);

      if (ptr != nullptr)
      {
        result.m_page = &m_page_chain[i];
        result.m_ptr  = ptr;
      }
#endif

    return result;
  }

  dust_error_t Sector::free(void const * t_ptr)
  {
    dust_size_t i;

    for(i = 0; i < m_page_chain.get_size(); ++i)
    {
      if (m_page_chain[i].free(t_ptr) == DUST_OK)
      {
        return DUST_OK;
      }
    }

    return DUST_SKIP;
  }
  
  dust_error_t Sector::request_new_region()
  {
    Page new_page;
    Buffer<dust_byte_t> new_region;

    dust_size_t new_region_size = m_expand_size;
    dust_size_t old_page_chain_capacity;

    dust_error_t error;

    /**
     * No page (region) was able to perform the allocation,
     * allocate a new page if the expand policy allows it...
     */
    switch(m_expand_policy)
    {
      case Sector::Expand::NEVER:
        return DUST_SKIP;
        break;

      default:
        break;
    }
    
    /**
     * Before we actually try to generate a new region page,
     * lets make sure we can store the page first.
     * 
     * If the page chain is full, expand it...
     * 
     * Also, remember the old page chain capacity because
     * the extend_page_chain() function can also possibly
     * request a new region, if this is a meta sector...
     */

    old_page_chain_capacity = m_page_chain.get_capacity();

    if (m_page_chain.get_size() == m_page_chain.get_capacity())
    {
      /**
       * If the new page was successfully allocated and
       * initialized but the page chain is full, expand
       * the page chain...
       */

      error = expand_page_chain(
        m_page_chain.get_capacity() + DUST_SECTOR_PAGE_CHAIN_SIZE);

      if (error != DUST_OK)
      {
        /**
         * Give up on the new region since we can't put
         * it into the page chain...
         */
        m_memory_server->free_last_region(new_region_size);

        return DUST_ERROR_SECTOR_PAGE_CHAIN_EXPAND;
      }
    }

    if (old_page_chain_capacity == m_page_chain.get_capacity())
    {
      /**
       * Whether or not the expand_page_chain() function
       * was called in the previous if statement, no new
       * region was allocated. Thus we can get a new one
       * here...
       */

      new_region = m_memory_server->get_new_region(new_region_size);

      if (!new_region.is_valid())
      {
        return DUST_ERROR_MEMORY_SERVER_REGION_FAIL;
      }

      if (m_meta_allocator == nullptr)
      {
        /**
         * 'this' is a meta sector, initialize the page
         * at the start of the new region. We can't call
         * allocate on this sector here because it's
         * already failing an allocation...
         */
        new_page.initialize_self(*this, new_region.yield(),
          m_page_chain[0].get_array_size(),
          m_page_chain[0].get_expand_size());
      }
      else
      {
        /**
         * 'this' is not a meta sector so use the external
         * meta sector...
         */
        new_page.initialize(*m_meta_allocator, new_region.yield(),
          m_page_chain[0].get_array_size(),
          m_page_chain[0].get_expand_size());
      }

      if (!new_page.is_valid())
      {
        return DUST_ERROR_PAGE_INIT_FAIL;
      }

      /**
       * New (region) page is valid and the chain array
       * is ok so we add the new page to the chain...
       */
      m_page_chain.push_back(new_page);
    }

    return DUST_OK;
  }

  dust_error_t Sector::expand_page_chain(dust_size_t new_size)
  {
    Array<Page>   new_array;
    Buffer<Page> &new_buffer = new_array.get_buffer();

    Page new_page;
    Buffer<dust_byte_t> new_region;

    Buffer<Page> result;

    /**
     * This entire 'if' block tries to set up a new page chain.
     * When this succeeds we copy over the old page chain data
     * and insert the new region page into the chain...
     * 
     * There are two ways of expanding the page chain:
     *  1) By allocating through the assigned meta allocator.
     *  2) By allocating through 'this' sector, if it is a meta
     *     sector itself.
     */

    if (m_meta_allocator != nullptr)
    {
      /**
       * Use the external sector to allocate a new page chain...
       */
      result = m_meta_allocator->allocate<Page>(new_size);

      if (result.is_valid())
      {
        new_array.take(result.yield());
      }
    }
    else
    {
      /**
       * If the external meta sector is nullptr then this
       * sector is its own meta sector.
       * 
       * First we will try to allocate a new page chain
       * array 
       */

      new_buffer = allocate<Page>(new_size);

      if (!new_buffer.is_valid())
      {
        /**
         * If the sector fails to allocate its own new
         * page chain array, we need to expand this sector.
         * 
         * Get a new data region from the memory server...
         */
        new_region = m_memory_server->get_new_region();

        /**
         * Use the first page's size and expansion size as
         * reference for the new page.
         */
        new_page.initialize_self(*this, new_region.yield(),
          m_page_chain[0].get_array_size(),
          m_page_chain[0].get_expand_size());

        if (new_page.is_valid())
        {
          /**
           * Allocate a new page chain array...
           */
          new_array.take(
            new_page.allocate<Page>(new_size));

          if (!new_array.is_valid())
          {
            return DUST_ERROR_SECTOR_PAGE_CHAIN_EXPAND;
          }

          /**
           * If this succeeds, let the 'if' fall through. We
           * will deal with the newly created array at the end...
           */
        }
        else
        {
          /**
           * Regions are big by default so this should
           * effectively never happen, but just in case...
           */

          this->free(new_buffer.get_ptr());

          /**
           * Return the region because we can't use it...
           */
          m_memory_server->free_last_region(new_region.get_size());

          return DUST_ERROR_SECTOR_PAGE_CHAIN_EXPAND;
        }
      }
    }

    /**
     * Copy all current pages over to the new array..
     */
    new_array.copy_from(m_page_chain);

    /**
     * If a new region page was allocated, add it to the chain...
     */
    if (new_page.is_valid())
    {
      new_array.push_back(new_page);
    }

    m_page_chain.take(new_array.yield());

    return DUST_OK;
  }

}
