#include <dust/common.hpp>
#include <dust/memory/sector.hpp>
#include <dust/utility/file.hpp>

#include <dust/graphics/gl.hpp>
#include <dust/graphics/shader.hpp>
#include <dust/graphics/model_shader.hpp>
#include <dust/graphics/sprite_shader.hpp>

#include <iostream>
#include <string_view>

namespace DUST_NAMESPACE
{

  SpriteShader::SpriteShader()
    : ModelShader()
  {
    invalidate();
  }

  dust_error_t SpriteShader::initialize(
    Sector& string_sector,
    Sector& data_sector,
    GLsizei extra_vertex_floats,
    std::string_view vertex_shader_path,
    std::string_view fragment_shader_path)
  {
    dust_error_t error = ModelShader::initialize(
      string_sector, data_sector, 2 + extra_vertex_floats,
      vertex_shader_path, fragment_shader_path);

    if (error)
    {
      invalidate();

      return error;
    }

    m_a_UV_location = glGetAttribLocation(
      Shader::get_program(), "aUV");

    if (m_a_UV_location == -1)
    {
      std::cout << "[SpriteShader] Warning: \"aUV\" attribute not found by the shader. Is it unused in shader code?" << std::endl;
    }

    return DUST_OK;
  }
  
  dust_error_t SpriteShader::initialize_VAO() const
  {
    GLsizei const stride = get_vertex_attribute_stride();
    dust_ptr_t const pointer = ModelShader::get_vertex_pointer_offset();

    ModelShader::initialize_VAO();

    /**
     * This shader adds 2 floats for the UV coordinates
     * on top of the model shader attributes so we pass
     * 2 to the ModelShader 'get_stride' function...
     */

    /**
     *  Vertex shader texture UVs input attribute...
     */
    glVertexAttribPointer(m_a_UV_location, 2, GL_FLOAT, GL_FALSE,
      stride, (void const *) pointer);

    glEnableVertexAttribArray(m_a_UV_location);

    return gl_get_error();
  }

  dust_size_t SpriteShader::get_vertex_pointer_offset() const
  {
    return ModelShader::get_vertex_pointer_offset() + (2 * sizeof(GLfloat));
  }

  void SpriteShader::invalidate()
  {
    ModelShader::invalidate();

    m_a_UV_location = -1;
  }

};
