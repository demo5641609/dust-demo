#include <dust/common.hpp>
#include <dust/memory.hpp>

#include <cstring> // std::memmove, std::memcpy

namespace DUST_NAMESPACE
{

  void memory_move(void *dst, void const *src, std::size_t const size)
  {
    std::memmove(dst, src, size);
  }

  void memory_copy(void *dst, void const *src, std::size_t const size)
  {
    std::memcpy(dst, src, size);
  }

  void memory_clear(void *dst, std::size_t const size)
  {
    std::memset(dst, 0, size);
  }

}
