#include <dust/common.hpp>
#include <dust/memory/sector.hpp>
#include <dust/utility/file.hpp>
#include <dust/graphics/gl.hpp>
#include <dust/graphics/shader.hpp>

#include <iostream>

namespace DUST_NAMESPACE
{

  /**
   * Prints the OpenGL error log.
   */
  static void gl_print_program_error(
    GLuint program);

  /*
   * Prints the shader compilation error log.
   */
  static void gl_print_program_error(
    GLuint program);

  Shader::Shader()
  {
    invalidate();
  }

  Shader::~Shader()
  {
    if (m_program != 0)
    {
      done();
    }

    invalidate();
  }

  dust_error_t Shader::initialize(
    std::string_view vertex_source,
    std::string_view fragment_source)
  {
    GLuint vertex_shader   = 0;
    GLuint fragment_shader = 0;

    GLchar const *source;

    m_program = glCreateProgram();

    if (m_program == 0)
    {
      return DUST_ERROR_OPENGL_SHADER;
    }

    vertex_shader = glCreateShader(GL_VERTEX_SHADER);

    if (vertex_shader == 0)
    {
      goto error;
    }
  
    fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);

    if (fragment_shader == 0)
    {
      goto error;
    }

    source = vertex_source.data();

    glShaderSource(vertex_shader, 1, &source, NULL);

    glCompileShader(vertex_shader);

    if (get_shader_error(vertex_shader))
    {
      goto error;
    }

    source = fragment_source.data();

    glShaderSource(fragment_shader, 1, &source, NULL);

    glCompileShader(fragment_shader);

    if (get_shader_error(fragment_shader))
    {
      goto error;
    }

    glAttachShader(m_program, vertex_shader);
    glAttachShader(m_program, fragment_shader);
    glLinkProgram(m_program);

    glDeleteShader(vertex_shader);
    vertex_shader = 0;

    glDeleteShader(fragment_shader);
    fragment_shader = 0;

    glUseProgram(0);

    if ((get_link_error() != DUST_OK) ||
        (gl_get_error()   != DUST_OK))
    {
      goto error;
    }

    return DUST_OK;

error:

    glDeleteShader(vertex_shader);
    glDeleteShader(fragment_shader);

    done();
    invalidate();

    return DUST_ERROR_OPENGL_SHADER;
  }

  dust_error_t Shader::load(
    Sector& string_sector,
    Sector& data_sector,
    std::string_view vertex_shader_path,
    std::string_view fragment_shader_path)
  {
    dust::File vertex_file;
    dust::File fragment_file;

    dust::Buffer<dust_byte_t> vertex_data;
    dust::Buffer<dust_byte_t> fragment_data;

    dust_error_t error = DUST_OK;

    if (vertex_file.initialize(string_sector, data_sector,
      vertex_shader_path, DUST_FILE_READ) != DUST_OK)
    {
      return DUST_ERROR_OPENGL_SHADER;
    }

    if (vertex_file.open() != DUST_OK)
    {
      return DUST_ERROR_OPENGL_SHADER;
    }

    vertex_data = vertex_file.read();
    vertex_file.close();

    if (!vertex_data.is_valid())
    {
      return DUST_ERROR_OPENGL_SHADER;
    }

    if (fragment_file.initialize(string_sector, data_sector,
      fragment_shader_path, DUST_FILE_READ) != DUST_OK)
    {
      return DUST_ERROR_OPENGL_SHADER;
    }

    if (fragment_file.open() != DUST_OK)
    {
      return DUST_ERROR_OPENGL_SHADER;
    }

    fragment_data = fragment_file.read();
    fragment_file.close();

    error = initialize(
      (char *) vertex_data.get_ptr(),
      (char *) fragment_data.get_ptr());

    /**
     * Shader source code was handed over to OpenGL during
     * initialization so we can deallocate the data...
     */

    data_sector.free(vertex_data.get_ptr());
    data_sector.free(fragment_data.get_ptr());

    return error;
  }
  
  GLuint Shader::get_program() const
  {
    return m_program;
  }

  void Shader::use()
  {
    glUseProgram(m_program);
  }

  void Shader::done()
  {
    glDeleteProgram(m_program);
  }

  void Shader::invalidate()
  {
    m_program = 0;
  }

  void gl_print_shader_error(
    GLuint shader)
  {
    char log[1024];

    glGetShaderInfoLog(shader, sizeof(log), nullptr, log);

    std::cerr << "[Shader] Failed to compile shader #"
              << shader << ":\n" << log << std::endl;
  }

  dust_error_t Shader::get_shader_error(
    GLuint shader) const
  {
    GLint success;

    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);

    if (success == GL_FALSE)
    {
      gl_print_shader_error(shader);

      return DUST_ERROR_OPENGL_SHADER;
    }

    return DUST_OK;
  }

  void gl_print_program_error(
    GLuint program)
  {
    char log[1024];

    glGetProgramInfoLog(program, sizeof(log), nullptr, log);

    std::cerr << "[Shader] Failed to link program #"
              << program << ":\n" << log << std::endl;
  }

  dust_error_t Shader::get_link_error() const
  {
    GLint success;

    glGetProgramiv(m_program, GL_LINK_STATUS, &success);

    if (success == GL_FALSE)
    {
      gl_print_program_error(m_program);

      return DUST_ERROR_OPENGL_SHADER;
    }

    return DUST_OK;
  }

}
