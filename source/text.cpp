#include <dust/common.hpp>
#include <dust/memory/buffer.hpp>
#include <dust/memory/allocator.hpp>

#include <dust/graphics/gl.hpp>
#include <dust/graphics/shader.hpp>

#include <dust/graphics/model_shader.hpp>
#include <dust/graphics/msdf_font_shader.hpp>

#include <dust/game/camera.hpp>

#include <dust/graphics/glyph.hpp>
#include <dust/graphics/msdf_font.hpp>
#include <dust/graphics/text.hpp>

#include <glm/matrix.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/ext/matrix_clip_space.hpp>

#include <cstdio>
#include <cstdarg>

#include <string_view>

namespace DUST_NAMESPACE
{

  Text::Text()
    : Model()
  {
    invalidate();
  }

  Text::~Text()
  {
    done();
  }

  dust_error_t Text::initialize(
    Allocator& string_allocator,
    Allocator& data_allocator,
    MSDFFont& font,
    GLfloat size,
    glm::vec4 color,
    dust_size_t max_characters)
  {
    m_string_allocator = &string_allocator;
    m_data_allocator = &data_allocator;

    m_font = &font;

    m_size = size;
    m_length = 0;

    m_VBO = 0;

    m_color = color;

    m_string.take(m_string_allocator->allocate<char>(
      max_characters + 1));

    if (m_string.is_valid())
    {
      /**
       * We multiply by 6 here because we have 6
       * vertices per glyph...
       */
      m_glyphs.take(m_data_allocator->allocate<Glyph::Vertex>(
        6 * max_characters));

      if (m_glyphs.is_valid())
      {
        /**
         * Set all characters in the string to the
         * null-terminating character.
         */
        std::memset(m_string.get_ptr(), '\0', m_string.get_size());

        glGenBuffers(1, &m_VBO);

        if (!gl_get_error())
        {
          gl_bind_VBO(m_VBO);

          glGenVertexArrays(1, &m_VAO);

          if (!gl_get_error())
          {
            gl_bind_VAO(m_VAO);

            /**
             * Prepare a dynamic buffer big enough to
             * hold the maximum number of glyphs for
             * this text instance...
             * 
             * We don't actually send any data yet. We
             * will do that when the string content of
             * the instance changes.
             * 
             *   6 floats per vertex,
             *   6 vertices per glyph.
             */
            glBufferData(GL_ARRAY_BUFFER,
              sizeof(GLfloat) * get_max_characters() * 6 * 6,
              NULL, GL_STATIC_DRAW);

            m_font->get_shader().initialize_VAO();

            gl_unbind_VAO();
            gl_unbind_VBO();

            return DUST_OK;
          }
        }
      }
    }

    /**
     * De-initialize the instance...
     */
    done();

    return DUST_ERROR_TEXT_INIT;
  }

  void Text::set_string(
    std::string_view format,
    ...)
  {
    va_list args;
    va_start(args, format);

    Glyph const *glyph;
    Glyph::Vertex *vertex;

    GLfloat const *plane_size;
    GLfloat const *UVs;

    char *string = m_string.get_ptr();

    dust_size_t i;

    GLfloat plane_left;
    GLfloat plane_right;
    GLfloat plane_top;
    GLfloat plane_bottom;

    GLfloat advance = 0.0f;

    int length = std::vsnprintf(
      string, m_string.get_size(), format.data(), args);

    va_end(args);

    if (length > 0)
    {
      m_length = (dust_size_t) length;

      for(i = 0; i < m_length; ++i)
      {
        if (string[i] != '\n')
        {
          glyph = &m_font->get_glyph((dust_size_t) string[i]);

          /**
           * We fill 6 vertices at once (6 vertices per
           * character...
           */
          vertex = m_glyphs.get_ptr() + (6 * i);

          plane_size = glyph->get_plane_size();
          UVs = glyph->get_UVs();

          plane_left   = (advance + plane_size[DUST_GLYPH_LEFT])  * m_size;
          plane_right  = (advance + plane_size[DUST_GLYPH_RIGHT]) * m_size;
          plane_bottom = (plane_size[DUST_GLYPH_BOTTOM] * m_size);
          plane_top    = (plane_size[DUST_GLYPH_TOP]    * m_size);

          vertex[0].position = { plane_left, plane_bottom, 0.0f };
          vertex[0].color = m_color;
          vertex[0].UV = { UVs[DUST_GLYPH_LEFT], UVs[DUST_GLYPH_BOTTOM] };

          vertex[1].position = { plane_right, plane_bottom, 0.0f };
          vertex[1].color = m_color;
          vertex[1].UV = { UVs[DUST_GLYPH_RIGHT], UVs[DUST_GLYPH_BOTTOM] };

          vertex[2].position = { plane_right, plane_top, 0.0f };
          vertex[2].color = m_color;
          vertex[2].UV = { UVs[DUST_GLYPH_RIGHT], UVs[DUST_GLYPH_TOP] };

          vertex[3].position = { plane_right, plane_top, 0.0f };
          vertex[3].color = m_color;
          vertex[3].UV = { UVs[DUST_GLYPH_RIGHT], UVs[DUST_GLYPH_TOP] };

          vertex[4].position = { plane_left, plane_top, 0.0f };
          vertex[4].color = m_color;
          vertex[4].UV = { UVs[DUST_GLYPH_LEFT], UVs[DUST_GLYPH_TOP] };

          vertex[5].position = { plane_left, plane_bottom, 0.0f };
          vertex[5].color = m_color;
          vertex[5].UV = { UVs[DUST_GLYPH_LEFT], UVs[DUST_GLYPH_BOTTOM] };

          /**
           * Accumulate advance for every character...
           */
          advance += glyph->get_advance();
        }
      }

      gl_bind_VBO(m_VBO);

      glBufferSubData(GL_ARRAY_BUFFER, 0,
        sizeof(Glyph::Vertex) * 6 * m_length, m_glyphs.get_ptr());

      gl_unbind_VBO();
    }
  }

  void Text::set_size(
    GLfloat size)
  {
    m_size = size;
  }

  glm::vec4& Text::get_color()
  {
    return m_color;
  }

  glm::vec4 const& Text::get_color() const
  {
    return m_color;
  }

  GLfloat Text::get_size() const
  {
    return m_size;
  }

  MSDFFont *Text::get_font() const
  {
    return m_font;
  }

  std::string_view Text::get_string() const
  {
    return std::string_view(m_string.get_ptr(), m_length);
  }

  dust_size_t Text::get_max_characters() const
  {
    return m_string.get_size() - 1;
  }

  dust_size_t Text::get_length() const
  {
    return m_length;
  }

  void Text::draw(
    Camera const& camera)
  {
    gl_bind_VBO(m_VBO);
    gl_bind_VAO(m_VAO);

    apply_shader_uniforms(camera);

    glDrawArrays(GL_TRIANGLES, 0, 6 * m_length);

    gl_unbind_VAO();
    gl_unbind_VBO();
  }

  void Text::done()
  {
    glDeleteBuffers(1, &m_VBO);
    m_VBO = 0;

    glDeleteVertexArrays(1, &m_VAO);
    m_VAO = 0;

    if (m_string_allocator != nullptr)
    {
      m_string_allocator->free(m_string.get_ptr());
    }

    if (m_data_allocator != nullptr)
    {
      m_data_allocator->free(m_glyphs.get_ptr());
    }

    invalidate();
  }

  void Text::invalidate()
  {
    m_color = glm::vec4(0.0f);

    m_string.invalidate();
    m_glyphs.invalidate();

    m_string_allocator = nullptr;
    m_data_allocator = nullptr;

    m_font = nullptr;

    m_length = 0;
    m_size = 0;

    m_VBO = 0;
  }

  void Text::apply_shader_uniforms(
    Camera const& camera) const
  {
    glm::mat4 const MVP_matrix =
      camera.get_MVP_matrix(Model::get_model_matrix());

    MSDFFontShader const& shader = m_font->get_shader();

    glUniformMatrix4fv(
      shader.get_MVP_uniform_id(),
      1, GL_FALSE, glm::value_ptr(MVP_matrix));

    glUniform1f(shader.get_screen_pixel_range_uniform_id(),
      m_font->get_screen_pixel_range(m_size));
  }

}
