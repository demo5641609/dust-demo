#include <dust/common.hpp>
#include <dust/memory.hpp>
#include <dust/memory/buffer.hpp>
#include <dust/memory/sector.hpp>
#include <dust/memory/memory_server.hpp>

#include <cassert>
#include <memoryapi.h>

namespace DUST_NAMESPACE
{

  MemoryServer::MemoryServer()
    : m_meta_sector(), m_memory(), m_offset(0)
  {
    
  }

  MemoryServer::~MemoryServer()
  {
    release_memory();
  }

  dust_error_t MemoryServer::initialize(
    dust_size_t mapped_size)
  {
    // Holds the total memory of the server.
    Buffer<dust_byte_t> meta_region_buffer;

    /**
     * If the memory server already has memory and is
     * operating, don't allow a re-initialization...
     */
    if (m_memory.is_valid())
    {
      return DUST_ERROR_MEMORY_SERVER_INIT_FAIL;
    }

    /**
     * Require that at least 2 minimal memory regions
     * can be allocated...
     */
    assert(mapped_size >= (2 * DUST_MINIMUM_REGION_SIZE));

    /**
     * Create a virtual mapping with the operating system.
     * This virtual systems allows us to take memory pages
     * when we need them. We don't take up RAM unless we
     * explicitly write or read from memory pages.
     */
    m_memory = Buffer<dust_byte_t>::create(mapped_size,
      (dust_byte_t *) VirtualAlloc(nullptr, mapped_size,
        MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE));

    if (!m_memory.is_valid())
    {
      return DUST_ERROR_VIRTUAL_MEM_MAP_FAIL;
    }

    m_meta_sector = Sector();
    m_offset = 0;

    /**
     * Initialize the meta sector with the newly initialized
     * meta region page...
     * 
     * The sector will take the page and use it * for its own
     * metadata allocation. Then the sector is ready to serve
     * other sectors...
     */
    if (m_meta_sector.initialize(*this) == DUST_OK)
    {
      return DUST_OK;
    }

    /**
     * If, for some reason, the the sector failed to
     * initialize we release the held memory and reset
     * state...
     */
    release_memory();

    return DUST_ERROR_MEMORY_SERVER_INIT_FAIL;
  }

  void MemoryServer::release_memory()
  {
    if (m_memory.is_valid())
    {
      VirtualFree((LPVOID) m_memory.get_ptr(), 0, MEM_RELEASE);

      m_meta_sector = Sector();
      m_memory = Buffer<dust_byte_t>();
      m_offset = 0;
    }
  }

  Sector const& MemoryServer::get_meta_sector() const
  {
    return m_meta_sector;
  }

  Sector& MemoryServer::get_meta_sector()
  {
    return m_meta_sector;
  }

  Buffer<dust_byte_t> MemoryServer::get_new_region(
    dust_size_t size)
  {
    dust_byte_t *result = nullptr;

    if ((m_offset + size) <= m_memory.get_size())
    {
      result = DUST_PTR_OFFSET(m_memory.get_ptr(), m_offset);

      m_offset += size;
    }

    return Buffer<dust_byte_t>::create(size, result);
  }

  void MemoryServer::free_last_region(
    dust_size_t size)
  {
    if (m_offset >= size)
    {
      m_offset -= size;
    }
  }

}
