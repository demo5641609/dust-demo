#include <dust/common.hpp>
#include <dust/utility/file.hpp>
#include <dust/graphics/gl.hpp>
#include <dust/graphics/glyph.hpp>
#include <dust/graphics/shader.hpp>
#include <dust/graphics/msdf_font.hpp>

#include <stb/stb_image.h>

namespace DUST_NAMESPACE
{
  MSDFFontShader MSDFFont::m_shader = MSDFFontShader();

  MSDFFont::MSDFFont()
    : m_glyphs(),
      m_allocator(nullptr),
      m_plane_size_max(0.0f),
      m_field_size(0.0f),
      m_pixel_range(0.0f),
      m_atlas_width(0),
      m_atlas_height(0),
      m_texture(0)
  {

  }

  MSDFFont::~MSDFFont()
  {
    if ((m_allocator != nullptr) && (m_glyphs.is_valid()))
    {
      m_allocator->free(m_glyphs.get_buffer().get_ptr());
      m_glyphs.invalidate();
    }

    m_allocator = nullptr;
    m_texture = 0;
  }

  dust_error_t MSDFFont::initialize(
    Allocator& string_allocator,
    Allocator& data_allocator,
    std::string_view atlas_filename,
    std::string_view csv_filename,
    GLuint field_size,
    GLuint pixel_range)
  {
    File csv_file;

    Buffer<dust_byte_t> data;

    GLfloat plane_size[4];
    GLfloat UVs[4];

    GLfloat plane_size_max = 0.0f;

    char *c;
    char *cc;

    dust_byte_t *stb_image_data = nullptr;

    dust_size_t i;

    union
    {
      dust_size_t csv_bytes;
      dust_size_t character;
    };

    dust_size_t lines = 0;

    GLfloat advance;

    int width_i;
    int height_i;
    int components_i;

    stbi_set_flip_vertically_on_load(1);

    stb_image_data = (dust_byte_t *) stbi_load(
      atlas_filename.data(), &width_i, &height_i, &components_i, 0);

    stbi_set_flip_vertically_on_load(0);

    if (stb_image_data != nullptr)
    {
      m_atlas_width  = (GLsizei) width_i;
      m_atlas_height = (GLsizei) height_i;

      glGenTextures(1, &m_texture);

      if (gl_get_error() == DUST_OK)
      {
        glBindTexture(GL_TEXTURE_2D, m_texture);

        /**
         * Upload the MSDF font atlas texture to the GPU...
         */
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,
          m_atlas_width, m_atlas_height, 0,
          GL_RGB, GL_UNSIGNED_BYTE, stb_image_data);

        free(stb_image_data);
        stb_image_data = nullptr;

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        glBindTexture(GL_TEXTURE_2D, 0);

        if (gl_get_error() != DUST_OK)
        {
          return DUST_ERROR_TEXT_INIT;
        }
      }
      else
      {
        free(stb_image_data);
        stb_image_data = nullptr;

        return DUST_ERROR_TEXT_INIT;
      }
    }
    else
    {
      return DUST_ERROR_TEXT_INIT;
    }

    if (csv_file.initialize(
      string_allocator, data_allocator,
      csv_filename, DUST_FILE_READ) == DUST_OK)
    {
      m_allocator = &data_allocator;

      data = csv_file.read();
      csv_file.close();

      if (data.is_valid())
      {
        csv_bytes = csv_file.get_size();

        c = (char *) data.get_ptr();

        m_glyphs.take(
          data_allocator.allocate<Glyph>(256));

        if (m_glyphs.is_valid())
        {
          dust::memory_clear(m_glyphs.get_buffer().get_ptr(),
            m_glyphs.get_buffer().get_size() * sizeof(Glyph));

          /**
           * This loop determines the number of lines in the
           * CSV file. This is the number of glyph we will
           * be parsing...
           */
          for(i = 0; i < csv_bytes; ++i)
          {
            if (*c == '\n')
            {
              lines++;
            }

            c++;
          }

          cc = (char *) data.get_ptr();

          for(i = 0; i < lines; ++i)
          {
            /**
             * Each c++ and cc++ just skips a comma or
             * a newline character...
             */

            character = std::strtoul(cc, &c, 10);
            c++;

            char ccc = (char) character;
            (void) ccc;

            advance = std::strtof(c, &cc);
            cc++;

            plane_size[DUST_GLYPH_LEFT] = std::strtof(cc, &c);
            c++;

            plane_size[DUST_GLYPH_BOTTOM] = std::strtof(c , &cc);
            cc++;

            plane_size[DUST_GLYPH_RIGHT] = std::strtof(cc, &c);
            c++;

            plane_size[DUST_GLYPH_TOP] = std::strtof(c , &cc);
            cc++;

            UVs[DUST_GLYPH_LEFT] = std::strtof(cc, &c);
            c++;

            UVs[DUST_GLYPH_BOTTOM] = std::strtof(c , &cc);
            cc++;

            UVs[DUST_GLYPH_RIGHT] = std::strtof(cc, &c);
            c++;

            UVs[DUST_GLYPH_TOP] = std::strtof(c , &cc);
            cc++;

            /**
             * Probably should be calculated before we
             * convert UV coords...
             */
            plane_size_max = std::max(plane_size_max,
              std::max(
                plane_size[DUST_GLYPH_RIGHT] - plane_size[DUST_GLYPH_LEFT],
                plane_size[DUST_GLYPH_TOP]   - plane_size[DUST_GLYPH_BOTTOM]));

            UVs[DUST_GLYPH_LEFT]   /= (GLfloat) m_atlas_width;
            UVs[DUST_GLYPH_RIGHT]  /= (GLfloat) m_atlas_width;
            UVs[DUST_GLYPH_TOP]    /= (GLfloat) m_atlas_height;
            UVs[DUST_GLYPH_BOTTOM] /= (GLfloat) m_atlas_height;

            m_glyphs[character].initialize(
              advance, plane_size, UVs);
          }

          data_allocator.free(data.get_ptr());

          m_plane_size_max = plane_size_max;

          m_field_size  = (GLfloat) field_size;
          m_pixel_range = (GLfloat) pixel_range;

          return DUST_OK;
        }
      }
    }

    /**
     * This part of the function is only reached
     * when an error occurs...
     */

    data_allocator.free(data.get_ptr());
    data_allocator.free(m_glyphs.get_buffer().get_ptr());

    free(stb_image_data);
    stb_image_data = nullptr;

    m_glyphs.invalidate();
    m_allocator = nullptr;

    return DUST_ERROR_MSDF_FONT_INIT;
  }

  MSDFFontShader& MSDFFont::get_shader()
  {
    return m_shader;
  }

  Glyph const& MSDFFont::get_glyph(
    dust_size_t id) const
  {
    return m_glyphs[id];
  }
  
  GLfloat MSDFFont::get_screen_pixel_range(
    GLfloat render_size) const
  {
    return ((m_plane_size_max * render_size) / m_field_size) * m_pixel_range;
  }

  void MSDFFont::bind_atlas_texture() const
  {
    glBindTexture(GL_TEXTURE_2D, m_texture);
  }

}
