#include <dust/common.hpp>
#include <dust/memory/buffer.hpp>
#include <dust/memory/array.hpp>
#include <dust/memory/page.hpp>
#include <dust/memory/sector.hpp>
#include <dust/memory/memory_server.hpp>
#include <dust/utility/math.hpp>
#include <dust/utility/file.hpp>
#include <dust/utility/clock.hpp>

#include <dust/graphics/gl.hpp>
#include <dust/graphics/window.hpp>
#include <dust/graphics/msdf_font_shader.hpp>
#include <dust/graphics/msdf_font.hpp>
#include <dust/graphics/text.hpp>
#include <dust/graphics/sprite.hpp>

#include <dust/input/input.hpp>

#include <dust/game/camera.hpp>

#include <iostream>
#include <cassert>

int main()
{
  dust::MemoryServer memory_server;

  dust::Sector string_sector;
  dust::Sector asset_sector;
  dust::Sector data_sector;

  dust::Window window;
  dust::Input  input;

  dust::MSDFFont font;

  dust::Text fps_text;
  dust::Text delta_text;
  dust::Text cursor_pos_text;

  dust::Texture test_texture;
  dust::Sprite test_sprite;

  dust::Camera camera3D;
  dust::Camera camera2D;

  dust::Clock  fps_clock;
  dust::Clock  elapsed_clock;
  dust::Clock  delta_clock;

  glm::vec2 cursor_pos;

  dust_size_t fps = 0;

  float delta = 0.0f;

  std::cout << "[Game] Version: " << DUST_VERSION_FULL << std::endl;
  std::cout << "[Game] Starting...\n" << std::endl;

  if (memory_server.initialize() != DUST_OK)
  {
    std::cout << "[Memory server] Failed to initialize" << std::endl;

    return EXIT_FAILURE;
  }

  if (string_sector.initialize(
    memory_server, memory_server.get_meta_sector(),
    DUST_SECTOR_SIZE_K64) != DUST_OK)
  {
    std::cout << "[Main] String sector failed to initialize" << std::endl;

    return EXIT_FAILURE;
  }

  if (asset_sector.initialize(
    memory_server, memory_server.get_meta_sector(),
    DUST_SECTOR_SIZE_M128) != DUST_OK)
  {
    std::cout << "[Main] Asset sector failed to initialize" << std::endl;

    return EXIT_FAILURE;
  }

  if (data_sector.initialize(
    memory_server, memory_server.get_meta_sector(),
    DUST_SECTOR_SIZE_M512) != DUST_OK)
  {
    std::cout << "[Main] Data sector failed to initialize" << std::endl;

    return EXIT_FAILURE;
  }

  if (window.open("Dust v" DUST_VERSION_FULL, 1280, 720))
  {
    std::cout << "[Main] Failed to open the window" << std::endl;

    return EXIT_FAILURE;
  }

  std::cout << "[Main] OpenGL version: ";
  std::cout << glGetString(GL_VERSION) << std::endl;

  std::cout << "[Main] GLSL version: ";
  std::cout << glGetString(GL_SHADING_LANGUAGE_VERSION) << '\n' << std::endl;

  std::cout << "[Main] Window size: ";
  std::cout << window.get_width() << 'x' << window.get_height() << std::endl;

  if (input.initialize(data_sector) != DUST_OK)
  {
    std::cout << "[Main] Failed to initialize input" << std::endl;

    return EXIT_FAILURE;
  }

  if (dust::MSDFFont::get_shader().initialize(
    string_sector, data_sector) != DUST_OK)
  {
    std::cout << "[Main] Failed to initialize the MSDF font shader" << std::endl;

    return EXIT_FAILURE;
  }

  if (dust::Sprite::get_shader().initialize(
    string_sector, data_sector) != DUST_OK)
  {
    std::cout << "[Main] Failed to initialize the sprite shader" << std::endl;

    return EXIT_FAILURE;
  }

  if (font.initialize(
    string_sector, data_sector, 
    "assets/fonts/typographer_rotunda_msdf_48_4.png",
    "assets/fonts/typographer_rotunda_msdf_48_4.csv",
    48, 4) != DUST_OK)
  {
    std::cout << "[Main] Failed to initialize a font." << std::endl;

    return EXIT_FAILURE;
  }

  camera3D.initialize(
    dust::Camera::Type::PERSPECTIVE,
    window.get_width(),window.get_height());

  camera2D.initialize(
    dust::Camera::Type::ORTHOGRAPHIC,
    window.get_width(), window.get_height());

  if (fps_text.initialize(
    string_sector, data_sector, font, 24.0f) != DUST_OK)
  {
    std::cout << "[Main] Failed to initialize a text instance." << std::endl;

    return EXIT_FAILURE;
  }

  if (delta_text.initialize(
    string_sector, data_sector, font, fps_text.get_size()) != DUST_OK)
  {
    std::cout << "[Main] Failed to initialize a text instance." << std::endl;

    return EXIT_FAILURE;
  }

  if (cursor_pos_text.initialize(
    string_sector, data_sector, font, fps_text.get_size()) != DUST_OK)
  {
    std::cout << "[Main] Failed to initialize a text instance." << std::endl;

    return EXIT_FAILURE;
  }

  if (test_texture.initialize(
    "assets/textures/test.png") != DUST_OK)
  {
    std::cout << "[Main] Failed to initialize the \"test.png\" texture." << std::endl;

    return EXIT_FAILURE;
  }

  if (test_sprite.initialize(test_texture))
  {
    std::cout << "[Main] Failed to initialize the test sprite" << std::endl;

    return EXIT_FAILURE;
  }

  camera3D.translate({0.0f, 0.0f, -3.0f});

  fps_text.set_string("0 fps");
  fps_text.translate({5.0f, 720.0f - fps_text.get_size(), 0.0f});

  delta_text.translate({5.0f, 720.0f - 2.0f * delta_text.get_size(), 0.0f});
  cursor_pos_text.translate({5.0f, 720.0f - 3.0f * delta_text.get_size(), 0.0f});

  test_sprite.scale({8.0f, 8.0f, 8.0f});
  test_sprite.set_origin({128.0f, 128.0f, 0.0f});

  fps_clock.start();

  while(window.is_open())
  {
    delta_clock.start();

    dust::gl_clear();
    input.update();

    if (input.is_key_just_released(GLFW_KEY_ESCAPE))
    {
      std::cout << "[Main] ESC was pressed. Exiting..." << std::endl;
      break;
    }

    if (input.is_key_just_pressed(GLFW_KEY_A)) 
    {
      std::cout << "Start" << std::endl;
    }

    if (input.is_key_pressed(GLFW_KEY_A))
    {
      std::cout << "Press" << std::endl;
    }

    if (input.is_key_just_released(GLFW_KEY_A))
    {
      std::cout << "Stop" << std::endl;
    }

    if (input.is_key_just_released(GLFW_KEY_F11))
    {
      window.set_fullscreen(!window.is_fullscreen(), true);

      camera3D.initialize(
        dust::Camera::Type::PERSPECTIVE,
        window.get_width(),window.get_height());

      camera2D.initialize(
        dust::Camera::Type::ORTHOGRAPHIC,
        window.get_width(), window.get_height());
    }

    cursor_pos = input.get_cursor_position_opengl();

    cursor_pos_text.set_string("%.0f, %.0f",
      cursor_pos.x, cursor_pos.y);

    test_sprite.set_position({ cursor_pos.x, cursor_pos.y, 0.0 });
    test_sprite.rotate({ 0.0f, 0.0f, delta * 0.5f});

    dust::MSDFFont::get_shader().use();

    font.bind_atlas_texture();
    fps_text.draw(camera2D);
    delta_text.draw(camera2D);
    cursor_pos_text.draw(camera2D);

    dust::Sprite::get_shader().use();
    test_sprite.draw(camera2D);

    window.loop_done();

    elapsed_clock.start();

    if (dust::Clock::elapsed(fps_clock, elapsed_clock) >= 1000.0f)
    {
      fps_text.set_string("%llu fps", fps);

      fps_clock = elapsed_clock;
      fps = 0;
    }
    else
    {
      fps++;
    }

    delta = delta_clock.get_delta_time();
    delta_text.set_string("%.2f ms", 1000.0f * delta);
  }

  std::cout << "[Main] Done" << std::endl;

  return EXIT_SUCCESS;
}
