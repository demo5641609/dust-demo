#include <dust/common.hpp>
#include <dust/memory.hpp>
#include <dust/graphics/gl.hpp>
#include <dust/graphics/glyph.hpp>

namespace DUST_NAMESPACE
{

  Glyph::Glyph()
    : m_plane_size{ 0.0f, 0.0f, 0.0f, 0.0f },
      m_UVs{ 0.0f, 0.0f, 0.0f, 0.0f },
      m_advance(0.0f)
  {

  }

  dust_error_t Glyph::initialize(
    float advance,
    float const *plane_size,
    float const *UVs)
  {
    m_advance = advance;

    /**
     * Copy the plane sizes over to our array...
     */
    dust::memory_copy(m_plane_size, plane_size, sizeof(m_plane_size));

    /**
     * Copy the UV coordinates over to our array...
     */
    dust::memory_copy(m_UVs, UVs, sizeof(m_UVs));

    return DUST_OK;
  }
  
  GLfloat const *Glyph::get_plane_size() const
  {
    return m_plane_size;
  }

  GLfloat const *Glyph::get_UVs() const
  {
    return m_UVs;
  }

  GLfloat Glyph::get_advance() const
  {
    return m_advance;
  }
}
