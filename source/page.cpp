#include <dust/common.hpp>
#include <dust/memory/buffer.hpp>
#include <dust/memory/array.hpp>
#include <dust/memory/allocator.hpp>
#include <dust/memory/page.hpp>

#include <cassert>
#include <algorithm>

namespace DUST_NAMESPACE
{

  Page::Page()
    : m_free_blocks(PageBlocks::null()),
      m_allocated_blocks(PageBlocks::null()),
      m_memory(PageBlock::null()),
      m_meta_allocator(nullptr),
      m_expand_size(0)
  {

  }

  dust_error_t Page::initialize(
    Allocator &meta_allocator,
    Buffer<dust_byte_t>&& data,
    dust_size_t blocks_size,
    dust_size_t blocks_expand_size)
  {
    if (!data.is_valid())
    {
      return DUST_ERROR_PAGE_INIT_FAIL;
    }

    m_meta_allocator = &meta_allocator;
    m_expand_size = blocks_expand_size;

    m_memory.take(data.yield());

    m_free_blocks.take(
      (m_meta_allocator->allocate<PageBlock>(blocks_size)));

    if (!m_free_blocks.is_valid())
    {
      return DUST_ERROR_PAGE_INIT_FAIL;
    }

    m_allocated_blocks.take(
      (m_meta_allocator->allocate<PageBlock>(blocks_size)));

    if (!m_allocated_blocks.is_valid())
    {
      return DUST_ERROR_PAGE_INIT_FAIL;
    }

    m_free_blocks.push_back(
      PageBlock(m_memory.get_size(), m_memory.get_ptr()));

    return DUST_OK;
  }

  dust_error_t Page::initialize_self(
    Allocator &meta_allocator,
    Buffer<dust_byte_t>&& data,
    dust_size_t blocks_size,
    dust_size_t blocks_expand_size)
  {
    Buffer<PageBlock> new_meta_buffer;
    PageBlock new_block;

    /**
     * We don't actually know if the data buffer is aligned
     * for page descriptors so we calculate an aligned
     * pointer just to be sure...
     */
    PageBlock *const ptr = (PageBlock *) DUST_ALIGN_POINTER(
      data.get_ptr(), alignof(PageBlock));

    dust_size_t const arrays_size_combined =
      2 * blocks_size * sizeof(PageBlock);

    /**
     * Here we calculate the offset that was required to
     * align for the page block arrays...
     */
    dust_size_t offset = DUST_PTR_DIFF(ptr, data.get_ptr());

    if (!data.is_valid())
    {
      return DUST_ERROR_PAGE_INIT_FAIL;
    }

    m_meta_allocator = &meta_allocator;
    m_expand_size = blocks_expand_size;

    m_memory.take(data.yield());

    /**
     * Make sure we can store the page arrays in the given
     * memory block...
     */
    if (!m_memory.is_valid() ||
         data.get_size() >= (arrays_size_combined + offset))
    {
      return DUST_ERROR_PAGE_INIT_FAIL;
    }

    /**
     * Initialize the free-blocks array at the start of
     * the memory region...
     */
    m_free_blocks.take(
      Buffer<PageBlock>::create(
        blocks_size, ((PageBlock *) ptr)));

    /**
     * Initialize the allocated-blocks array just after
     * the free-blocks array...
     */
    m_allocated_blocks.take(
      Buffer<PageBlock>::create(
        blocks_size, ((PageBlock *) ptr + blocks_size)));

    if (offset > 0)
    {
      /**
       * In case we had to offset the data pointer, we
       * add the little free-block to the free-blocks
       * array. This will maybe never be used by the
       * page but it is correct to do it here.
       * 
       * We don't have to sort the free-blocks array
       * because this is always smaller than a page
       * block descriptor.
       */

      new_block.m_size = blocks_size;
      new_block.m_ptr  = m_memory.get_ptr();

      m_free_blocks.push_back(new_block);
    }

    /**
     * Since we manually took two chunks of memory we
     * have to also manually push the allocated blocks
     * to the array. This is to ensure that the page
     * will only make allocations after these page
     * block arrays.
     */

    new_block.m_size = (sizeof(PageBlock) * blocks_size);
    new_block.m_ptr  = m_memory.get_ptr() + offset;

    m_allocated_blocks.push_back(new_block);

    new_block.m_size = blocks_size;
    new_block.m_ptr  = DUST_PTR_OFFSET(
      m_memory.get_ptr(), sizeof(PageBlock) * blocks_size);

    m_allocated_blocks.push_back(new_block);

    /**
     * Now we define the rest of the data as a 'free block'...
     */
    if (m_memory.get_capacity() > (arrays_size_combined + offset))
    {
      // The first free byte is after the 2 arrays...
      new_block.m_size = m_memory.get_capacity() - (arrays_size_combined + offset);
      new_block.m_ptr  = m_memory.get_ptr() + arrays_size_combined + offset;

      m_free_blocks.push_back(new_block);
    }

    return DUST_OK;
  }

  Buffer<dust_byte_t> Page::allocate(
    dust_size_t size,
    dust_size_t align)
  {
    PageBlock  new_block;
    PageBlock *free_block;

    // The size that we subtract from the free block.
    dust_size_t required_size;

    dust_size_t i;
    dust_size_t const blocks_size = m_free_blocks.get_size();

    dust_ptr_t ptr;
    dust_ptr_t aligned_ptr;
    dust_ptr_t misalign;

    for(i = 0; i < blocks_size; ++i)
    {
      free_block = &m_free_blocks[i];

      /**
       * Converting pointers into a purely numeric form
       * allows us to do comparison and arithmetic over them.
       */
      ptr = (dust_ptr_t) free_block->get_ptr();

      /**
       * Calculate the aligned pointer that we should be using...
       */
      aligned_ptr = DUST_ALIGN_POINTER(
        ptr, (align == 0) ? alignof(max_align_t) : align);

      /**
       * Calculate the difference between the block's pointer
       * and the desired aligned pointer. This value tells us
       */
      misalign = aligned_ptr - ptr;

      required_size = size + misalign;

      if (free_block->get_size() >= required_size)
      {
        free_block->m_size -= required_size;
        free_block->m_ptr  = DUST_PTR_OFFSET(free_block->get_ptr(), required_size);

        /**
         * If the free block was taken up completely,
         * remove it from the list.
         */
        if (free_block->get_size() == 0)
        {
          m_free_blocks.erase(i);
        }

        /**
         * If a small piece of memory was skipped due
         * to misalignment, add a small free block.
         */
        if (misalign > 0)
        {
          new_block.m_ptr = (dust_byte_t *) ptr;
          new_block.m_size = misalign;

          /**
           * The allocation is about to succeed but first
           * we have to make sure that we can store the
           * block descriptors in both the free-blocks array
           * and in the allocated-blocks array.
           */

          if (m_free_blocks.is_full())
          {
            if (expand_blocks_array(m_free_blocks) != DUST_OK)
            {
              return Buffer<dust_byte_t>();
            }
          }

          if (m_allocated_blocks.is_full())
          {
            if (expand_blocks_array(m_allocated_blocks) != DUST_OK)
            {
              return Buffer<dust_byte_t>();
            }
          }

          m_free_blocks.push_back(new_block);
          
          if (free_block->get_size() > 1)
          {
            /**
             * Sort free blocks by pointers in ascending order.
             */
            std::sort(m_free_blocks.begin(), m_free_blocks.end(),
              [](PageBlock const& lb, PageBlock const& rb)->bool {
                return ((dust_ptr_t) lb.get_ptr()) < ((dust_ptr_t) rb.get_ptr());
            });
          }
        }

        new_block.m_ptr = (dust_byte_t *) (ptr + misalign);
        new_block.m_size = size;

        m_allocated_blocks.push_back(new_block);

        return new_block;
      }
    }

    return Buffer<dust_byte_t>();
  }

  dust_error_t Page::free(
    void const * t_ptr)
  {
    PageBlock  new_block;
    PageBlock *block;
    PageBlock *prev_block;

    dust_ptr_t ptr = (dust_ptr_t) t_ptr;

    dust_ssize_t i;
    dust_size_t  blocks_size = m_allocated_blocks.get_size();

    for(i = 0; i < (dust_ssize_t) blocks_size; ++i)
    {
      block = &(m_allocated_blocks.get_buffer().get_ptr()[i]);

      /**
       * Try to find an allocated block with the same pointer...
       */
      if (ptr == ((dust_ptr_t) block->get_ptr()))
      {
        /**
         * We are moving the allocated block to the
         * 'free_blocks' array.
         */
        dust::copy_object(&new_block, *block);

        m_allocated_blocks.erase(i);
        
        if (m_free_blocks.get_size() == m_free_blocks.get_capacity())
        {
          expand_blocks_array(m_free_blocks);
        }

        m_free_blocks.push_back(new_block);
        
        /**
         * Sort free blocks by size in ascending order...
         */
        std::sort(m_free_blocks.begin(), m_free_blocks.end(),
          [](PageBlock const& lb, PageBlock const& rb)->bool {
            return ((dust_ptr_t) lb.get_ptr()) < ((dust_ptr_t) rb.get_ptr());
        });

        /**
         * Now that free blocks are properly sorted,
         * we can try to find adjacent free blocks
         * which we can combine into a single bigger block...
         */
        if (m_free_blocks.get_size() > 1)
        {
          /**
           * There have to be at least 2 free blocks
           * for this to be a valid operation...
           * 
           * Also, let the for loop update the size
           * after each iteration, since we possibly
           * remove blocks from the 'free_blocks' array...
           * 
           * We don't know where the new free block is
           * after sorting, so we start from 'i = 0'.
           */
          for(i = 0; i < ((dust_ssize_t) m_free_blocks.get_size()) - 1; ++i)
          {
            prev_block = &(m_free_blocks.get_buffer().get_ptr()[i]);
            block = &(m_free_blocks.get_buffer().get_ptr()[i + 1]);

            ptr = ((dust_ptr_t) prev_block->get_ptr()) + prev_block->get_size();

            /**
             * If the previous and current block are adjacent...
             */
            if (ptr == ((dust_ptr_t) block->get_ptr()))
            {
              prev_block->m_size += block->get_size();

              m_free_blocks.erase(i + 1);

              /**
               * Since 'i' will increase before the next iteration,
               * this subtraction will make the loop repeat the check
               * on the same index
               */
              i -= 1;
            }
          }

          /**
           * The requested block was freed, and the 'free_blocks'
           * array was updated. Our job here is done...
           */

          return DUST_OK;
        }
      }
    }

    return DUST_SKIP;
  }
  
  bool Page::is_empty() const
  {
    return m_allocated_blocks.get_size() == 0;
  }

  bool Page::is_full() const
  {
    return m_free_blocks.get_size() == 0;
  }

  bool Page::is_valid() const
  {
    return m_memory.is_valid();
  }

  Page::PageBlock const& Page::get_memory()
  {
    return m_memory;
  }

  dust_error_t Page::expand_blocks_array(
    PageBlocks &blocks_array)
  {
    PageBlocks new_array;
    Buffer<PageBlock> new_buffer;

    dust_size_t new_size = blocks_array.get_capacity() + m_expand_size;

    /**
     * Allocate a new buffer for the array...
     */
    new_buffer = m_meta_allocator->allocate<PageBlock>(new_size);

    if (new_buffer.is_valid())
    {
      new_array = PageBlocks::create(new_buffer.yield());

      /**
       * Copy over the old block descriptors...
       */
      new_array.copy_from(blocks_array);

      blocks_array.take(new_array.yield());

      return DUST_OK;
    }

    return DUST_ERROR_PAGE_BLOCKS_ARRAY_EXPAND;
  }

  dust_size_t Page::get_array_size() const
  {
    /**
     * Return the capacity of the larger array...
     */

    return (m_allocated_blocks.get_capacity() > m_free_blocks.get_capacity()) ?
      m_allocated_blocks.get_capacity() : m_free_blocks.get_capacity();
  }

  dust_size_t Page::get_expand_size() const
  {
    return m_expand_size;
  }

  Page& Page::reset()
  {
    PageBlock free_block = {
      m_memory.get_size(),
      m_memory.get_ptr()
    };

    m_free_blocks.empty();
    m_allocated_blocks.empty();

    m_free_blocks.push_back(free_block);

    return *this;
  }

  void Page::release()
  {
    m_meta_allocator->free(m_free_blocks.get_buffer().get_ptr());
    m_meta_allocator->free(m_allocated_blocks.get_buffer().get_ptr());

    m_free_blocks = PageBlocks::null();
    m_allocated_blocks = PageBlocks::null();
  }

}
