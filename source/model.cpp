#include <dust/common.hpp>
#include <dust/utility/math.hpp>
#include <dust/graphics/gl.hpp>

#include <dust/game/model.hpp>

#include <glm/glm.hpp>
#include <glm/matrix.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

#include <iostream>

namespace DUST_NAMESPACE
{

  Model::Model()
    : m_rotation(glm::vec3(0.0f, 0.0f, 0.0f)),
      m_position(glm::vec3(0.0f, 0.0f, 0.0f)),
      m_scale(glm::vec3(1.0f, 1.0f, 1.0f)),
      m_origin(glm::vec3(0.0f, 0.0f, 0.0f))
  {

  }

  glm::quat const& Model::get_rotation() const
  {
    return m_rotation;
  }

  glm::vec3 Model::get_position() const
  {
    return m_position;
  }

  glm::vec3 Model::get_scale() const
  {
    return m_scale;
  }

  glm::vec3 Model::get_origin() const
  {
    return m_origin;
  }

  Model& Model::set_rotation(
    glm::vec3 const& rotation)
  {
    m_rotation = glm::quat(rotation);

    return *this;
  }

  Model& Model::set_rotation(
    glm::quat const& rotation)
  {
    m_rotation = rotation;

    return *this;
  }

  Model& Model::set_rotation_2D(
    float radians)
  {
    m_rotation = glm::quat(DUST_MODEL_2D_ROTATION_AXIS * radians);

    return *this;
  }

  Model& Model::set_position(
    glm::vec3 const& position)
  {
    m_position = position;

    return *this;
  }

  Model& Model::set_scale(
    glm::vec3 const& scale)
  {
    m_scale = scale;

    return *this;
  }

  glm::mat4 Model::get_model_matrix() const
  {
    glm::mat4 model_matrix;

    this->export_model_matrix(model_matrix);

    return model_matrix;
  }

  void Model::export_model_matrix(
    glm::mat4& model_matrix) const
  {
    model_matrix  = glm::identity<glm::mat4>();
    model_matrix *= glm::translate(m_position - m_origin);
    model_matrix *= glm::translate(m_origin);
    model_matrix *= glm::toMat4(m_rotation);
    model_matrix *= glm::translate(-m_origin);
    model_matrix *= glm::scale(m_scale);
  }

  Model& Model::rotate(
    glm::vec3 const& rotation)
  {
    /**
     * Quaternion rotations are applied in
     * order of 'second * first'...
     */
    m_rotation = glm::quat(rotation) * m_rotation;

    return *this;
  }

  Model& Model::rotate(
    glm::quat const& rotation)
  {
    /**
     * Quaternion rotations are applied in
     * order of 'second * first'...
     */
    m_rotation = rotation * m_rotation;

    return *this;
  }

  Model& Model::rotate(
    float radians,
    glm::vec3 const& axis)
  {
    m_rotation = glm::quat(axis * radians) * m_rotation;

    return *this;
  }

  Model& Model::rotate_2D(
    float radians)
  {
    rotate(radians, DUST_MODEL_2D_ROTATION_AXIS);

    return *this;
  }

  Model& Model::translate(
    glm::vec3 const& offset)
  {
    m_position += offset;

    return *this;
  }

  Model& Model::scale(
    glm::vec3 const& scale)
  {
    m_scale *= scale;

    return *this;
  }

  Model& Model::set_origin(
    glm::vec3 const& origin)
  {
    m_origin = origin;

    return *this;
  }

  void Model::look_at(
    glm::vec3 const& target_position)
  {
    m_rotation = dust::look_at(get_position(), target_position);
  }

}
