#version 330 core

attribute vec3 aPos;
attribute vec4 aColor;

varying vec4 vColor;

uniform mat4 uMVP;

void main()
{
  vColor = aColor;

  gl_Position = uMVP * vec4(aPos, 1.0);
}
