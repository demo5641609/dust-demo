#version 330 core

attribute vec3 aPos;
attribute vec4 aColor;
attribute vec2 aUV;

varying vec4 vColor;
varying vec2 vUV;

uniform mat4 uMVP;

void main()
{
  vColor = aColor;
  vUV = aUV;

  gl_Position = uMVP * vec4(aPos.xyz, 1.0);
}
