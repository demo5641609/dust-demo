#version 330 core

in vec4 vColor;
in vec2 vUV;

uniform sampler2D uTexture;

void main()
{
  gl_FragColor = texture(uTexture, vUV) * vColor;
}
