#version 330 core

in vec4 vColor;
in vec2 vUV;

uniform float uScreenPixelRange;
uniform sampler2D uMSDF;

float median(float r, float g, float b)
{
  return max(min(r, g), min(max(r, g), b));
}

void main()
{
  vec3 msd = texture2D(uMSDF, vUV).rgb;
  float sd = median(msd.r, msd.g, msd.b);
  float screenPixelDistance = uScreenPixelRange * (sd - 0.5);
  float opacity = clamp(screenPixelDistance + 0.5, 0.0, 1.0);

  gl_FragColor = mix(vec4(vColor.rgb, 0.0), vColor, opacity);
}
