
CPPFLAGS = -std=c++17
BUILD_TYPE = Debug

CPP_DEFINES = -DDUST_NAMESPACE=dust

ifndef cpp
  CPP = g++
else
  CPP = $(cpp)
endif

ifndef build
  build = debug
endif

ifeq ($(build), debug)
  CPPFLAGS += -O0 -g
  BUILD_TYPE = Debug
  CPP_DEFINES += -DDUST_DEBUG

  # Disable asserts
  CPP_DEFINES += -DNDEBUG
else
  ifeq ($(build), release)
    CPPFLAGS += -O3 -ffast-math
    BUILD_TYPE = Release
    CPP_DEFINES += -DDUST_RELEASE
  else
    $(error Unknown build type '$(build)')
  endif
endif

ifeq ($(host), native)
  CPPFLAGS += -march=native
endif

CPPFLAGS += -pipe -pedantic

$(info [Make] Building with: $(CPPFLAGS))

CPPFLAGS += -Wall -Wextra
