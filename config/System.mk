
OS_TAG := Unknown

ifeq ($(OS), Windows_NT)

   ifeq ($(PROCESSOR_ARCHITECTURE),AMD64)
    OS_TAG := win64
    CPP_DEFINES += -DDUST_OS_WIN64
   else
    ifeq ($(PROCESSOR_ARCHITECTURE),x86)
      OS_TAG := win32
      CPP_DEFINES += -DDUST_OS_WIN32
    endif
  endif # AMD64

  PROGRAM := game.exe

endif # Windows_NT

OUT_DIR := $(OUT_DIR)/$(OS_TAG)
